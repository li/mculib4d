module arm.syscfg;

import chip;
private enum bool hasBusPeriph = HasPeriph!("SYSCFG");

/**
* 内存映射模式
*/
enum FLagMemMode 
{
    Flash = 0,      /// Flash memory mapped at 0x00000000
    SRAM = 1,       /// SRAM mapped at 0x00000000
    ROM = 2         /// System Flash memory mapped at 0x00000000
}



static if(hasBusPeriph):
import arm.bus;
import arm.rcc;

static interface SYSCFG //: MMIOBus!(Peripherals.SYSCFG)
{
    enum SYSCFG_Type* mBase = Peripherals.SYSCFG;

    private
    pragma(crt_constructor,CrtPriority!(FlagCrt.ROOT,0))
    static this()
    {
        RCC.Power!("SYSCFGEN")(true);
    }
}


