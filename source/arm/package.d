module arm;

version(LDC){

    // 按需求插入
/*
    static if (__traits(targetCPU) is "cortex-m4"){
        public import arm.target_cortex_m4;
    }else static if (__traits(targetCPU) is	"cortex-m3"){
    }
*/
    /*
    寄存器读写
    */


}


static if (__traits(targetCPU) == "cortex-m3")
{

    public import arm.trace;
}
static if (__traits(targetCPU) == "cortex-m4")
{
    //public import arm.target_cortex_m4;
    public import arm.cortex_m4;
    public import arm.trace;
    
}


/**
    通用函数
*/
public{
    import arm.rcc;
    import arm.pwr;
    //import arm.stk;
    import arm.timer;
    import arm.gpio;
    import arm.fpu;
    import arm.stk;
    import arm.pwr;
    import arm.flash;
    import arm.utils;
} 
