module arm.other;

private import ldc.llvmasm;

void __CLREX()
{
	__asm("clrex","");
}


// uint
//pragma(inline,true)
bool __STREXW(const(uint*) addr, immutable uint val) 
{
	return __asm!bool("strex $0,$2,[$1]", "=&r,r,r",addr,val);
}

// ushort
//pragma(inline,true)
bool __STREXH(const(ushort*) addr,immutable ushort val)
{
	return __asm!bool("strexh $0,$2,[$1]", "=&r,r,r",addr,val);
}
// ubyte
bool __STREXB(const(ubyte*) addr, immutable ubyte val) 
{
	return __asm!bool("strexb $0,$2,[$1]", "=&r,r,r",addr,val);
}


// uint
uint __LDREXW(uint* addr)
{
    return __asm!uint(
        "ldrex $0,[$1]",
        "=r,r",
        addr);
}

//ushort
ushort __LDREXH(ushort* addr)
{
    return __asm!ushort(
        "ldrexh $0,[$1]",
        "=r,r",
        addr);
}

//ubyte
ubyte __LDREXB(ubyte* addr)
{
    return __asm!ubyte(
        "ldrexb $0,[$1]",
        "=r,r",
        addr);
}
