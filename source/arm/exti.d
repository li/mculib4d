module arm.exti;

/**
* 外部中断 事件控制器
* https://blog.csdn.net/killweiwei/article/details/128219472
* system tick  https://blog.csdn.net/qq_41703711/article/details/107601819
*/
import chip;

import arm.nvic;
import arm.misc;
/// 判断 是否声明了 EXTI
private enum bool hasBusPeriph = HasPeriph!("EXTI");
static if(hasBusPeriph):
final abstract class D_EXTI
{

}