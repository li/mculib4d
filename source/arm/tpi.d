module arm.tpi;


import arm;

import arm.bus;

version (NONE):

final abstract class TPI : MMIOBus!(MMIO!(CorePeripheralAddress.TPI_Base,TPI_Type))
{
    private enum TPI_Type* mBase = MMIO!(CorePeripheralAddress.ITM_Base,TPI_Type);
}

enum flagProtocol
{
    SWD = 0,
    Manchester = 1,
    NRZ = 2,
}

/// Structure type to access the Trace Port Interface Register (TPI)
struct TPI_Type
{
    // CMSIS_TPI
    /// 支持同步端口大小状态寄存器
    enum mask_SSPSR
    {
        ONE = 1 << 0,           
        TWO = 1 << 1,
        THREE = 1 << 2,
        FOUR = 1 << 3,
    }

    enum mask_CSPSR
    {
        ONE = 1 << 0,           
        TWO = 1 << 1,
        THREE = 1 << 2,
        FOUR = 1 << 3,
    }

    /// 异步时钟分频
    enum mask_ACPR
    {
        PRESCALER = 0xFF,

    }

    /// 选择协议
    enum mask_SPPR
    {
        PROTOCOL = 0x03,    /// 0x00 = TracePORT/SWD, 0x01 = Manchester, 0x02 = NRZ
    }

    enum mask_FFSR
    {
        FTNONSTOP = 1 << 0,     /// Formatter and flush status
    }

    enum mask_FFCR
    {
        ENFCONT = 1 << 1,       /// Formatter and flush control
        TRIGIN = 1 << 8,        /// Trigger in
    }
    enum mask_FSCR
    {
        FSCR = uint.max,
    }
    /*
    enum mask_TRIGGER
    {

    }
    enum mask_FIFO0
    {

    }
    enum mask_ITATBCTR2
    {

    }
    enum mask_ITATBCTR0
    {

    }
    enum mask_FIFO1
    {

    }
    enum mask_ITCTRL
    {

    }
    */
    enum mask_CLAIMSET
    {
        CLAIMMASK = uint.max,

    }
    enum mask_CLAIMCLR
    {
        CLAIMCLR = uint.max,
    }
    enum mask_DEVID
    {
        //DEVID = uint.max,
        NrTraceInput = 0x1F << 0,
        AsynCLkIn = 1 << 5,
        MinBufSz = 0x07 << 6,
        PRINVALID = 1 << 9,
        MANCVALID = 1 << 10,
        NRZVALID = 1 << 11,
    }
    enum mask_DEVTYPE
    {
        MajorType = 0x0f << 0,
        SubType = 0x0F << 4,

    }
    @disable this();
    @Register("read-only",0x00)     uint SSPSR;     /// 支持接口大小寄存器
    @Register("read-write",0x04)    uint CSPSR;     /// 当前接口寄存器
    uint[2] Reserved0;
    @Register("read-write",0x10)    uint ACPR;      /// 异步时钟分频寄存器
    uint[55] Reserved1;
    @Register("read-write",0xF0)    uint SPPR;      /// 选择端口寄存器
    uint[131] Reserved2;
    @Register("read-only",0x300)    uint FFSR;      /// Formatter and Flush Status Register
    @Register("read-write",0x304)   uint FFCR;      /// Formatter and Flush Control Register
    @Register("read-write",0x308)   uint FSCR;      /// Formatter Synchronization Counter Register
    uint[759] Reserved3;
    @Register("read-only",0xEE8)    uint TRIGGER;   /// 触发器寄存器
    @Register("read-only",0xEEC)    uint FIFO0;     /// FIFO0寄存器
    @Register("read-only",0xEF0)    uint ITATBCTR2; /// ITATBCTR2寄存器
    uint[1] Reserved4;
    @Register("read-only",0xEF8)    uint ITATBCTR0; /// ITATBCTR0寄存器
    @Register("read-only",0xEFC)    uint FIFO1;     /// FIFO1寄存器
    @Register("read-write",0xF00)   uint ITCTRL;    /// ITCTRL寄存器
    uint[39] Reserved5;
    @Register("read-write",0xFA0)   uint CLAIMSET;  /// CLAIMSET寄存器
    @Register("read-write",0xFA4)   uint CLAIMCLR;  /// CLAIMCLR寄存器
    uint[8] Reserved6;
    @Register("read-only",0xFC8)    uint DEVID;     /// DEVID寄存器
    @Register("read-only",0xFCC)    uint DEVTYPE;   /// DEVTYPE寄存器
}