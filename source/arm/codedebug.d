module arm.codedebug;

import arm;

import arm.bus;


/// CoreDebug
static interface CoreDebug //MMIOBus!(MMIO!(CorePeripheralAddress.CoreDebug_Base,CoreDebug_Type))
{
    /// 外设地址
    private enum CoreDebug_Type* mBase = MMIO!(CorePeripheralAddress.CoreDebug_Base,CoreDebug_Type);
    enum IFName = "CoreDebug";
    /// 寄存器指针
    //enum CoreDebug_Struct* pCoreDebug = cast(CoreDebug_Struct*)PeripheralAddress;
}

/// CoreDebug 结构体
struct CoreDebug_Type
{
    enum mask_DHCSR
    {
        C_DEBUGEN = 1 << 0,       /// 调试使能
        C_HALT = 1 << 1,          /// 暂停
        C_STEP = 1 << 2,          /// 单步
        C_MASKINTS = 1 << 3,      /// 中断屏蔽
        C_SNAPSTALL = 1 << 5,     /// 暂停时快照
        S_REGRDY = 1 << 16,       /// 寄存器就绪

    }
    /// 核心寄存器选择
    enum mask_DCRSR
    {
        REGSEL = 0x1F,            /// 寄存器选择
        REGWNR = 1 << 16,         /// 寄存器读写 选择 1:写 0:读

    }
    /// 调试核心寄存器数据
    enum mask_DCRDR
    {
        DCRDR = uint.max,         
    }
    /// 调试异常和监视寄存器
    enum mask_DEMCR
    {
        VC_CORERESET = 1 << 0,    /// 复位
        VC_MMERR = 1 << 4,        /// MMU 错误
        VC_NOCPERR = 1 << 5,      /// 没有协处理器
        VC_CHKERR = 1 << 6,       /// 检查错误
        VC_STATERR = 1 << 7,      /// 状态错误
        VC_BUSERR = 1 << 8,       /// 总线错误
        VC_INTERR = 1 << 9,       /// 中断错误
        VC_HARDERR = 1 << 10,     /// 硬件错误
        MON_EN = 1 << 16,      /// 监视错误
        MON_PEND = 1 << 17,    /// 监视挂起
        MON_STEP = 1 << 18,    /// 监视单步
        MON_REQ = 1 << 19,     /// 监视请求
        TRCENA = 1 << 24,      /// 跟踪使能
    }
    @disable this();
    /// 对齐
    align(1):
    /// 调试控制寄存器
    @Register("read-write",0x00)    uint   DHCSR;
    /// 调试异常和监视寄存器
    @Register("read-write",0x04)    uint   DCRSR;
    /// 调试数据寄存器
    @Register("read-write",0x08)    uint   DCRDR;
    /// 调试异常和监视寄存器
    @Register("read-write",0x0C)    uint   DEMCR;
}