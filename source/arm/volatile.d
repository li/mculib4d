module arm.volatile;

import arm.conf;

static if(SupportBitBand){
    public import arm.bitband:volatile;
}else{
    public import baremetal.volatile:volatile;
}

/// 8bit 操作,对应`UByte`
alias volatileUByte = volatile!(ubyte);
/// 16bit 操作,对应`UShort`
alias volatileUShort = volatile!(ushort);
/// 32bit 操作,对应`UInt`
alias volatileUInt = volatile!(uint);
