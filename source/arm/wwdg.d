module arm.wwdg;


import arm.hal;
import arm.misc;
import chip;

final abstract class D_Wwdg
{
    /// wwdg 结构定义
    struct WWDG_Struct
    {
        /// WWDG 控制寄存器
        uint CR;
        /// WWDG 配置寄存器
        uint CFR;
        /// WWDG 状态寄存器
        uint SR;
    }

    /// WWDG 初始化数据结构定义
    struct InitType_Struct
    {
        /// WWDG 预分频器值
        uint Prescaler;
        /// WWDG 窗口值
        uint Window;
        /// WWDG 计数器值
        uint Counter;
        /// WWDG 唤醒中断
    }

    /// Handle 结构定义
    struct Handle_Struct
    {
        /// WWDG 结构体
        /// WWDG_Struct* Instance;
        //WWDG Instance;
        /// WWDG 初始化结构体
        InitType_Struct Init;
    }

    Status Init(Handle_Struct* hwwdg)
    in{
        // 检查输入参数
        //assert(hwwdg !is null);
    }do{
        if(hwwdg is null)
        {
            return Status.Error;
        }
        //WriteMemory!(0x336)(cast(uint)15);
        //uint f1 = ReadMemory!(0x336,uint)();
        
        // 设置WWDG的计数器
        //hwwdg.Instance.
        return Status.None;
    }
}