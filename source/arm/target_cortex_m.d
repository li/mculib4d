module arm.target_cortex_m;

version(ARM){}else{
    static assert(0,"not supported");
}

import baremetal;
import arm.misc;
import arm;

import core.attribute;










/// 中断函数定义


//---------------------------------------------------------------------

@naked extern(C) void defaultExceptionHandler()
{
    import arm.builtins;
    
	while(true)
	{
		__wfi();
	}
}
//---------------------------------------------------------------------



