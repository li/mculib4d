module arm.scs;


import chip;
import arm.bus;
import arm.cortex_m;

static interface SCS //: MMIOBus!(CorePeripheral.SCS)
{
    private{
        enum mBase = CorePeripheral.SCS;
        enum IFName = "SCS";
    }
    package{

    }

}