module arm.conf;

/// 是否支持bitband
enum SupportBitBand = CheckTargetCPU!("cortex-m0","cortex-m0+","cortex-m3","cortex-m4","cortex-m7");

debug{
    /// 调试模式下，不使用内联函数 .
    enum __Inline = false;
}else{
    /// 发布模式下，使用内联函数
    enum __Inline = true;
}

package
{
    import std.meta:allSatisfy;
    import std.traits:isSomeString;

    /// 检查特定MCU型号模板
    template CheckTargetCPU(T...) if (allSatisfy!(isSomeString, typeof(T)) )
    {
        static if(T.length > 1)
        {
            enum CheckTargetCPU = __traits(targetCPU) == T[0] || CheckTargetCPU!(T[1..$]);
        }
        else
        {
            enum CheckTargetCPU = __traits(targetCPU) == T[0];
        }
    }
}