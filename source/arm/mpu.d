module arm.mpu;
/**
* 内存保护单元
* $(link https://blog.csdn.net/as480133937/article/details/123656394)
*/

import arm;

import arm.misc;
//import core.atomic;

import arm.builtins;

version(none):

final abstract class D_MPU
{
    /// MPU 禁止
    static void Disable()
    {
        __dmb();
        //SCB.pSCB.SHCSR &= ~SCB.SHCSR_MEMFAULTENA;
        //WriteMemory!(&MPU.pMPU.CTRL)(cast(uint)0);
    }

}

/// 内存保护单元
final abstract class D_Mpu{
    /// 外设地址
    enum PeripheralAddress = CorePeripheralAddress.MPU_Base;
    /// MPU 结构体
    struct MPU_Struct
    {
        /// MPU类型寄存器
        uint   TYPE;
        /// MPU控制寄存器
        uint   CTRL;
        /// MPU区域号寄存器
        uint   RNR;
        /// MPU区域基地址寄存器
        uint   RBAR;
        /// MPU区域属性寄存器
        uint   RASR;
        /// MPU区域基地址寄存器1
        uint   RBAR1;
        /// MPU区域属性寄存器1
        uint   RASR1;
        /// MPU区域基地址寄存器2
        uint   RBAR2;
        /// MPU区域属性寄存器2
        uint   RASR2;
        /// MPU区域基地址寄存器3
        uint   RBAR3;
        /// MPU区域属性寄存器3
        uint   RASR3;
    }
    /// 寄存器指针
    enum MPU_Struct* pMPU = cast(MPU_Struct*)PeripheralAddress;
}
