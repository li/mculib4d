module arm.adc;

/**
* ADC模块 以 stm32f401cc手册为范本
*/

import chip;


/// ADC通道设置
enum FlagADCChannel
{
    IN0 = 1 << 0,           /// ADC通道0,PA0
    IN1 = 1 << 1,           /// ADC通道1,PA1
    IN2 = 1 << 2,           /// ADC通道2,PA2
    IN3 = 1 << 3,           /// ADC通道3,PA3
    IN4 = 1 << 4,           /// ADC通道4,PA4
    IN5 = 1 << 5,           /// ADC通道5,PA5
    IN6 = 1 << 6,           /// ADC通道6,PA6
    IN7 = 1 << 7,           /// ADC通道7,PA7
    IN8 = 1 << 8,           /// ADC通道8,PC0
    IN9 = 1 << 9,           /// ADC通道9,PC1
    IN10 = 1 << 10,         /// ADC通道10,PC2
    IN11 = 1 << 11,         /// ADC通道11,PC3
    IN12 = 1 << 12,         /// ADC通道12,PC4
    IN13 = 1 << 13,         /// ADC通道13,PC5
    IN14 = 1 << 14,         /// ADC通道14,内部温度传感器
    IN15 = 1 << 15,         /// ADC通道15,内部参考电压
    TempSensor = 1 << 16,   /// 温度传感器
    Vrefint = 1 << 17,      /// 内部参考电压
    Vbat = 1 << 18,         /// 电池电压

}


/**
* 判定是否有ADC外设
*/
private enum bool hasBusPeriph = HasPeriph!("ADC1", "ADC2", "ADC3", "ADC4", "ADC5", "ADC6");
static if (hasBusPeriph):
import arm.bus;
import arm.gpio;
import arm.utils;
import arm.rcc;

/// PCLK2 分频标志
enum FlagADCPrescaler
{
    PCLK2_Div2 = 0,         /// PCLK2分频2
    PCLK2_Div4 = 1,         /// PCLK2分频4
    PCLK2_Div6 = 2,         /// PCLK2分频6
    PCLK2_Div8 = 3,         /// PCLK2分频8
}




/**
*/
static interface ADC(alias mPeri,alias Common) //: MMIOBus!mPeri
{
    /// 启用对应时钟
    private
    {
        version(none) enum ADC1_Type* mBase = Peripherals.ADC1;
        else enum mBase = mPeri;
        version(none) enum ADC_Common_Type* mCommon = Peripherals.ADC_Common;
        else enum mCommon = Common;

    }

    /// 通道
    final abstract class AdcChannel(size_t mChannel)
    {

        /// 通道
        static void Init()
        {
            /// 设置对应通道
            
        }
        /// 获取结果
        static uint GetResult()
        {
            return mBase.DR;
        }
    }


    static void Init()
    {
        /// 设置adc分频
        //mCommon.CCR &=  mCommon.mask_CCR.ADCPRE
        /// 设定分频
        mCommon.CCR.ADCPRE = FlagADCPrescaler.PCLK2_Div2;
        mBase.CR1.SCAN = 0;
        mBase.CR1.RES = 0;


        mBase.CR2.ALIGN =0;


        RCC.Power!("ADC1EN")(1);
        
        

    }

}



//Peripherals.ADC_Common.
alias ADC1 = ADC!(Peripherals.ADC1, Peripherals.ADC_Common);

