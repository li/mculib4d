module baremetal.aeabi;

// 内存部分
pragma(inline,true)
extern(C)
{
import baremetal.stdlib;
    void __aeabi_memset(void *dest, size_t n, int c)
    {
        memset(dest, cast(ubyte)(c & 0xFF), n);
    };
    void __aeabi_memset4(void *dest, size_t n, int c) => __aeabi_memset(dest, n, c);
    void __aeabi_memset8(void *dest, size_t n, int c) => __aeabi_memset(dest, n, c);

    void __aeabi_memclr(void *dest, size_t n) =>__aeabi_memset(dest, n, 0x00); 
    void __aeabi_memclr4(void *dest, size_t n) => __aeabi_memset(dest, n, 0x00); 
    void __aeabi_memclr8(void *dest, size_t n) => __aeabi_memset(dest, n, 0x00); 

    void __aeabi_memcpy(void *dest, const void *src, size_t n) {
        memcpy(dest, src, n); 
    }
    void __aeabi_memcpy4(void *dest, void *src, size_t n) =>__aeabi_memcpy(dest, src, n); 
    void __aeabi_memcpy8(void *dest, void *src, size_t n) =>__aeabi_memcpy(dest, src, n); 

    
}