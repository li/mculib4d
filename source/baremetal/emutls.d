module baremetal.emutls;

//version(NONE):

struct emutls_object
{
    /// 大小
    size_t size;
    /// 对齐
    size_t align_;
    union
    {
        /// 偏移
        size_t offset;
        /// 指针
        void* ptr;
    }
    /// 原始数据
    ubyte* templ;
}

struct emutls_array
{
  size_t skip_destructor_rounds;
  size_t size;
  //void **data[];

};


extern(C):

/*
    获取一块TLS内存地址.
*/
void * __emutls_get_address(shared emutls_object* obj)
{
    if(obj.ptr is null)
    {

    }
    return null;
}

void __emutls_register_common(emutls_object* obj, size_t size, size_t align_, ubyte* templ) @nogc
{
    if (obj.size < size)
    {
        obj.size = size;
        obj.templ = null;
    }
    if (obj.align_ < align_)
        obj.align_ = align_;
    if (templ && size == obj.size)
        obj.templ = templ;
}