module hook.hook_object;
//pragma(LDC_no_moduleinfo);
//pragma(LDC_no_typeinfo)
/**
* 给druntime的Object类添加一个hook,
*/


//alias Object = hook_obj;


//pragma(msg,__traits(toType, "_D6object6Object6toHashMFNbNeZk"));

/*
pragma(mangle,Object.mangleof)
class hook_Object{
    override string toString()
    {
        return "";
    }
    override size_t toHash()
    {
        //size_t addr = cast(size_t) (cast(void*) this);

        //return addr ^ (addr >>> 4);
        return 0;
    }
    override int opCmp(Object o)
    {
        return 0;
    }
    override bool opEquals(Object o)
    {
        return false;
    }
}
*/


extern(C) Object _d_allocclass(TypeInfo_Class ti) 
{
    import baremetal.malloc;
    import baremetal.stdlib;
    
    auto buf = malloc(ti.m_init.length);
    memcpy(buf,ti.m_init.ptr,ti.m_init.length);
    return cast(Object)buf;
}


extern(C) void _d_array_slice_copy(void* dst, size_t dstlen, void* src, size_t srclen, size_t elemsz)
{
    import baremetal.stdlib;
	auto d = cast(ubyte*) dst;
	auto s = cast(ubyte*) src;
	auto len = dstlen * elemsz;
    //memcpy(d,s,len);
    
	while(len) 
	{
		*d = *s;
		d++;
		s++;
		len--;
	}
    
}
