module app;


//import baremetal;
import chip;
//
import arm;
import arm.scb;

import ext.ws2812;
import baremetal.hook;

enum ppp = cast(PortMask)(PortMask.Pin13|PortMask.Pin14);

alias PortC = GPIO!(Peripherals.GPIOC,ppp);

alias Port_IIC1 = GPIO!(Peripherals.GPIOB,PortMask.Pin8|PortMask.Pin9);

alias PortB = GPIO!(Peripherals.GPIOB,PortMask.PortAll);

alias PortA = GPIO!(Peripherals.GPIOA,PortMask.PortAll);

alias clockOut = PortA.Pin!(8);


alias Led_Pin = PortC.Pin!(13);
alias Ws2812 = PortC.Pin!(14);


/*
* ADC
* SYStick <*>
* GPIO <*...>
* USART
* DMA
* Tim
* I2c
*/



int a1 = 0xFCCFFCCF;
int a1_0 = 0xEFFEEFFE;
int a1_1 = 66;
int a1_2 = 66;
int a11;
static int a2 = 66;
static int a22;

shared int a4 = 66;
__gshared int a5 = 66;
immutable int a3 = 66;


/// 时钟设置
void Hal_Init()
{
	// 设置权限
	//
	//SCB.Priority!(IRQn_Core.SysTick_IRQn)(0);
	//Peripherals
	//HAL.InitTick(STK.Tick_Init_Priority);
	
}

void SystemClock_Config()
{
	__gshared uint a6;
	shared(int) a757;
	auto pa1 =&a1;

	auto pa1_0 =&a1_0;
	auto pa1_1 =&a1_1;
	auto pa1_2 =&a1_2;

	auto pa2 = &a2;
	auto pa4 = &a4;
	auto pa5 = &a5;

	auto pa11 = &a11;
	auto pa22 = &a22;


	if(*pa1){
		*pa1 =a3;
	}

	if(*pa1_0){
		*pa1_0 =a3;
	}

	if(*pa1_1){
		*pa1_1 =a3;
	}

	if(*pa1_2){
		*pa1_2 =a3;
	}

	if(*pa2)
	{
		*pa2 = 66;
	}

	if(*pa4)
	{
		*pa4 = 100;
	}

	if(*pa5)
	{
		*pa5 = 63;
	}
	
	if(*pa11)
	{
		*pa11 = 33;
	}

	if(*pa22)
	{
		*pa22 = 23;
	}

	a6 = a3;
	a757 = a3;
	//RCC.Mmio_
	//D_RCC.PWREN_Enable();
	//RCC.isEnabled!("")();
	//D_PW
	//D_PWR.VoltageScale(VOSFlag.Scale_2);
	//RCC.PWREN = true;
	//RCC.Power!("PWREN") = true;
	
}





/// GPIO初始化
void GPIO_Init()
{
	PortA.Enable();
	PortB.Enable();
	PortC.Enable();
	Led_Pin.PinMode = PinModer.output;
	Led_Pin.PinSpeed = PinOSpeedr.veryHigh;
	Led_Pin.PinType = PinOTyper.pushPull;

	Ws2812.PinMode = PinModer.output;
	Ws2812.PinSpeed = PinOSpeedr.veryHigh;
	Ws2812.PinType = PinOTyper.pushPull;

	Port_IIC1.PortMode = PinModer.alternate;
	Port_IIC1.PortSpeed = PinOSpeedr.veryHigh;
	Port_IIC1.PortPull = PinPUPDR.none;
	Port_IIC1.PortAlter = PinAlternate.AF4;

	clockOut.PinMode = PinModer.alternate;
	clockOut.PinSpeed = PinOSpeedr.veryHigh;
	clockOut.PinType = PinOTyper.pushPull;
	clockOut.PinAlter = PinAlternate.AF0;
	clockOut.PinPull = PinPUPDR.pullUp;
	
}
extern(C)
__gshared static uint ttaa = 0;

shared int v61 = 998;



static void test()
{
	
	import arm.bus;
	
	import arm.i2c;
	/*
	enum ffff = CalcClockConfig(84_000_000,true);
	pragma(msg,"Clock:",ffff);
*/
	

	enum string ss1 = "cc1";
	
	alias I2C1Type1 = I2C_Master!(Peripherals.I2C1);

	I2C1Type1 iic1 = CreateObject!(I2C1Type1)();

	enum conf = iic1.I2C_Config();
	iic1.Config!(conf)();

	//iic1.Busy();

	
	
	//iic1.writes("cccc".ptr,2);
	//iic1.
	

	MemoryStream ccc = iic1;
	//ccc.writes("cccc".ptr,2);

	DestroyObject(iic1);
	
	
}

alias ww2812 = IWS2812_Soft!(Ws2812,24);

extern(C) 
void mloop()
{
	test();

	//Hal_Init();
	SystemClock_Config();
	GPIO_Init();
	

	auto v = Flash.isBusy();

	auto v1 = Led_Pin.PinRW;
	

	v61 = v1;

	//auto ww2812 = CreateObject!(DWS2812!(Ws2812))();



	/*
	shared( uint*) pLed =cast(uint*)Led_Pin.Band_ODR;
	enum ppled = cast(shared(byte*))Led_Pin.Band_ODR;
	enum size_t adad = Led_Pin.Band_ODR;
	*/

	RCC.ConfigMCO1(RCC.FlagMCOPresc.Div_4,RCC.FlagMCOClk1.PLL);
	//ww2812.init();
	GRB_Type rgb;
	uint idx;


	
	Led_Pin.PinSet();
	ww2812.Init();
	//auto prgb = cast(uint*)&rgb;

	while(1)
	{
		
		rgb.value ++;
		if(rgb.value >= 0xFF){
			rgb.value = 0;
			idx++;
		}
		
		

		ww2812.SetPoint(idx%24,rgb);



		
		Led_Pin.PinClr();
		Led_Pin.PinSet();
		//ww2812.write(RR,GG,BB,idx%24);
		//ww2812.test();
		//Led_Pin.Direct_PinToggle();
		ww2812.Update2(true);

		Utils.Delay(10);
		//__nop();
		//Led_Pin.PinToggle();
		//Led_Pin.Direct_PinToggle();		
	}
	
}
import arm.builtins;




version(NONE):
//import std.stdio;





void Error_Handler()
{
	import arm.builtins;
	disable_irq();
	while(1)
	{
		//FLASH.stringof
		
	}
}



// trace
//semihostingInvoke(0x05, "teststr");



/**
* 项目基础函数需求
* 1. 读取key状态,读取触发时间,累计触发次数,重置触发次数
* 2. 读取霍尔传感器状态,读取霍尔传感器触发时间,累计触发次数,重置触发次数
* 3. 读取正交传感器状态,判断方向,读取加速度
* 4. 读取电源传感器状态,读取电源电压,反馈电压数据
* 5. 读取电机状态,通过i2c读取电机状态,转速,电流,温度
* 7. 485通讯
* 8. Ws2812控制
* 9. 回调继电器控制
*/

/// I2c通讯



//enum vv1 = mixin chkbus!();

//enum ccode = isPeriphBus_Code!("ADC3");

//enum ay = __traits(isFinalClass,__traits(compiles, ADC3)?mixin("ADC3"):null);

//enum vv1 = mixin(ccode);

//mixin Mk1!("adc1","adc2","adc3");


//pragma(msg,"code_:",ccode);
//pragma(msg,"APP_cNameay:",ay);
//pragma(msg,"APP_cNameT:",vv1);
//pragma(msg,"APP_cNameC:",CB2);


/*
enum str1 = "__traits(compiles, ADC6)";
pragma(msg,"APP_cName:",str1);
pragma(msg,"APP_cName:",mixin(str1));
*/

