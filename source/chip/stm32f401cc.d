/*****************************************************************************************
*	STM32F401
*/
/*
struct Conf {
	enum string SVDName                  = `STM32F401`;
	enum string SVDVersion               = `1.1`;
	enum size_t SVDUnitBits              = 8;
	enum size_t SVDWidth                 = 32;
	enum size_t SVDSize                  = 0x20;
	enum size_t SVDresetValue            = 0x0;
	enum size_t SVDresetMask             = 0xFFFFFFFF;
	enum string SVDCpuName               = `CM4`;
	enum string SVDCpuRevision           = `r1p0`;
	enum string SVDCpuEndian             = `little`;
	enum bool SVDCpuMpuPresent           = false;
	enum bool SVDCpuFpuPresent           = false;
	enum size_t SVDCpuNvicPrioBits       = 3;
	enum bool SVDCpuVendorSystickConfig  = false;
}
*/
module chip.stm32f401cc;
import arm.cortex_m4;
import baremetal;

public{
    import chip.stm32f401;
}
/// HSI RC默认频率,内部振荡器
enum HSI_RC_Frequency           = 16_000_000;
/// LSI RC 默认频率
enum LSI_RC_Frequency           = 32_000;


/// 时间参数限定
enum ClockLimit
{
    PLLM_MIN = 2, /// PLLM最小值
    PLLM_MAX = 63, /// PLLM最大值
    PLLM_Frequency_Max = 2_100_000, /// PLLM最大频率
    PLLM_Frequency_Min = 950_000, /// PLLM最小频率
    PLLN_MIN = 50, /// PLLN最小值
    PLLN_MAX = 432, /// PLLN最大值
    PLLN_Frequency_Max = 432_000_000, /// PLLN最大频率
    PLLN_Frequency_Min = 100_000_000, /// PLLN最小频率
    PLLP_MIN = 2, /// PLLP最小值
    PLLP_MAX = 8, /// PLLP最大值
    PLLP_Frequency_Max = 84_000_000, /// PLLP最大频率
    PLLP_Frequency_Min = 24_000_000, /// PLLP最小频率
    PLLQ_MIN = 2, /// PLLQ最小值
    PLLQ_MAX = 15, /// PLLQ最大值
    PLLQ_Frequency_Max = 75_000_000, /// PLLQ最大频率
    PLLQ_Frequency_Min = 0, /// PLLQ最小频率

    SYSCLK_Frequency_Max = 84_000_000, /// SYSCLK最大频率
    HCLK_Frequency_Max = 84_000_000, /// HCLK最大频率
    HCLK_Frequency_Min = 14_200_000, /// HCLK最小频率

    USB_Frequency = 48_000_000, /// USB频率
    USB_Frequency_Max = 48_120_000, /// USB最大频率
    USB_Frequency_Min = 47_880_000, /// USB最小频率
}


/// 外设寄存器访问地址

/**
* stm32f401cc 向量表
*/
/// 中断向量表

extern(C)
enum VectorFunc[] Cortex_M4 =[
    IRQn_Ext.WWDG:&WWDG_IRQHandler,
    IRQn_Ext.PVD:&PVD_IRQHandler,
    IRQn_Ext.TAMP_STAMP:&TAMP_STAMP_IRQHandler,
    IRQn_Ext.RTC_WKUP:&RTC_WKUP_IRQHandler,
    IRQn_Ext.FLASH:&FLASH_IRQHandler,
    IRQn_Ext.RCC:&RCC_IRQHandler,
    IRQn_Ext.EXTI0:&EXTI0_IRQHandler,
    IRQn_Ext.EXTI1:&EXTI1_IRQHandler,
    IRQn_Ext.EXTI2:&EXTI2_IRQHandler,
    IRQn_Ext.EXTI3:&EXTI3_IRQHandler,
    IRQn_Ext.EXTI4:&EXTI4_IRQHandler,
    IRQn_Ext.DMA1_Stream0:&DMA1_Stream0_IRQHandler,
    IRQn_Ext.DMA1_Stream1:&DMA1_Stream1_IRQHandler,
    IRQn_Ext.DMA1_Stream2:&DMA1_Stream2_IRQHandler,
    IRQn_Ext.DMA1_Stream3:&DMA1_Stream3_IRQHandler,
    IRQn_Ext.DMA1_Stream4:&DMA1_Stream4_IRQHandler,
    IRQn_Ext.DMA1_Stream5:&DMA1_Stream5_IRQHandler,
    IRQn_Ext.DMA1_Stream6:&DMA1_Stream6_IRQHandler,
    IRQn_Ext.ADC:&ADC_IRQHandler,
    IRQn_Ext.EXTI9_5:&EXTI9_5_IRQHandler,
    IRQn_Ext.TIM1_BRK_TIM9:&TIM1_BRK_TIM9_IRQHandler,
    IRQn_Ext.TIM1_UP_TIM10:&TIM1_UP_TIM10_IRQHandler,
    IRQn_Ext.TIM1_TRG_COM_TIM11:&TIM1_TRG_COM_TIM11_IRQHandler,
    IRQn_Ext.TIM1_CC:&TIM1_CC_IRQHandler,
    IRQn_Ext.TIM2:&TIM2_IRQHandler,
    IRQn_Ext.TIM3:&TIM3_IRQHandler,
    IRQn_Ext.TIM4:&TIM4_IRQHandler,
    IRQn_Ext.I2C1_EV:&I2C1_EV_IRQHandler,
    IRQn_Ext.I2C1_ER:&I2C1_ER_IRQHandler,
    IRQn_Ext.I2C2_EV:&I2C2_EV_IRQHandler,
    IRQn_Ext.I2C2_ER:&I2C2_ER_IRQHandler,
    IRQn_Ext.SPI1:&SPI1_IRQHandler,
    IRQn_Ext.SPI2:&SPI2_IRQHandler,
    IRQn_Ext.USART1:&USART1_IRQHandler,
    IRQn_Ext.USART2:&USART2_IRQHandler,
    IRQn_Ext.EXTI15_10:&EXTI15_10_IRQHandler,
    IRQn_Ext.RTC_Alarm:&RTC_Alarm_IRQHandler,
    IRQn_Ext.OTG_FS_WKUP:&OTG_FS_WKUP_IRQHandler,
    IRQn_Ext.DMA1_Stream7:&DMA1_Stream7_IRQHandler,
    IRQn_Ext.SDIO:&SDIO_IRQHandler,
    IRQn_Ext.TIM5:&TIM5_IRQHandler,
    IRQn_Ext.SPI3:&SPI3_IRQHandler,
    IRQn_Ext.DMA2_Stream0:&DMA2_Stream0_IRQHandler,
    IRQn_Ext.DMA2_Stream1:&DMA2_Stream1_IRQHandler,
    IRQn_Ext.DMA2_Stream2:&DMA2_Stream2_IRQHandler,
    IRQn_Ext.DMA2_Stream3:&DMA2_Stream3_IRQHandler,
    IRQn_Ext.DMA2_Stream4:&DMA2_Stream4_IRQHandler,
    IRQn_Ext.OTG_FS:&OTG_FS_IRQHandler,
    IRQn_Ext.DMA2_Stream5:&DMA2_Stream5_IRQHandler,
    IRQn_Ext.DMA2_Stream6:&DMA2_Stream6_IRQHandler,
    IRQn_Ext.DMA2_Stream7:&DMA2_Stream7_IRQHandler,
    IRQn_Ext.USART6:&USART6_IRQHandler,
    IRQn_Ext.I2C3_EV:&I2C3_EV_IRQHandler,
    IRQn_Ext.I2C3_ER:&I2C3_ER_IRQHandler,
    IRQn_Ext.FPU:&FPU_IRQHandler,
];



