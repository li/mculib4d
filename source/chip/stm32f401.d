module chip.stm32f401;

//import arm.target_cortex_m4;
import baremetal;
import core.attribute;




/// Peripheral Access Statement
static struct Peripherals
{
	@disable this();
	enum DBG_Type* DBG = MMIO!(0xE0042000,DBG_Type);		/// Debug support
	enum DMA2_Type* DMA2 = MMIO!(0x40026400,DMA2_Type);		/// DMA controller
	enum DMA1_Type* DMA1 = MMIO!(0x40026000,DMA1_Type);		/// DMA controller
	enum RCC_Type* RCC = MMIO!(0x40023800,RCC_Type);		/// Reset and clock control
	enum GPIOH_Type* GPIOH = MMIO!(0x40021C00,GPIOH_Type);		/// General-purpose I/Os
	enum GPIOE_Type* GPIOE = MMIO!(0x40021000,GPIOE_Type);		/// General-purpose I/Os
	enum GPIOD_Type* GPIOD = MMIO!(0X40020C00,GPIOD_Type);		/// General-purpose I/Os
	enum GPIOC_Type* GPIOC = MMIO!(0x40020800,GPIOC_Type);		/// General-purpose I/Os
	enum GPIOB_Type* GPIOB = MMIO!(0x40020400,GPIOB_Type);		/// General-purpose I/Os
	enum GPIOA_Type* GPIOA = MMIO!(0x40020000,GPIOA_Type);		/// General-purpose I/Os
	enum SYSCFG_Type* SYSCFG = MMIO!(0x40013800,SYSCFG_Type);		/// System configuration controller
	enum SPI1_Type* SPI1 = MMIO!(0x40013000,SPI1_Type);		/// Serial peripheral interface
	enum SPI2_Type* SPI2 = MMIO!(0x40003800,SPI2_Type);		/// Serial peripheral interface
	enum SPI3_Type* SPI3 = MMIO!(0x40003C00,SPI3_Type);		/// Serial peripheral interface
	enum I2S2ext_Type* I2S2ext = MMIO!(0x40003400,I2S2ext_Type);		/// Serial peripheral interface
	enum I2S3ext_Type* I2S3ext = MMIO!(0x40004000,I2S3ext_Type);		/// Serial peripheral interface
	enum SDIO_Type* SDIO = MMIO!(0x40012C00,SDIO_Type);		/// Secure digital input/output interface
	enum ADC1_Type* ADC1 = MMIO!(0x40012000,ADC1_Type);		/// Analog-to-digital converter
	enum USART6_Type* USART6 = MMIO!(0x40011400,USART6_Type);		/// Universal synchronous asynchronous receiver transmitter
	enum USART1_Type* USART1 = MMIO!(0x40011000,USART1_Type);		/// Universal synchronous asynchronous receiver transmitter
	enum USART2_Type* USART2 = MMIO!(0x40004400,USART2_Type);		/// Universal synchronous asynchronous receiver transmitter
	enum PWR_Type* PWR = MMIO!(0x40007000,PWR_Type);		/// Power control
	enum I2C3_Type* I2C3 = MMIO!(0x40005C00,I2C3_Type);		/// Inter-integrated circuit
	enum I2C2_Type* I2C2 = MMIO!(0x40005800,I2C2_Type);		/// Inter-integrated circuit
	enum I2C1_Type* I2C1 = MMIO!(0x40005400,I2C1_Type);		/// Inter-integrated circuit
	enum IWDG_Type* IWDG = MMIO!(0x40003000,IWDG_Type);		/// Independent watchdog
	enum WWDG_Type* WWDG = MMIO!(0x40002C00,WWDG_Type);		/// Window watchdog
	enum RTC_Type* RTC = MMIO!(0x40002800,RTC_Type);		/// Real-time clock
	enum TIM1_Type* TIM1 = MMIO!(0x40010000,TIM1_Type);		/// Advanced-timers
	enum TIM2_Type* TIM2 = MMIO!(0x40000000,TIM2_Type);		/// General purpose timers
	enum TIM3_Type* TIM3 = MMIO!(0x40000400,TIM3_Type);		/// General purpose timers
	enum TIM4_Type* TIM4 = MMIO!(0x40000800,TIM4_Type);		/// General purpose timers
	enum TIM5_Type* TIM5 = MMIO!(0x40000C00,TIM5_Type);		/// General-purpose-timers
	enum TIM9_Type* TIM9 = MMIO!(0x40014000,TIM9_Type);		/// General purpose timers
	enum TIM10_Type* TIM10 = MMIO!(0x40014400,TIM10_Type);		/// General-purpose-timers
	enum TIM11_Type* TIM11 = MMIO!(0x40014800,TIM11_Type);		/// General-purpose-timers
	enum CRC_Type* CRC = MMIO!(0x40023000,CRC_Type);		/// Cryptographic processor
	enum OTG_FS_GLOBAL_Type* OTG_FS_GLOBAL = MMIO!(0x50000000,OTG_FS_GLOBAL_Type);		/// USB on the go full speed
	enum OTG_FS_HOST_Type* OTG_FS_HOST = MMIO!(0x50000400,OTG_FS_HOST_Type);		/// USB on the go full speed
	enum OTG_FS_DEVICE_Type* OTG_FS_DEVICE = MMIO!(0x50000800,OTG_FS_DEVICE_Type);		/// USB on the go full speed
	enum OTG_FS_PWRCLK_Type* OTG_FS_PWRCLK = MMIO!(0x50000E00,OTG_FS_PWRCLK_Type);		/// USB on the go full speed
	enum FLASH_Type* FLASH = MMIO!(0x40023C00,FLASH_Type);		/// FLASH
	enum EXTI_Type* EXTI = MMIO!(0x40013C00,EXTI_Type);		/// External interrupt/event controller
	enum NVIC_Type* NVIC = MMIO!(0xE000E000,NVIC_Type);		/// Nested Vectored Interrupt Controller
	enum ADC_Common_Type* ADC_Common = MMIO!(0x40012300,ADC_Common_Type);		/// ADC common registers
	enum FPU_Type* FPU = MMIO!(0xE000EF34,FPU_Type);		/// Floting point unit
	enum MPU_Type* MPU = MMIO!(0xE000ED90,MPU_Type);		/// Memory protection unit
	enum STK_Type* STK = MMIO!(0xE000E010,STK_Type);		/// SysTick timer
	enum SCB_Type* SCB = MMIO!(0xE000ED00,SCB_Type);		/// System control block
}
enum IRQn_Ext{
	WWDG = 0,	 /// Window Watchdog interrupt
	PVD = 1,	 /// PVD through EXTI line detection interrupt
	TAMP_STAMP = 2,	 /// Tamper and TimeStamp interrupts through the EXTI line
	RTC_WKUP = 3,	 /// RTC Wakeup interrupt through the EXTI line
	FLASH = 4,	 /// FLASH global interrupt
	RCC = 5,	 /// RCC global interrupt
	EXTI0 = 6,	 /// EXTI Line0 interrupt
	EXTI1 = 7,	 /// EXTI Line1 interrupt
	EXTI2 = 8,	 /// EXTI Line2 interrupt
	EXTI3 = 9,	 /// EXTI Line3 interrupt
	EXTI4 = 10,	 /// EXTI Line4 interrupt
	DMA1_Stream0 = 11,	 /// DMA1 Stream0 global interrupt
	DMA1_Stream1 = 12,	 /// DMA1 Stream1 global interrupt
	DMA1_Stream2 = 13,	 /// DMA1 Stream2 global interrupt
	DMA1_Stream3 = 14,	 /// DMA1 Stream3 global interrupt
	DMA1_Stream4 = 15,	 /// DMA1 Stream4 global interrupt
	DMA1_Stream5 = 16,	 /// DMA1 Stream5 global interrupt
	DMA1_Stream6 = 17,	 /// DMA1 Stream6 global interrupt
	ADC = 18,	 /// ADC1 global interrupt
	EXTI9_5 = 23,	 /// EXTI Line[9:5] interrupts
	TIM1_BRK_TIM9 = 24,	 /// TIM1 Break interrupt and TIM9 global interrupt
	TIM1_UP_TIM10 = 25,	 /// TIM1 Update interrupt and TIM10 global interrupt
	TIM1_TRG_COM_TIM11 = 26,	 /// TIM1 Trigger and Commutation interrupts and TIM11 global interrupt
	TIM1_CC = 27,	 /// TIM1 Capture Compare interrupt
	TIM2 = 28,	 /// TIM2 global interrupt
	TIM3 = 29,	 /// TIM3 global interrupt
	TIM4 = 30,	 /// TIM4 global interrupt
	I2C1_EV = 31,	 /// I2C1 event interrupt
	I2C1_ER = 32,	 /// I2C1 error interrupt
	I2C2_EV = 33,	 /// I2C2 event interrupt
	I2C2_ER = 34,	 /// I2C2 error interrupt
	SPI1 = 35,	 /// SPI1 global interrupt
	SPI2 = 36,	 /// SPI2 global interrupt
	USART1 = 37,	 /// USART1 global interrupt
	USART2 = 38,	 /// USART2 global interrupt
	EXTI15_10 = 40,	 /// EXTI Line[15:10] interrupts
	RTC_Alarm = 41,	 /// RTC Alarms (A and B) through EXTI line interrupt
	OTG_FS_WKUP = 42,	 /// USB On-The-Go FS Wakeup through EXTI line interrupt
	DMA1_Stream7 = 47,	 /// DMA1 Stream7 global interrupt
	SDIO = 49,	 /// SDIO global interrupt
	TIM5 = 50,	 /// TIM5 global interrupt
	SPI3 = 51,	 /// SPI3 global interrupt
	DMA2_Stream0 = 56,	 /// DMA2 Stream0 global interrupt
	DMA2_Stream1 = 57,	 /// DMA2 Stream1 global interrupt
	DMA2_Stream2 = 58,	 /// DMA2 Stream2 global interrupt
	DMA2_Stream3 = 59,	 /// DMA2 Stream3 global interrupt
	DMA2_Stream4 = 60,	 /// DMA2 Stream4 global interrupt
	OTG_FS = 67,	 /// USB On The Go FS global interrupt
	DMA2_Stream5 = 68,	 /// DMA2 Stream5 global interrupt
	DMA2_Stream6 = 69,	 /// DMA2 Stream6 global interrupt
	DMA2_Stream7 = 70,	 /// DMA2 Stream7 global interrupt
	USART6 = 71,	 /// USART6 global interrupt
	I2C3_EV = 72,	 /// I2C3 event interrupt
	I2C3_ER = 73,	 /// I2C3 error interrupt
	FPU = 81,	 /// FPU interrupt
}
@weak
pragma(LDC_extern_weak)
 extern(C)  __gshared
{
	void WWDG_IRQHandler();		/// Window Watchdog interrupt Interrupts:0
	void PVD_IRQHandler();		/// PVD through EXTI line detection interrupt Interrupts:1
	void TAMP_STAMP_IRQHandler();		/// Tamper and TimeStamp interrupts through the EXTI line Interrupts:2
	void RTC_WKUP_IRQHandler();		/// RTC Wakeup interrupt through the EXTI line Interrupts:3
	void FLASH_IRQHandler();		/// FLASH global interrupt Interrupts:4
	void RCC_IRQHandler();		/// RCC global interrupt Interrupts:5
	void EXTI0_IRQHandler();		/// EXTI Line0 interrupt Interrupts:6
	void EXTI1_IRQHandler();		/// EXTI Line1 interrupt Interrupts:7
	void EXTI2_IRQHandler();		/// EXTI Line2 interrupt Interrupts:8
	void EXTI3_IRQHandler();		/// EXTI Line3 interrupt Interrupts:9
	void EXTI4_IRQHandler();		/// EXTI Line4 interrupt Interrupts:10
	void DMA1_Stream0_IRQHandler();		/// DMA1 Stream0 global interrupt Interrupts:11
	void DMA1_Stream1_IRQHandler();		/// DMA1 Stream1 global interrupt Interrupts:12
	void DMA1_Stream2_IRQHandler();		/// DMA1 Stream2 global interrupt Interrupts:13
	void DMA1_Stream3_IRQHandler();		/// DMA1 Stream3 global interrupt Interrupts:14
	void DMA1_Stream4_IRQHandler();		/// DMA1 Stream4 global interrupt Interrupts:15
	void DMA1_Stream5_IRQHandler();		/// DMA1 Stream5 global interrupt Interrupts:16
	void DMA1_Stream6_IRQHandler();		/// DMA1 Stream6 global interrupt Interrupts:17
	void ADC_IRQHandler();		/// ADC1 global interrupt Interrupts:18
	void EXTI9_5_IRQHandler();		/// EXTI Line[9:5] interrupts Interrupts:23
	void TIM1_BRK_TIM9_IRQHandler();		/// TIM1 Break interrupt and TIM9 global interrupt Interrupts:24
	void TIM1_UP_TIM10_IRQHandler();		/// TIM1 Update interrupt and TIM10 global interrupt Interrupts:25
	void TIM1_TRG_COM_TIM11_IRQHandler();		/// TIM1 Trigger and Commutation interrupts and TIM11 global interrupt Interrupts:26
	void TIM1_CC_IRQHandler();		/// TIM1 Capture Compare interrupt Interrupts:27
	void TIM2_IRQHandler();		/// TIM2 global interrupt Interrupts:28
	void TIM3_IRQHandler();		/// TIM3 global interrupt Interrupts:29
	void TIM4_IRQHandler();		/// TIM4 global interrupt Interrupts:30
	void I2C1_EV_IRQHandler();		/// I2C1 event interrupt Interrupts:31
	void I2C1_ER_IRQHandler();		/// I2C1 error interrupt Interrupts:32
	void I2C2_EV_IRQHandler();		/// I2C2 event interrupt Interrupts:33
	void I2C2_ER_IRQHandler();		/// I2C2 error interrupt Interrupts:34
	void SPI1_IRQHandler();		/// SPI1 global interrupt Interrupts:35
	void SPI2_IRQHandler();		/// SPI2 global interrupt Interrupts:36
	void USART1_IRQHandler();		/// USART1 global interrupt Interrupts:37
	void USART2_IRQHandler();		/// USART2 global interrupt Interrupts:38
	void EXTI15_10_IRQHandler();		/// EXTI Line[15:10] interrupts Interrupts:40
	void RTC_Alarm_IRQHandler();		/// RTC Alarms (A and B) through EXTI line interrupt Interrupts:41
	void OTG_FS_WKUP_IRQHandler();		/// USB On-The-Go FS Wakeup through EXTI line interrupt Interrupts:42
	void DMA1_Stream7_IRQHandler();		/// DMA1 Stream7 global interrupt Interrupts:47
	void SDIO_IRQHandler();		/// SDIO global interrupt Interrupts:49
	void TIM5_IRQHandler();		/// TIM5 global interrupt Interrupts:50
	void SPI3_IRQHandler();		/// SPI3 global interrupt Interrupts:51
	void DMA2_Stream0_IRQHandler();		/// DMA2 Stream0 global interrupt Interrupts:56
	void DMA2_Stream1_IRQHandler();		/// DMA2 Stream1 global interrupt Interrupts:57
	void DMA2_Stream2_IRQHandler();		/// DMA2 Stream2 global interrupt Interrupts:58
	void DMA2_Stream3_IRQHandler();		/// DMA2 Stream3 global interrupt Interrupts:59
	void DMA2_Stream4_IRQHandler();		/// DMA2 Stream4 global interrupt Interrupts:60
	void OTG_FS_IRQHandler();		/// USB On The Go FS global interrupt Interrupts:67
	void DMA2_Stream5_IRQHandler();		/// DMA2 Stream5 global interrupt Interrupts:68
	void DMA2_Stream6_IRQHandler();		/// DMA2 Stream6 global interrupt Interrupts:69
	void DMA2_Stream7_IRQHandler();		/// DMA2 Stream7 global interrupt Interrupts:70
	void USART6_IRQHandler();		/// USART6 global interrupt Interrupts:71
	void I2C3_EV_IRQHandler();		/// I2C3 event interrupt Interrupts:72
	void I2C3_ER_IRQHandler();		/// I2C3 error interrupt Interrupts:73
	void FPU_IRQHandler();		/// FPU interrupt Interrupts:81
}
/// Debug support address:0xE0042000
struct DBG_Type{
	@disable this();
	enum Interrupt {
		FPU = 81,		/// FPU interrupt
	};
	/// IDCODE
	enum mask_DBGMCU_IDCODE {
		DEV_ID = 0x00000FFF,		/// DEV_ID offset:0 width:12 
		REV_ID = 0xFFFF0000,		/// REV_ID offset:16 width:16 
	}
	/// Control Register
	enum mask_DBGMCU_CR {
		DBG_SLEEP = 0x00000001,		/// DBG_SLEEP offset:0 width:1 
		DBG_STOP = 0x00000002,		/// DBG_STOP offset:1 width:1 
		DBG_STANDBY = 0x00000004,		/// DBG_STANDBY offset:2 width:1 
		TRACE_IOEN = 0x00000020,		/// TRACE_IOEN offset:5 width:1 
		TRACE_MODE = 0x000000C0,		/// TRACE_MODE offset:6 width:2 
	}
	/// Debug MCU APB1 Freeze registe
	enum mask_DBGMCU_APB1_FZ {
		DBG_TIM2_STOP = 0x00000001,		/// DBG_TIM2_STOP offset:0 width:1 
		DBG_TIM3_STOP = 0x00000002,		/// DBG_TIM3 _STOP offset:1 width:1 
		DBG_TIM4_STOP = 0x00000004,		/// DBG_TIM4_STOP offset:2 width:1 
		DBG_TIM5_STOP = 0x00000008,		/// DBG_TIM5_STOP offset:3 width:1 
		DBG_WWDG_STOP = 0x00000800,		/// DBG_WWDG_STOP offset:11 width:1 
		DBG_IWDEG_STOP = 0x00001000,		/// DBG_IWDEG_STOP offset:12 width:1 
		DBG_I2C1_SMBUS_TIMEOUT = 0x00200000,		/// DBG_J2C1_SMBUS_TIMEOUT offset:21 width:1 
		DBG_I2C2_SMBUS_TIMEOUT = 0x00400000,		/// DBG_J2C2_SMBUS_TIMEOUT offset:22 width:1 
		DBG_I2C3SMBUS_TIMEOUT = 0x00800000,		/// DBG_J2C3SMBUS_TIMEOUT offset:23 width:1 
	}
	/// Debug MCU APB2 Freeze registe
	enum mask_DBGMCU_APB2_FZ {
		DBG_TIM1_STOP = 0x00000001,		/// TIM1 counter stopped when core is halted offset:0 width:1 
		DBG_TIM9_STOP = 0x00010000,		/// TIM9 counter stopped when core is halted offset:16 width:1 
		DBG_TIM10_STOP = 0x00020000,		/// TIM10 counter stopped when core is halted offset:17 width:1 
		DBG_TIM11_STOP = 0x00040000,		/// TIM11 counter stopped when core is halted offset:18 width:1 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_DBGMCU_IDCODE,uint) DBGMCU_IDCODE;		/// IDCODE offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_DBGMCU_CR,uint) DBGMCU_CR;		/// Control Register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_DBGMCU_APB1_FZ,uint) DBGMCU_APB1_FZ;		/// Debug MCU APB1 Freeze registe offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DBGMCU_APB2_FZ,uint) DBGMCU_APB2_FZ;		/// Debug MCU APB2 Freeze registe offset:0x0000000C
}
/// DMA controller address:0x40026400
struct DMA2_Type{
	@disable this();
	enum Interrupt {
		DMA2_Stream4 = 60,		/// DMA2 Stream4 global interrupt
		DMA2_Stream0 = 56,		/// DMA2 Stream0 global interrupt
		DMA2_Stream6 = 69,		/// DMA2 Stream6 global interrupt
		DMA2_Stream5 = 68,		/// DMA2 Stream5 global interrupt
		DMA2_Stream7 = 70,		/// DMA2 Stream7 global interrupt
		DMA2_Stream3 = 59,		/// DMA2 Stream3 global interrupt
		DMA2_Stream1 = 57,		/// DMA2 Stream1 global interrupt
		DMA2_Stream2 = 58,		/// DMA2 Stream2 global interrupt
	};
	/// low interrupt status register
	enum mask_LISR {
		TCIF3 = 0x08000000,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:27 width:1 
		HTIF3 = 0x04000000,		/// Stream x half transfer interrupt flag (x=3..0) offset:26 width:1 
		TEIF3 = 0x02000000,		/// Stream x transfer error interrupt flag (x=3..0) offset:25 width:1 
		DMEIF3 = 0x01000000,		/// Stream x direct mode error interrupt flag (x=3..0) offset:24 width:1 
		FEIF3 = 0x00400000,		/// Stream x FIFO error interrupt flag (x=3..0) offset:22 width:1 
		TCIF2 = 0x00200000,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:21 width:1 
		HTIF2 = 0x00100000,		/// Stream x half transfer interrupt flag (x=3..0) offset:20 width:1 
		TEIF2 = 0x00080000,		/// Stream x transfer error interrupt flag (x=3..0) offset:19 width:1 
		DMEIF2 = 0x00040000,		/// Stream x direct mode error interrupt flag (x=3..0) offset:18 width:1 
		FEIF2 = 0x00010000,		/// Stream x FIFO error interrupt flag (x=3..0) offset:16 width:1 
		TCIF1 = 0x00000800,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:11 width:1 
		HTIF1 = 0x00000400,		/// Stream x half transfer interrupt flag (x=3..0) offset:10 width:1 
		TEIF1 = 0x00000200,		/// Stream x transfer error interrupt flag (x=3..0) offset:9 width:1 
		DMEIF1 = 0x00000100,		/// Stream x direct mode error interrupt flag (x=3..0) offset:8 width:1 
		FEIF1 = 0x00000040,		/// Stream x FIFO error interrupt flag (x=3..0) offset:6 width:1 
		TCIF0 = 0x00000020,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:5 width:1 
		HTIF0 = 0x00000010,		/// Stream x half transfer interrupt flag (x=3..0) offset:4 width:1 
		TEIF0 = 0x00000008,		/// Stream x transfer error interrupt flag (x=3..0) offset:3 width:1 
		DMEIF0 = 0x00000004,		/// Stream x direct mode error interrupt flag (x=3..0) offset:2 width:1 
		FEIF0 = 0x00000001,		/// Stream x FIFO error interrupt flag (x=3..0) offset:0 width:1 
	}
	/// high interrupt status register
	enum mask_HISR {
		TCIF7 = 0x08000000,		/// Stream x transfer complete interrupt flag (x=7..4) offset:27 width:1 
		HTIF7 = 0x04000000,		/// Stream x half transfer interrupt flag (x=7..4) offset:26 width:1 
		TEIF7 = 0x02000000,		/// Stream x transfer error interrupt flag (x=7..4) offset:25 width:1 
		DMEIF7 = 0x01000000,		/// Stream x direct mode error interrupt flag (x=7..4) offset:24 width:1 
		FEIF7 = 0x00400000,		/// Stream x FIFO error interrupt flag (x=7..4) offset:22 width:1 
		TCIF6 = 0x00200000,		/// Stream x transfer complete interrupt flag (x=7..4) offset:21 width:1 
		HTIF6 = 0x00100000,		/// Stream x half transfer interrupt flag (x=7..4) offset:20 width:1 
		TEIF6 = 0x00080000,		/// Stream x transfer error interrupt flag (x=7..4) offset:19 width:1 
		DMEIF6 = 0x00040000,		/// Stream x direct mode error interrupt flag (x=7..4) offset:18 width:1 
		FEIF6 = 0x00010000,		/// Stream x FIFO error interrupt flag (x=7..4) offset:16 width:1 
		TCIF5 = 0x00000800,		/// Stream x transfer complete interrupt flag (x=7..4) offset:11 width:1 
		HTIF5 = 0x00000400,		/// Stream x half transfer interrupt flag (x=7..4) offset:10 width:1 
		TEIF5 = 0x00000200,		/// Stream x transfer error interrupt flag (x=7..4) offset:9 width:1 
		DMEIF5 = 0x00000100,		/// Stream x direct mode error interrupt flag (x=7..4) offset:8 width:1 
		FEIF5 = 0x00000040,		/// Stream x FIFO error interrupt flag (x=7..4) offset:6 width:1 
		TCIF4 = 0x00000020,		/// Stream x transfer complete interrupt flag (x=7..4) offset:5 width:1 
		HTIF4 = 0x00000010,		/// Stream x half transfer interrupt flag (x=7..4) offset:4 width:1 
		TEIF4 = 0x00000008,		/// Stream x transfer error interrupt flag (x=7..4) offset:3 width:1 
		DMEIF4 = 0x00000004,		/// Stream x direct mode error interrupt flag (x=7..4) offset:2 width:1 
		FEIF4 = 0x00000001,		/// Stream x FIFO error interrupt flag (x=7..4) offset:0 width:1 
	}
	/// low interrupt flag clear register
	enum mask_LIFCR {
		CTCIF3 = 0x08000000,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:27 width:1 
		CHTIF3 = 0x04000000,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:26 width:1 
		CTEIF3 = 0x02000000,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:25 width:1 
		CDMEIF3 = 0x01000000,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:24 width:1 
		CFEIF3 = 0x00400000,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:22 width:1 
		CTCIF2 = 0x00200000,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:21 width:1 
		CHTIF2 = 0x00100000,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:20 width:1 
		CTEIF2 = 0x00080000,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:19 width:1 
		CDMEIF2 = 0x00040000,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:18 width:1 
		CFEIF2 = 0x00010000,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:16 width:1 
		CTCIF1 = 0x00000800,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:11 width:1 
		CHTIF1 = 0x00000400,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:10 width:1 
		CTEIF1 = 0x00000200,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:9 width:1 
		CDMEIF1 = 0x00000100,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:8 width:1 
		CFEIF1 = 0x00000040,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:6 width:1 
		CTCIF0 = 0x00000020,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:5 width:1 
		CHTIF0 = 0x00000010,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:4 width:1 
		CTEIF0 = 0x00000008,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:3 width:1 
		CDMEIF0 = 0x00000004,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:2 width:1 
		CFEIF0 = 0x00000001,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:0 width:1 
	}
	/// high interrupt flag clear register
	enum mask_HIFCR {
		CTCIF7 = 0x08000000,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:27 width:1 
		CHTIF7 = 0x04000000,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:26 width:1 
		CTEIF7 = 0x02000000,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:25 width:1 
		CDMEIF7 = 0x01000000,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:24 width:1 
		CFEIF7 = 0x00400000,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:22 width:1 
		CTCIF6 = 0x00200000,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:21 width:1 
		CHTIF6 = 0x00100000,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:20 width:1 
		CTEIF6 = 0x00080000,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:19 width:1 
		CDMEIF6 = 0x00040000,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:18 width:1 
		CFEIF6 = 0x00010000,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:16 width:1 
		CTCIF5 = 0x00000800,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:11 width:1 
		CHTIF5 = 0x00000400,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:10 width:1 
		CTEIF5 = 0x00000200,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:9 width:1 
		CDMEIF5 = 0x00000100,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:8 width:1 
		CFEIF5 = 0x00000040,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:6 width:1 
		CTCIF4 = 0x00000020,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:5 width:1 
		CHTIF4 = 0x00000010,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:4 width:1 
		CTEIF4 = 0x00000008,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:3 width:1 
		CDMEIF4 = 0x00000004,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:2 width:1 
		CFEIF4 = 0x00000001,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:0 width:1 
	}
	/// stream x configuration register
	enum mask_S0CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S0NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S0PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S0M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S0M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S0FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S1CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S1NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S1PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S1M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S1M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S1FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S2CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S2NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S2PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S2M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S2M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S2FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S3CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S3NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S3PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S3M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S3M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S3FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S4CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S4NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S4PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S4M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S4M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S4FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S5CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S5NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S5PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S5M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S5M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S5FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S6CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S6NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S6PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S6M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S6M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S6FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S7CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S7NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S7PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S7M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S7M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S7FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_LISR,uint) LISR;		/// low interrupt status register offset:0x00000000
	@Register("read-only",0x04)	RO_Reg!(mask_HISR,uint) HISR;		/// high interrupt status register offset:0x00000004
	@Register("write-only",0x08)	WO_Reg!(mask_LIFCR,uint) LIFCR;		/// low interrupt flag clear register offset:0x00000008
	@Register("write-only",0x0C)	WO_Reg!(mask_HIFCR,uint) HIFCR;		/// high interrupt flag clear register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_S0CR,uint) S0CR;		/// stream x configuration register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_S0NDTR,uint) S0NDTR;		/// stream x number of data register offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_S0PAR,uint) S0PAR;		/// stream x peripheral address register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_S0M0AR,uint) S0M0AR;		/// stream x memory 0 address register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_S0M1AR,uint) S0M1AR;		/// stream x memory 1 address register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_S0FCR,uint) S0FCR;		/// stream x FIFO control register offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_S1CR,uint) S1CR;		/// stream x configuration register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_S1NDTR,uint) S1NDTR;		/// stream x number of data register offset:0x0000002C
	@Register("read-write",0x30)	RW_Reg!(mask_S1PAR,uint) S1PAR;		/// stream x peripheral address register offset:0x00000030
	@Register("read-write",0x34)	RW_Reg!(mask_S1M0AR,uint) S1M0AR;		/// stream x memory 0 address register offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_S1M1AR,uint) S1M1AR;		/// stream x memory 1 address register offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_S1FCR,uint) S1FCR;		/// stream x FIFO control register offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_S2CR,uint) S2CR;		/// stream x configuration register offset:0x00000040
	@Register("read-write",0x44)	RW_Reg!(mask_S2NDTR,uint) S2NDTR;		/// stream x number of data register offset:0x00000044
	@Register("read-write",0x48)	RW_Reg!(mask_S2PAR,uint) S2PAR;		/// stream x peripheral address register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_S2M0AR,uint) S2M0AR;		/// stream x memory 0 address register offset:0x0000004C
	@Register("read-write",0x50)	RW_Reg!(mask_S2M1AR,uint) S2M1AR;		/// stream x memory 1 address register offset:0x00000050
	@Register("read-write",0x54)	RW_Reg!(mask_S2FCR,uint) S2FCR;		/// stream x FIFO control register offset:0x00000054
	@Register("read-write",0x58)	RW_Reg!(mask_S3CR,uint) S3CR;		/// stream x configuration register offset:0x00000058
	@Register("read-write",0x5C)	RW_Reg!(mask_S3NDTR,uint) S3NDTR;		/// stream x number of data register offset:0x0000005C
	@Register("read-write",0x60)	RW_Reg!(mask_S3PAR,uint) S3PAR;		/// stream x peripheral address register offset:0x00000060
	@Register("read-write",0x64)	RW_Reg!(mask_S3M0AR,uint) S3M0AR;		/// stream x memory 0 address register offset:0x00000064
	@Register("read-write",0x68)	RW_Reg!(mask_S3M1AR,uint) S3M1AR;		/// stream x memory 1 address register offset:0x00000068
	@Register("read-write",0x6C)	RW_Reg!(mask_S3FCR,uint) S3FCR;		/// stream x FIFO control register offset:0x0000006C
	@Register("read-write",0x70)	RW_Reg!(mask_S4CR,uint) S4CR;		/// stream x configuration register offset:0x00000070
	@Register("read-write",0x74)	RW_Reg!(mask_S4NDTR,uint) S4NDTR;		/// stream x number of data register offset:0x00000074
	@Register("read-write",0x78)	RW_Reg!(mask_S4PAR,uint) S4PAR;		/// stream x peripheral address register offset:0x00000078
	@Register("read-write",0x7C)	RW_Reg!(mask_S4M0AR,uint) S4M0AR;		/// stream x memory 0 address register offset:0x0000007C
	@Register("read-write",0x80)	RW_Reg!(mask_S4M1AR,uint) S4M1AR;		/// stream x memory 1 address register offset:0x00000080
	@Register("read-write",0x84)	RW_Reg!(mask_S4FCR,uint) S4FCR;		/// stream x FIFO control register offset:0x00000084
	@Register("read-write",0x88)	RW_Reg!(mask_S5CR,uint) S5CR;		/// stream x configuration register offset:0x00000088
	@Register("read-write",0x8C)	RW_Reg!(mask_S5NDTR,uint) S5NDTR;		/// stream x number of data register offset:0x0000008C
	@Register("read-write",0x90)	RW_Reg!(mask_S5PAR,uint) S5PAR;		/// stream x peripheral address register offset:0x00000090
	@Register("read-write",0x94)	RW_Reg!(mask_S5M0AR,uint) S5M0AR;		/// stream x memory 0 address register offset:0x00000094
	@Register("read-write",0x98)	RW_Reg!(mask_S5M1AR,uint) S5M1AR;		/// stream x memory 1 address register offset:0x00000098
	@Register("read-write",0x9C)	RW_Reg!(mask_S5FCR,uint) S5FCR;		/// stream x FIFO control register offset:0x0000009C
	@Register("read-write",0xA0)	RW_Reg!(mask_S6CR,uint) S6CR;		/// stream x configuration register offset:0x000000A0
	@Register("read-write",0xA4)	RW_Reg!(mask_S6NDTR,uint) S6NDTR;		/// stream x number of data register offset:0x000000A4
	@Register("read-write",0xA8)	RW_Reg!(mask_S6PAR,uint) S6PAR;		/// stream x peripheral address register offset:0x000000A8
	@Register("read-write",0xAC)	RW_Reg!(mask_S6M0AR,uint) S6M0AR;		/// stream x memory 0 address register offset:0x000000AC
	@Register("read-write",0xB0)	RW_Reg!(mask_S6M1AR,uint) S6M1AR;		/// stream x memory 1 address register offset:0x000000B0
	@Register("read-write",0xB4)	RW_Reg!(mask_S6FCR,uint) S6FCR;		/// stream x FIFO control register offset:0x000000B4
	@Register("read-write",0xB8)	RW_Reg!(mask_S7CR,uint) S7CR;		/// stream x configuration register offset:0x000000B8
	@Register("read-write",0xBC)	RW_Reg!(mask_S7NDTR,uint) S7NDTR;		/// stream x number of data register offset:0x000000BC
	@Register("read-write",0xC0)	RW_Reg!(mask_S7PAR,uint) S7PAR;		/// stream x peripheral address register offset:0x000000C0
	@Register("read-write",0xC4)	RW_Reg!(mask_S7M0AR,uint) S7M0AR;		/// stream x memory 0 address register offset:0x000000C4
	@Register("read-write",0xC8)	RW_Reg!(mask_S7M1AR,uint) S7M1AR;		/// stream x memory 1 address register offset:0x000000C8
	@Register("read-write",0xCC)	RW_Reg!(mask_S7FCR,uint) S7FCR;		/// stream x FIFO control register offset:0x000000CC
}
/// Nullable.null address:0x40026000
struct DMA1_Type{
	@disable this();
	enum Interrupt {
		DMA1_Stream2 = 13,		/// DMA1 Stream2 global interrupt
		DMA1_Stream4 = 15,		/// DMA1 Stream4 global interrupt
		DMA1_Stream6 = 17,		/// DMA1 Stream6 global interrupt
		DMA1_Stream7 = 47,		/// DMA1 Stream7 global interrupt
		DMA1_Stream0 = 11,		/// DMA1 Stream0 global interrupt
		DMA1_Stream5 = 16,		/// DMA1 Stream5 global interrupt
		DMA1_Stream1 = 12,		/// DMA1 Stream1 global interrupt
		DMA1_Stream3 = 14,		/// DMA1 Stream3 global interrupt
	};
	/// low interrupt status register
	enum mask_LISR {
		TCIF3 = 0x08000000,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:27 width:1 
		HTIF3 = 0x04000000,		/// Stream x half transfer interrupt flag (x=3..0) offset:26 width:1 
		TEIF3 = 0x02000000,		/// Stream x transfer error interrupt flag (x=3..0) offset:25 width:1 
		DMEIF3 = 0x01000000,		/// Stream x direct mode error interrupt flag (x=3..0) offset:24 width:1 
		FEIF3 = 0x00400000,		/// Stream x FIFO error interrupt flag (x=3..0) offset:22 width:1 
		TCIF2 = 0x00200000,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:21 width:1 
		HTIF2 = 0x00100000,		/// Stream x half transfer interrupt flag (x=3..0) offset:20 width:1 
		TEIF2 = 0x00080000,		/// Stream x transfer error interrupt flag (x=3..0) offset:19 width:1 
		DMEIF2 = 0x00040000,		/// Stream x direct mode error interrupt flag (x=3..0) offset:18 width:1 
		FEIF2 = 0x00010000,		/// Stream x FIFO error interrupt flag (x=3..0) offset:16 width:1 
		TCIF1 = 0x00000800,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:11 width:1 
		HTIF1 = 0x00000400,		/// Stream x half transfer interrupt flag (x=3..0) offset:10 width:1 
		TEIF1 = 0x00000200,		/// Stream x transfer error interrupt flag (x=3..0) offset:9 width:1 
		DMEIF1 = 0x00000100,		/// Stream x direct mode error interrupt flag (x=3..0) offset:8 width:1 
		FEIF1 = 0x00000040,		/// Stream x FIFO error interrupt flag (x=3..0) offset:6 width:1 
		TCIF0 = 0x00000020,		/// Stream x transfer complete interrupt flag (x = 3..0) offset:5 width:1 
		HTIF0 = 0x00000010,		/// Stream x half transfer interrupt flag (x=3..0) offset:4 width:1 
		TEIF0 = 0x00000008,		/// Stream x transfer error interrupt flag (x=3..0) offset:3 width:1 
		DMEIF0 = 0x00000004,		/// Stream x direct mode error interrupt flag (x=3..0) offset:2 width:1 
		FEIF0 = 0x00000001,		/// Stream x FIFO error interrupt flag (x=3..0) offset:0 width:1 
	}
	/// high interrupt status register
	enum mask_HISR {
		TCIF7 = 0x08000000,		/// Stream x transfer complete interrupt flag (x=7..4) offset:27 width:1 
		HTIF7 = 0x04000000,		/// Stream x half transfer interrupt flag (x=7..4) offset:26 width:1 
		TEIF7 = 0x02000000,		/// Stream x transfer error interrupt flag (x=7..4) offset:25 width:1 
		DMEIF7 = 0x01000000,		/// Stream x direct mode error interrupt flag (x=7..4) offset:24 width:1 
		FEIF7 = 0x00400000,		/// Stream x FIFO error interrupt flag (x=7..4) offset:22 width:1 
		TCIF6 = 0x00200000,		/// Stream x transfer complete interrupt flag (x=7..4) offset:21 width:1 
		HTIF6 = 0x00100000,		/// Stream x half transfer interrupt flag (x=7..4) offset:20 width:1 
		TEIF6 = 0x00080000,		/// Stream x transfer error interrupt flag (x=7..4) offset:19 width:1 
		DMEIF6 = 0x00040000,		/// Stream x direct mode error interrupt flag (x=7..4) offset:18 width:1 
		FEIF6 = 0x00010000,		/// Stream x FIFO error interrupt flag (x=7..4) offset:16 width:1 
		TCIF5 = 0x00000800,		/// Stream x transfer complete interrupt flag (x=7..4) offset:11 width:1 
		HTIF5 = 0x00000400,		/// Stream x half transfer interrupt flag (x=7..4) offset:10 width:1 
		TEIF5 = 0x00000200,		/// Stream x transfer error interrupt flag (x=7..4) offset:9 width:1 
		DMEIF5 = 0x00000100,		/// Stream x direct mode error interrupt flag (x=7..4) offset:8 width:1 
		FEIF5 = 0x00000040,		/// Stream x FIFO error interrupt flag (x=7..4) offset:6 width:1 
		TCIF4 = 0x00000020,		/// Stream x transfer complete interrupt flag (x=7..4) offset:5 width:1 
		HTIF4 = 0x00000010,		/// Stream x half transfer interrupt flag (x=7..4) offset:4 width:1 
		TEIF4 = 0x00000008,		/// Stream x transfer error interrupt flag (x=7..4) offset:3 width:1 
		DMEIF4 = 0x00000004,		/// Stream x direct mode error interrupt flag (x=7..4) offset:2 width:1 
		FEIF4 = 0x00000001,		/// Stream x FIFO error interrupt flag (x=7..4) offset:0 width:1 
	}
	/// low interrupt flag clear register
	enum mask_LIFCR {
		CTCIF3 = 0x08000000,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:27 width:1 
		CHTIF3 = 0x04000000,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:26 width:1 
		CTEIF3 = 0x02000000,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:25 width:1 
		CDMEIF3 = 0x01000000,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:24 width:1 
		CFEIF3 = 0x00400000,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:22 width:1 
		CTCIF2 = 0x00200000,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:21 width:1 
		CHTIF2 = 0x00100000,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:20 width:1 
		CTEIF2 = 0x00080000,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:19 width:1 
		CDMEIF2 = 0x00040000,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:18 width:1 
		CFEIF2 = 0x00010000,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:16 width:1 
		CTCIF1 = 0x00000800,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:11 width:1 
		CHTIF1 = 0x00000400,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:10 width:1 
		CTEIF1 = 0x00000200,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:9 width:1 
		CDMEIF1 = 0x00000100,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:8 width:1 
		CFEIF1 = 0x00000040,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:6 width:1 
		CTCIF0 = 0x00000020,		/// Stream x clear transfer complete interrupt flag (x = 3..0) offset:5 width:1 
		CHTIF0 = 0x00000010,		/// Stream x clear half transfer interrupt flag (x = 3..0) offset:4 width:1 
		CTEIF0 = 0x00000008,		/// Stream x clear transfer error interrupt flag (x = 3..0) offset:3 width:1 
		CDMEIF0 = 0x00000004,		/// Stream x clear direct mode error interrupt flag (x = 3..0) offset:2 width:1 
		CFEIF0 = 0x00000001,		/// Stream x clear FIFO error interrupt flag (x = 3..0) offset:0 width:1 
	}
	/// high interrupt flag clear register
	enum mask_HIFCR {
		CTCIF7 = 0x08000000,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:27 width:1 
		CHTIF7 = 0x04000000,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:26 width:1 
		CTEIF7 = 0x02000000,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:25 width:1 
		CDMEIF7 = 0x01000000,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:24 width:1 
		CFEIF7 = 0x00400000,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:22 width:1 
		CTCIF6 = 0x00200000,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:21 width:1 
		CHTIF6 = 0x00100000,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:20 width:1 
		CTEIF6 = 0x00080000,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:19 width:1 
		CDMEIF6 = 0x00040000,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:18 width:1 
		CFEIF6 = 0x00010000,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:16 width:1 
		CTCIF5 = 0x00000800,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:11 width:1 
		CHTIF5 = 0x00000400,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:10 width:1 
		CTEIF5 = 0x00000200,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:9 width:1 
		CDMEIF5 = 0x00000100,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:8 width:1 
		CFEIF5 = 0x00000040,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:6 width:1 
		CTCIF4 = 0x00000020,		/// Stream x clear transfer complete interrupt flag (x = 7..4) offset:5 width:1 
		CHTIF4 = 0x00000010,		/// Stream x clear half transfer interrupt flag (x = 7..4) offset:4 width:1 
		CTEIF4 = 0x00000008,		/// Stream x clear transfer error interrupt flag (x = 7..4) offset:3 width:1 
		CDMEIF4 = 0x00000004,		/// Stream x clear direct mode error interrupt flag (x = 7..4) offset:2 width:1 
		CFEIF4 = 0x00000001,		/// Stream x clear FIFO error interrupt flag (x = 7..4) offset:0 width:1 
	}
	/// stream x configuration register
	enum mask_S0CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S0NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S0PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S0M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S0M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S0FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S1CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S1NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S1PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S1M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S1M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S1FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S2CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S2NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S2PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S2M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S2M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S2FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S3CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S3NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S3PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S3M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S3M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S3FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S4CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S4NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S4PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S4M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S4M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S4FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S5CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S5NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S5PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S5M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S5M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S5FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S6CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S6NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S6PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S6M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S6M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S6FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	/// stream x configuration register
	enum mask_S7CR {
		CHSEL = 0x0E000000,		/// Channel selection offset:25 width:3 
		MBURST = 0x01800000,		/// Memory burst transfer configuration offset:23 width:2 
		PBURST = 0x00600000,		/// Peripheral burst transfer configuration offset:21 width:2 
		ACK = 0x00100000,		/// ACK offset:20 width:1 
		CT = 0x00080000,		/// Current target (only in double buffer mode) offset:19 width:1 
		DBM = 0x00040000,		/// Double buffer mode offset:18 width:1 
		PL = 0x00030000,		/// Priority level offset:16 width:2 
		PINCOS = 0x00008000,		/// Peripheral increment offset size offset:15 width:1 
		MSIZE = 0x00006000,		/// Memory data size offset:13 width:2 
		PSIZE = 0x00001800,		/// Peripheral data size offset:11 width:2 
		MINC = 0x00000400,		/// Memory increment mode offset:10 width:1 
		PINC = 0x00000200,		/// Peripheral increment mode offset:9 width:1 
		CIRC = 0x00000100,		/// Circular mode offset:8 width:1 
		DIR = 0x000000C0,		/// Data transfer direction offset:6 width:2 
		PFCTRL = 0x00000020,		/// Peripheral flow controller offset:5 width:1 
		TCIE = 0x00000010,		/// Transfer complete interrupt enable offset:4 width:1 
		HTIE = 0x00000008,		/// Half transfer interrupt enable offset:3 width:1 
		TEIE = 0x00000004,		/// Transfer error interrupt enable offset:2 width:1 
		DMEIE = 0x00000002,		/// Direct mode error interrupt enable offset:1 width:1 
		EN = 0x00000001,		/// Stream enable / flag stream ready when read low offset:0 width:1 
	}
	/// stream x number of data register
	enum mask_S7NDTR {
		NDT = 0x0000FFFF,		/// Number of data items to transfer offset:0 width:16 
	}
	/// stream x peripheral address register
	enum mask_S7PAR {
		PA = 0xFFFFFFFF,		/// Peripheral address offset:0 width:32 
	}
	/// stream x memory 0 address register
	enum mask_S7M0AR {
		M0A = 0xFFFFFFFF,		/// Memory 0 address offset:0 width:32 
	}
	/// stream x memory 1 address register
	enum mask_S7M1AR {
		M1A = 0xFFFFFFFF,		/// Memory 1 address (used in case of Double buffer mode) offset:0 width:32 
	}
	/// stream x FIFO control register
	enum mask_S7FCR {
		FEIE = 0x00000080,		/// FIFO error interrupt enable offset:7 width:1 
		FS = 0x00000038,		/// FIFO status offset:3 width:3 
		DMDIS = 0x00000004,		/// Direct mode disable offset:2 width:1 
		FTH = 0x00000003,		/// FIFO threshold selection offset:0 width:2 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_LISR,uint) LISR;		/// low interrupt status register offset:0x00000000
	@Register("read-only",0x04)	RO_Reg!(mask_HISR,uint) HISR;		/// high interrupt status register offset:0x00000004
	@Register("write-only",0x08)	WO_Reg!(mask_LIFCR,uint) LIFCR;		/// low interrupt flag clear register offset:0x00000008
	@Register("write-only",0x0C)	WO_Reg!(mask_HIFCR,uint) HIFCR;		/// high interrupt flag clear register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_S0CR,uint) S0CR;		/// stream x configuration register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_S0NDTR,uint) S0NDTR;		/// stream x number of data register offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_S0PAR,uint) S0PAR;		/// stream x peripheral address register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_S0M0AR,uint) S0M0AR;		/// stream x memory 0 address register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_S0M1AR,uint) S0M1AR;		/// stream x memory 1 address register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_S0FCR,uint) S0FCR;		/// stream x FIFO control register offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_S1CR,uint) S1CR;		/// stream x configuration register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_S1NDTR,uint) S1NDTR;		/// stream x number of data register offset:0x0000002C
	@Register("read-write",0x30)	RW_Reg!(mask_S1PAR,uint) S1PAR;		/// stream x peripheral address register offset:0x00000030
	@Register("read-write",0x34)	RW_Reg!(mask_S1M0AR,uint) S1M0AR;		/// stream x memory 0 address register offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_S1M1AR,uint) S1M1AR;		/// stream x memory 1 address register offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_S1FCR,uint) S1FCR;		/// stream x FIFO control register offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_S2CR,uint) S2CR;		/// stream x configuration register offset:0x00000040
	@Register("read-write",0x44)	RW_Reg!(mask_S2NDTR,uint) S2NDTR;		/// stream x number of data register offset:0x00000044
	@Register("read-write",0x48)	RW_Reg!(mask_S2PAR,uint) S2PAR;		/// stream x peripheral address register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_S2M0AR,uint) S2M0AR;		/// stream x memory 0 address register offset:0x0000004C
	@Register("read-write",0x50)	RW_Reg!(mask_S2M1AR,uint) S2M1AR;		/// stream x memory 1 address register offset:0x00000050
	@Register("read-write",0x54)	RW_Reg!(mask_S2FCR,uint) S2FCR;		/// stream x FIFO control register offset:0x00000054
	@Register("read-write",0x58)	RW_Reg!(mask_S3CR,uint) S3CR;		/// stream x configuration register offset:0x00000058
	@Register("read-write",0x5C)	RW_Reg!(mask_S3NDTR,uint) S3NDTR;		/// stream x number of data register offset:0x0000005C
	@Register("read-write",0x60)	RW_Reg!(mask_S3PAR,uint) S3PAR;		/// stream x peripheral address register offset:0x00000060
	@Register("read-write",0x64)	RW_Reg!(mask_S3M0AR,uint) S3M0AR;		/// stream x memory 0 address register offset:0x00000064
	@Register("read-write",0x68)	RW_Reg!(mask_S3M1AR,uint) S3M1AR;		/// stream x memory 1 address register offset:0x00000068
	@Register("read-write",0x6C)	RW_Reg!(mask_S3FCR,uint) S3FCR;		/// stream x FIFO control register offset:0x0000006C
	@Register("read-write",0x70)	RW_Reg!(mask_S4CR,uint) S4CR;		/// stream x configuration register offset:0x00000070
	@Register("read-write",0x74)	RW_Reg!(mask_S4NDTR,uint) S4NDTR;		/// stream x number of data register offset:0x00000074
	@Register("read-write",0x78)	RW_Reg!(mask_S4PAR,uint) S4PAR;		/// stream x peripheral address register offset:0x00000078
	@Register("read-write",0x7C)	RW_Reg!(mask_S4M0AR,uint) S4M0AR;		/// stream x memory 0 address register offset:0x0000007C
	@Register("read-write",0x80)	RW_Reg!(mask_S4M1AR,uint) S4M1AR;		/// stream x memory 1 address register offset:0x00000080
	@Register("read-write",0x84)	RW_Reg!(mask_S4FCR,uint) S4FCR;		/// stream x FIFO control register offset:0x00000084
	@Register("read-write",0x88)	RW_Reg!(mask_S5CR,uint) S5CR;		/// stream x configuration register offset:0x00000088
	@Register("read-write",0x8C)	RW_Reg!(mask_S5NDTR,uint) S5NDTR;		/// stream x number of data register offset:0x0000008C
	@Register("read-write",0x90)	RW_Reg!(mask_S5PAR,uint) S5PAR;		/// stream x peripheral address register offset:0x00000090
	@Register("read-write",0x94)	RW_Reg!(mask_S5M0AR,uint) S5M0AR;		/// stream x memory 0 address register offset:0x00000094
	@Register("read-write",0x98)	RW_Reg!(mask_S5M1AR,uint) S5M1AR;		/// stream x memory 1 address register offset:0x00000098
	@Register("read-write",0x9C)	RW_Reg!(mask_S5FCR,uint) S5FCR;		/// stream x FIFO control register offset:0x0000009C
	@Register("read-write",0xA0)	RW_Reg!(mask_S6CR,uint) S6CR;		/// stream x configuration register offset:0x000000A0
	@Register("read-write",0xA4)	RW_Reg!(mask_S6NDTR,uint) S6NDTR;		/// stream x number of data register offset:0x000000A4
	@Register("read-write",0xA8)	RW_Reg!(mask_S6PAR,uint) S6PAR;		/// stream x peripheral address register offset:0x000000A8
	@Register("read-write",0xAC)	RW_Reg!(mask_S6M0AR,uint) S6M0AR;		/// stream x memory 0 address register offset:0x000000AC
	@Register("read-write",0xB0)	RW_Reg!(mask_S6M1AR,uint) S6M1AR;		/// stream x memory 1 address register offset:0x000000B0
	@Register("read-write",0xB4)	RW_Reg!(mask_S6FCR,uint) S6FCR;		/// stream x FIFO control register offset:0x000000B4
	@Register("read-write",0xB8)	RW_Reg!(mask_S7CR,uint) S7CR;		/// stream x configuration register offset:0x000000B8
	@Register("read-write",0xBC)	RW_Reg!(mask_S7NDTR,uint) S7NDTR;		/// stream x number of data register offset:0x000000BC
	@Register("read-write",0xC0)	RW_Reg!(mask_S7PAR,uint) S7PAR;		/// stream x peripheral address register offset:0x000000C0
	@Register("read-write",0xC4)	RW_Reg!(mask_S7M0AR,uint) S7M0AR;		/// stream x memory 0 address register offset:0x000000C4
	@Register("read-write",0xC8)	RW_Reg!(mask_S7M1AR,uint) S7M1AR;		/// stream x memory 1 address register offset:0x000000C8
	@Register("read-write",0xCC)	RW_Reg!(mask_S7FCR,uint) S7FCR;		/// stream x FIFO control register offset:0x000000CC
}
/// Reset and clock control address:0x40023800
struct RCC_Type{
	@disable this();
	enum Interrupt {
		RCC = 5,		/// RCC global interrupt
	};
	/// clock control register
	enum mask_CR {
		PLLI2SRDY = 0x08000000,		/// PLLI2S clock ready flag offset:27 width:1 
		PLLI2SON = 0x04000000,		/// PLLI2S enable offset:26 width:1 
		PLLRDY = 0x02000000,		/// Main PLL (PLL) clock ready flag offset:25 width:1 
		PLLON = 0x01000000,		/// Main PLL (PLL) enable offset:24 width:1 
		CSSON = 0x00080000,		/// Clock security system enable offset:19 width:1 
		HSEBYP = 0x00040000,		/// HSE clock bypass offset:18 width:1 
		HSERDY = 0x00020000,		/// HSE clock ready flag offset:17 width:1 
		HSEON = 0x00010000,		/// HSE clock enable offset:16 width:1 
		HSICAL = 0x0000FF00,		/// Internal high-speed clock calibration offset:8 width:8 
		HSITRIM = 0x000000F8,		/// Internal high-speed clock trimming offset:3 width:5 
		HSIRDY = 0x00000002,		/// Internal high-speed clock ready flag offset:1 width:1 
		HSION = 0x00000001,		/// Internal high-speed clock enable offset:0 width:1 
	}
	/// PLL configuration register
	enum mask_PLLCFGR {
		PLLQ3 = 0x08000000,		/// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks offset:27 width:1 
		PLLQ2 = 0x04000000,		/// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks offset:26 width:1 
		PLLQ1 = 0x02000000,		/// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks offset:25 width:1 
		PLLQ0 = 0x01000000,		/// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks offset:24 width:1 
		PLLSRC = 0x00400000,		/// Main PLL(PLL) and audio PLL (PLLI2S) entry clock source offset:22 width:1 
		PLLP1 = 0x00020000,		/// Main PLL (PLL) division factor for main system clock offset:17 width:1 
		PLLP0 = 0x00010000,		/// Main PLL (PLL) division factor for main system clock offset:16 width:1 
		PLLN8 = 0x00004000,		/// Main PLL (PLL) multiplication factor for VCO offset:14 width:1 
		PLLN7 = 0x00002000,		/// Main PLL (PLL) multiplication factor for VCO offset:13 width:1 
		PLLN6 = 0x00001000,		/// Main PLL (PLL) multiplication factor for VCO offset:12 width:1 
		PLLN5 = 0x00000800,		/// Main PLL (PLL) multiplication factor for VCO offset:11 width:1 
		PLLN4 = 0x00000400,		/// Main PLL (PLL) multiplication factor for VCO offset:10 width:1 
		PLLN3 = 0x00000200,		/// Main PLL (PLL) multiplication factor for VCO offset:9 width:1 
		PLLN2 = 0x00000100,		/// Main PLL (PLL) multiplication factor for VCO offset:8 width:1 
		PLLN1 = 0x00000080,		/// Main PLL (PLL) multiplication factor for VCO offset:7 width:1 
		PLLN0 = 0x00000040,		/// Main PLL (PLL) multiplication factor for VCO offset:6 width:1 
		PLLM5 = 0x00000020,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:5 width:1 
		PLLM4 = 0x00000010,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:4 width:1 
		PLLM3 = 0x00000008,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:3 width:1 
		PLLM2 = 0x00000004,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:2 width:1 
		PLLM1 = 0x00000002,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:1 width:1 
		PLLM0 = 0x00000001,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:0 width:1 
		PLLM = 0x0000003F,		/// Division factor for the main PLL (PLL) and audio PLL (PLLI2S) input clock offset:0 width:6 
		PLLN = 0x00007FC0,		/// Main PLL (PLL) multiplication factor for VCO offset:6 width:9 
		PLLP = 0x00030000,		/// Main PLL (PLL) division factor for main system clock offset:16 width:2 
		PLLQ = 0x0F000000,		/// Main PLL (PLL) division factor for USB OTG FS, SDIO and random number generator clocks offset:24 width:4 
	}
	/// clock configuration register
	enum mask_CFGR {
		MCO2 = 0xC0000000,		/// Microcontroller clock output 2 offset:30 width:2 
		MCO2PRE = 0x38000000,		/// MCO2 prescaler offset:27 width:3 
		MCO1PRE = 0x07000000,		/// MCO1 prescaler offset:24 width:3 
		I2SSRC = 0x00800000,		/// I2S clock selection offset:23 width:1 
		MCO1 = 0x00600000,		/// Microcontroller clock output 1 offset:21 width:2 
		RTCPRE = 0x001F0000,		/// HSE division factor for RTC clock offset:16 width:5 
		PPRE2 = 0x0000E000,		/// APB high-speed prescaler (APB2) offset:13 width:3 
		PPRE1 = 0x00001C00,		/// APB Low speed prescaler (APB1) offset:10 width:3 
		HPRE = 0x000000F0,		/// AHB prescaler offset:4 width:4 
		SWS1 = 0x00000008,		/// System clock switch status offset:3 width:1 
		SWS0 = 0x00000004,		/// System clock switch status offset:2 width:1 
		SWS = 0x0000000C,		/// System clock switch status offset:2 width:2 
		SW1 = 0x00000002,		/// System clock switch offset:1 width:1 
		SW0 = 0x00000001,		/// System clock switch offset:0 width:1 
		SW = 0x00000003,		/// System clock switch offset:0 width:2 
	}
	/// clock interrupt register
	enum mask_CIR {
		CSSC = 0x00800000,		/// Clock security system interrupt clear offset:23 width:1 
		PLLI2SRDYC = 0x00200000,		/// PLLI2S ready interrupt clear offset:21 width:1 
		PLLRDYC = 0x00100000,		/// Main PLL(PLL) ready interrupt clear offset:20 width:1 
		HSERDYC = 0x00080000,		/// HSE ready interrupt clear offset:19 width:1 
		HSIRDYC = 0x00040000,		/// HSI ready interrupt clear offset:18 width:1 
		LSERDYC = 0x00020000,		/// LSE ready interrupt clear offset:17 width:1 
		LSIRDYC = 0x00010000,		/// LSI ready interrupt clear offset:16 width:1 
		PLLI2SRDYIE = 0x00002000,		/// PLLI2S ready interrupt enable offset:13 width:1 
		PLLRDYIE = 0x00001000,		/// Main PLL (PLL) ready interrupt enable offset:12 width:1 
		HSERDYIE = 0x00000800,		/// HSE ready interrupt enable offset:11 width:1 
		HSIRDYIE = 0x00000400,		/// HSI ready interrupt enable offset:10 width:1 
		LSERDYIE = 0x00000200,		/// LSE ready interrupt enable offset:9 width:1 
		LSIRDYIE = 0x00000100,		/// LSI ready interrupt enable offset:8 width:1 
		CSSF = 0x00000080,		/// Clock security system interrupt flag offset:7 width:1 
		PLLI2SRDYF = 0x00000020,		/// PLLI2S ready interrupt flag offset:5 width:1 
		PLLRDYF = 0x00000010,		/// Main PLL (PLL) ready interrupt flag offset:4 width:1 
		HSERDYF = 0x00000008,		/// HSE ready interrupt flag offset:3 width:1 
		HSIRDYF = 0x00000004,		/// HSI ready interrupt flag offset:2 width:1 
		LSERDYF = 0x00000002,		/// LSE ready interrupt flag offset:1 width:1 
		LSIRDYF = 0x00000001,		/// LSI ready interrupt flag offset:0 width:1 
	}
	/// AHB1 peripheral reset register
	enum mask_AHB1RSTR {
		DMA2RST = 0x00400000,		/// DMA2 reset offset:22 width:1 
		DMA1RST = 0x00200000,		/// DMA2 reset offset:21 width:1 
		CRCRST = 0x00001000,		/// CRC reset offset:12 width:1 
		GPIOHRST = 0x00000080,		/// IO port H reset offset:7 width:1 
		GPIOERST = 0x00000010,		/// IO port E reset offset:4 width:1 
		GPIODRST = 0x00000008,		/// IO port D reset offset:3 width:1 
		GPIOCRST = 0x00000004,		/// IO port C reset offset:2 width:1 
		GPIOBRST = 0x00000002,		/// IO port B reset offset:1 width:1 
		GPIOARST = 0x00000001,		/// IO port A reset offset:0 width:1 
	}
	/// AHB2 peripheral reset register
	enum mask_AHB2RSTR {
		OTGFSRST = 0x00000080,		/// USB OTG FS module reset offset:7 width:1 
	}
	/// APB1 peripheral reset register
	enum mask_APB1RSTR {
		PWRRST = 0x10000000,		/// Power interface reset offset:28 width:1 
		I2C3RST = 0x00800000,		/// I2C3 reset offset:23 width:1 
		I2C2RST = 0x00400000,		/// I2C 2 reset offset:22 width:1 
		I2C1RST = 0x00200000,		/// I2C 1 reset offset:21 width:1 
		UART2RST = 0x00020000,		/// USART 2 reset offset:17 width:1 
		SPI3RST = 0x00008000,		/// SPI 3 reset offset:15 width:1 
		SPI2RST = 0x00004000,		/// SPI 2 reset offset:14 width:1 
		WWDGRST = 0x00000800,		/// Window watchdog reset offset:11 width:1 
		TIM5RST = 0x00000008,		/// TIM5 reset offset:3 width:1 
		TIM4RST = 0x00000004,		/// TIM4 reset offset:2 width:1 
		TIM3RST = 0x00000002,		/// TIM3 reset offset:1 width:1 
		TIM2RST = 0x00000001,		/// TIM2 reset offset:0 width:1 
	}
	/// APB2 peripheral reset register
	enum mask_APB2RSTR {
		TIM11RST = 0x00040000,		/// TIM11 reset offset:18 width:1 
		TIM10RST = 0x00020000,		/// TIM10 reset offset:17 width:1 
		TIM9RST = 0x00010000,		/// TIM9 reset offset:16 width:1 
		SYSCFGRST = 0x00004000,		/// System configuration controller reset offset:14 width:1 
		SPI1RST = 0x00001000,		/// SPI 1 reset offset:12 width:1 
		SDIORST = 0x00000800,		/// SDIO reset offset:11 width:1 
		ADCRST = 0x00000100,		/// ADC interface reset (common to all ADCs) offset:8 width:1 
		USART6RST = 0x00000020,		/// USART6 reset offset:5 width:1 
		USART1RST = 0x00000010,		/// USART1 reset offset:4 width:1 
		TIM1RST = 0x00000001,		/// TIM1 reset offset:0 width:1 
	}
	/// AHB1 peripheral clock register
	enum mask_AHB1ENR {
		DMA2EN = 0x00400000,		/// DMA2 clock enable offset:22 width:1 
		DMA1EN = 0x00200000,		/// DMA1 clock enable offset:21 width:1 
		CRCEN = 0x00001000,		/// CRC clock enable offset:12 width:1 
		GPIOHEN = 0x00000080,		/// IO port H clock enable offset:7 width:1 
		GPIOEEN = 0x00000010,		/// IO port E clock enable offset:4 width:1 
		GPIODEN = 0x00000008,		/// IO port D clock enable offset:3 width:1 
		GPIOCEN = 0x00000004,		/// IO port C clock enable offset:2 width:1 
		GPIOBEN = 0x00000002,		/// IO port B clock enable offset:1 width:1 
		GPIOAEN = 0x00000001,		/// IO port A clock enable offset:0 width:1 
	}
	/// AHB2 peripheral clock enable register
	enum mask_AHB2ENR {
		OTGFSEN = 0x00000080,		/// USB OTG FS clock enable offset:7 width:1 
	}
	/// APB1 peripheral clock enable register
	enum mask_APB1ENR {
		PWREN = 0x10000000,		/// Power interface clock enable offset:28 width:1 
		I2C3EN = 0x00800000,		/// I2C3 clock enable offset:23 width:1 
		I2C2EN = 0x00400000,		/// I2C2 clock enable offset:22 width:1 
		I2C1EN = 0x00200000,		/// I2C1 clock enable offset:21 width:1 
		USART2EN = 0x00020000,		/// USART 2 clock enable offset:17 width:1 
		SPI3EN = 0x00008000,		/// SPI3 clock enable offset:15 width:1 
		SPI2EN = 0x00004000,		/// SPI2 clock enable offset:14 width:1 
		WWDGEN = 0x00000800,		/// Window watchdog clock enable offset:11 width:1 
		TIM5EN = 0x00000008,		/// TIM5 clock enable offset:3 width:1 
		TIM4EN = 0x00000004,		/// TIM4 clock enable offset:2 width:1 
		TIM3EN = 0x00000002,		/// TIM3 clock enable offset:1 width:1 
		TIM2EN = 0x00000001,		/// TIM2 clock enable offset:0 width:1 
	}
	/// APB2 peripheral clock enable register
	enum mask_APB2ENR {
		TIM11EN = 0x00040000,		/// TIM11 clock enable offset:18 width:1 
		TIM10EN = 0x00020000,		/// TIM10 clock enable offset:17 width:1 
		TIM9EN = 0x00010000,		/// TIM9 clock enable offset:16 width:1 
		SYSCFGEN = 0x00004000,		/// System configuration controller clock enable offset:14 width:1 
		SPI1EN = 0x00001000,		/// SPI1 clock enable offset:12 width:1 
		SDIOEN = 0x00000800,		/// SDIO clock enable offset:11 width:1 
		ADC1EN = 0x00000100,		/// ADC1 clock enable offset:8 width:1 
		USART6EN = 0x00000020,		/// USART6 clock enable offset:5 width:1 
		USART1EN = 0x00000010,		/// USART1 clock enable offset:4 width:1 
		TIM1EN = 0x00000001,		/// TIM1 clock enable offset:0 width:1 
	}
	/// AHB1 peripheral clock enable in low power mode register
	enum mask_AHB1LPENR {
		DMA2LPEN = 0x00400000,		/// DMA2 clock enable during Sleep mode offset:22 width:1 
		DMA1LPEN = 0x00200000,		/// DMA1 clock enable during Sleep mode offset:21 width:1 
		SRAM1LPEN = 0x00010000,		/// SRAM 1interface clock enable during Sleep mode offset:16 width:1 
		FLITFLPEN = 0x00008000,		/// Flash interface clock enable during Sleep mode offset:15 width:1 
		CRCLPEN = 0x00001000,		/// CRC clock enable during Sleep mode offset:12 width:1 
		GPIOHLPEN = 0x00000080,		/// IO port H clock enable during Sleep mode offset:7 width:1 
		GPIOELPEN = 0x00000010,		/// IO port E clock enable during Sleep mode offset:4 width:1 
		GPIODLPEN = 0x00000008,		/// IO port D clock enable during Sleep mode offset:3 width:1 
		GPIOCLPEN = 0x00000004,		/// IO port C clock enable during Sleep mode offset:2 width:1 
		GPIOBLPEN = 0x00000002,		/// IO port B clock enable during Sleep mode offset:1 width:1 
		GPIOALPEN = 0x00000001,		/// IO port A clock enable during sleep mode offset:0 width:1 
	}
	/// AHB2 peripheral clock enable in low power mode register
	enum mask_AHB2LPENR {
		OTGFSLPEN = 0x00000080,		/// USB OTG FS clock enable during Sleep mode offset:7 width:1 
	}
	/// APB1 peripheral clock enable in low power mode register
	enum mask_APB1LPENR {
		PWRLPEN = 0x10000000,		/// Power interface clock enable during Sleep mode offset:28 width:1 
		I2C3LPEN = 0x00800000,		/// I2C3 clock enable during Sleep mode offset:23 width:1 
		I2C2LPEN = 0x00400000,		/// I2C2 clock enable during Sleep mode offset:22 width:1 
		I2C1LPEN = 0x00200000,		/// I2C1 clock enable during Sleep mode offset:21 width:1 
		USART2LPEN = 0x00020000,		/// USART2 clock enable during Sleep mode offset:17 width:1 
		SPI3LPEN = 0x00008000,		/// SPI3 clock enable during Sleep mode offset:15 width:1 
		SPI2LPEN = 0x00004000,		/// SPI2 clock enable during Sleep mode offset:14 width:1 
		WWDGLPEN = 0x00000800,		/// Window watchdog clock enable during Sleep mode offset:11 width:1 
		TIM5LPEN = 0x00000008,		/// TIM5 clock enable during Sleep mode offset:3 width:1 
		TIM4LPEN = 0x00000004,		/// TIM4 clock enable during Sleep mode offset:2 width:1 
		TIM3LPEN = 0x00000002,		/// TIM3 clock enable during Sleep mode offset:1 width:1 
		TIM2LPEN = 0x00000001,		/// TIM2 clock enable during Sleep mode offset:0 width:1 
	}
	/// APB2 peripheral clock enabled in low power mode register
	enum mask_APB2LPENR {
		TIM11LPEN = 0x00040000,		/// TIM11 clock enable during Sleep mode offset:18 width:1 
		TIM10LPEN = 0x00020000,		/// TIM10 clock enable during Sleep mode offset:17 width:1 
		TIM9LPEN = 0x00010000,		/// TIM9 clock enable during sleep mode offset:16 width:1 
		SYSCFGLPEN = 0x00004000,		/// System configuration controller clock enable during Sleep mode offset:14 width:1 
		SPI1LPEN = 0x00001000,		/// SPI 1 clock enable during Sleep mode offset:12 width:1 
		SDIOLPEN = 0x00000800,		/// SDIO clock enable during Sleep mode offset:11 width:1 
		ADC1LPEN = 0x00000100,		/// ADC1 clock enable during Sleep mode offset:8 width:1 
		USART6LPEN = 0x00000020,		/// USART6 clock enable during Sleep mode offset:5 width:1 
		USART1LPEN = 0x00000010,		/// USART1 clock enable during Sleep mode offset:4 width:1 
		TIM1LPEN = 0x00000001,		/// TIM1 clock enable during Sleep mode offset:0 width:1 
	}
	/// Backup domain control register
	enum mask_BDCR {
		BDRST = 0x00010000,		/// Backup domain software reset offset:16 width:1 
		RTCEN = 0x00008000,		/// RTC clock enable offset:15 width:1 
		RTCSEL1 = 0x00000200,		/// RTC clock source selection offset:9 width:1 
		RTCSEL0 = 0x00000100,		/// RTC clock source selection offset:8 width:1 
		LSEBYP = 0x00000004,		/// External low-speed oscillator bypass offset:2 width:1 
		LSERDY = 0x00000002,		/// External low-speed oscillator ready offset:1 width:1 
		LSEON = 0x00000001,		/// External low-speed oscillator enable offset:0 width:1 
	}
	/// clock control & status register
	enum mask_CSR {
		LPWRRSTF = 0x80000000,		/// Low-power reset flag offset:31 width:1 
		WWDGRSTF = 0x40000000,		/// Window watchdog reset flag offset:30 width:1 
		WDGRSTF = 0x20000000,		/// Independent watchdog reset flag offset:29 width:1 
		SFTRSTF = 0x10000000,		/// Software reset flag offset:28 width:1 
		PORRSTF = 0x08000000,		/// POR/PDR reset flag offset:27 width:1 
		PADRSTF = 0x04000000,		/// PIN reset flag offset:26 width:1 
		BORRSTF = 0x02000000,		/// BOR reset flag offset:25 width:1 
		RMVF = 0x01000000,		/// Remove reset flag offset:24 width:1 
		LSIRDY = 0x00000002,		/// Internal low-speed oscillator ready offset:1 width:1 
		LSION = 0x00000001,		/// Internal low-speed oscillator enable offset:0 width:1 
	}
	/// spread spectrum clock generation register
	enum mask_SSCGR {
		SSCGEN = 0x80000000,		/// Spread spectrum modulation enable offset:31 width:1 
		SPREADSEL = 0x40000000,		/// Spread Select offset:30 width:1 
		INCSTEP = 0x0FFFE000,		/// Incrementation step offset:13 width:15 
		MODPER = 0x00001FFF,		/// Modulation period offset:0 width:13 
	}
	/// PLLI2S configuration register
	enum mask_PLLI2SCFGR {
		PLLI2SRx = 0x70000000,		/// PLLI2S division factor for I2S clocks offset:28 width:3 
		PLLI2SNx = 0x00007FC0,		/// PLLI2S multiplication factor for VCO offset:6 width:9 
	}
	/// RCC Dedicated Clocks configuration register
	enum mask_DCKCFGR {
		TIMPRE = 0x01000000,		/// TIMPRE offset:24 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR,uint) CR;		/// clock control register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_PLLCFGR,uint) PLLCFGR;		/// PLL configuration register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_CFGR,uint) CFGR;		/// clock configuration register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CIR,uint) CIR;		/// clock interrupt register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_AHB1RSTR,uint) AHB1RSTR;		/// AHB1 peripheral reset register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_AHB2RSTR,uint) AHB2RSTR;		/// AHB2 peripheral reset register offset:0x00000014
	uint[0x02]		Reserved0;
	@Register("read-write",0x20)	RW_Reg!(mask_APB1RSTR,uint) APB1RSTR;		/// APB1 peripheral reset register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_APB2RSTR,uint) APB2RSTR;		/// APB2 peripheral reset register offset:0x00000024
	uint[0x02]		Reserved1;
	@Register("read-write",0x30)	RW_Reg!(mask_AHB1ENR,uint) AHB1ENR;		/// AHB1 peripheral clock register offset:0x00000030
	@Register("read-write",0x34)	RW_Reg!(mask_AHB2ENR,uint) AHB2ENR;		/// AHB2 peripheral clock enable register offset:0x00000034
	uint[0x02]		Reserved2;
	@Register("read-write",0x40)	RW_Reg!(mask_APB1ENR,uint) APB1ENR;		/// APB1 peripheral clock enable register offset:0x00000040
	@Register("read-write",0x44)	RW_Reg!(mask_APB2ENR,uint) APB2ENR;		/// APB2 peripheral clock enable register offset:0x00000044
	uint[0x02]		Reserved3;
	@Register("read-write",0x50)	RW_Reg!(mask_AHB1LPENR,uint) AHB1LPENR;		/// AHB1 peripheral clock enable in low power mode register offset:0x00000050
	@Register("read-write",0x54)	RW_Reg!(mask_AHB2LPENR,uint) AHB2LPENR;		/// AHB2 peripheral clock enable in low power mode register offset:0x00000054
	uint[0x02]		Reserved4;
	@Register("read-write",0x60)	RW_Reg!(mask_APB1LPENR,uint) APB1LPENR;		/// APB1 peripheral clock enable in low power mode register offset:0x00000060
	@Register("read-write",0x64)	RW_Reg!(mask_APB2LPENR,uint) APB2LPENR;		/// APB2 peripheral clock enabled in low power mode register offset:0x00000064
	uint[0x02]		Reserved5;
	@Register("read-write",0x70)	RW_Reg!(mask_BDCR,uint) BDCR;		/// Backup domain control register offset:0x00000070
	@Register("read-write",0x74)	RW_Reg!(mask_CSR,uint) CSR;		/// clock control & status register offset:0x00000074
	uint[0x02]		Reserved6;
	@Register("read-write",0x80)	RW_Reg!(mask_SSCGR,uint) SSCGR;		/// spread spectrum clock generation register offset:0x00000080
	@Register("read-write",0x84)	RW_Reg!(mask_PLLI2SCFGR,uint) PLLI2SCFGR;		/// PLLI2S configuration register offset:0x00000084
	uint[0x01]		Reserved7;
	@Register("read-write",0x8C)	RW_Reg!(mask_DCKCFGR,uint) DCKCFGR;		/// RCC Dedicated Clocks configuration register offset:0x0000008C
}
/// General-purpose I/Os address:0x40021C00
struct GPIOH_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// Nullable.null address:0x40021000
struct GPIOE_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// Nullable.null address:0X40020C00
struct GPIOD_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// Nullable.null address:0x40020800
struct GPIOC_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// General-purpose I/Os address:0x40020400
struct GPIOB_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// General-purpose I/Os address:0x40020000
struct GPIOA_Type{
	@disable this();
	/// GPIO port mode register
	enum mask_MODER {
		MODER15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		MODER14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		MODER13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		MODER12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		MODER11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		MODER10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		MODER9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		MODER8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		MODER7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		MODER6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		MODER5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		MODER4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		MODER3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		MODER2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		MODER1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		MODER0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port output type register
	enum mask_OTYPER {
		OT15 = 0x00008000,		/// Port x configuration bits (y = 0..15) offset:15 width:1 
		OT14 = 0x00004000,		/// Port x configuration bits (y = 0..15) offset:14 width:1 
		OT13 = 0x00002000,		/// Port x configuration bits (y = 0..15) offset:13 width:1 
		OT12 = 0x00001000,		/// Port x configuration bits (y = 0..15) offset:12 width:1 
		OT11 = 0x00000800,		/// Port x configuration bits (y = 0..15) offset:11 width:1 
		OT10 = 0x00000400,		/// Port x configuration bits (y = 0..15) offset:10 width:1 
		OT9 = 0x00000200,		/// Port x configuration bits (y = 0..15) offset:9 width:1 
		OT8 = 0x00000100,		/// Port x configuration bits (y = 0..15) offset:8 width:1 
		OT7 = 0x00000080,		/// Port x configuration bits (y = 0..15) offset:7 width:1 
		OT6 = 0x00000040,		/// Port x configuration bits (y = 0..15) offset:6 width:1 
		OT5 = 0x00000020,		/// Port x configuration bits (y = 0..15) offset:5 width:1 
		OT4 = 0x00000010,		/// Port x configuration bits (y = 0..15) offset:4 width:1 
		OT3 = 0x00000008,		/// Port x configuration bits (y = 0..15) offset:3 width:1 
		OT2 = 0x00000004,		/// Port x configuration bits (y = 0..15) offset:2 width:1 
		OT1 = 0x00000002,		/// Port x configuration bits (y = 0..15) offset:1 width:1 
		OT0 = 0x00000001,		/// Port x configuration bits (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output speed register
	enum mask_OSPEEDR {
		OSPEEDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		OSPEEDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		OSPEEDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		OSPEEDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		OSPEEDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		OSPEEDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		OSPEEDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		OSPEEDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		OSPEEDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		OSPEEDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		OSPEEDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		OSPEEDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		OSPEEDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		OSPEEDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		OSPEEDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		OSPEEDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port pull-up/pull-down register
	enum mask_PUPDR {
		PUPDR15 = 0xC0000000,		/// Port x configuration bits (y = 0..15) offset:30 width:2 
		PUPDR14 = 0x30000000,		/// Port x configuration bits (y = 0..15) offset:28 width:2 
		PUPDR13 = 0x0C000000,		/// Port x configuration bits (y = 0..15) offset:26 width:2 
		PUPDR12 = 0x03000000,		/// Port x configuration bits (y = 0..15) offset:24 width:2 
		PUPDR11 = 0x00C00000,		/// Port x configuration bits (y = 0..15) offset:22 width:2 
		PUPDR10 = 0x00300000,		/// Port x configuration bits (y = 0..15) offset:20 width:2 
		PUPDR9 = 0x000C0000,		/// Port x configuration bits (y = 0..15) offset:18 width:2 
		PUPDR8 = 0x00030000,		/// Port x configuration bits (y = 0..15) offset:16 width:2 
		PUPDR7 = 0x0000C000,		/// Port x configuration bits (y = 0..15) offset:14 width:2 
		PUPDR6 = 0x00003000,		/// Port x configuration bits (y = 0..15) offset:12 width:2 
		PUPDR5 = 0x00000C00,		/// Port x configuration bits (y = 0..15) offset:10 width:2 
		PUPDR4 = 0x00000300,		/// Port x configuration bits (y = 0..15) offset:8 width:2 
		PUPDR3 = 0x000000C0,		/// Port x configuration bits (y = 0..15) offset:6 width:2 
		PUPDR2 = 0x00000030,		/// Port x configuration bits (y = 0..15) offset:4 width:2 
		PUPDR1 = 0x0000000C,		/// Port x configuration bits (y = 0..15) offset:2 width:2 
		PUPDR0 = 0x00000003,		/// Port x configuration bits (y = 0..15) offset:0 width:2 
	}
	/// GPIO port input data register
	enum mask_IDR {
		IDR15 = 0x00008000,		/// Port input data (y = 0..15) offset:15 width:1 
		IDR14 = 0x00004000,		/// Port input data (y = 0..15) offset:14 width:1 
		IDR13 = 0x00002000,		/// Port input data (y = 0..15) offset:13 width:1 
		IDR12 = 0x00001000,		/// Port input data (y = 0..15) offset:12 width:1 
		IDR11 = 0x00000800,		/// Port input data (y = 0..15) offset:11 width:1 
		IDR10 = 0x00000400,		/// Port input data (y = 0..15) offset:10 width:1 
		IDR9 = 0x00000200,		/// Port input data (y = 0..15) offset:9 width:1 
		IDR8 = 0x00000100,		/// Port input data (y = 0..15) offset:8 width:1 
		IDR7 = 0x00000080,		/// Port input data (y = 0..15) offset:7 width:1 
		IDR6 = 0x00000040,		/// Port input data (y = 0..15) offset:6 width:1 
		IDR5 = 0x00000020,		/// Port input data (y = 0..15) offset:5 width:1 
		IDR4 = 0x00000010,		/// Port input data (y = 0..15) offset:4 width:1 
		IDR3 = 0x00000008,		/// Port input data (y = 0..15) offset:3 width:1 
		IDR2 = 0x00000004,		/// Port input data (y = 0..15) offset:2 width:1 
		IDR1 = 0x00000002,		/// Port input data (y = 0..15) offset:1 width:1 
		IDR0 = 0x00000001,		/// Port input data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port output data register
	enum mask_ODR {
		ODR15 = 0x00008000,		/// Port output data (y = 0..15) offset:15 width:1 
		ODR14 = 0x00004000,		/// Port output data (y = 0..15) offset:14 width:1 
		ODR13 = 0x00002000,		/// Port output data (y = 0..15) offset:13 width:1 
		ODR12 = 0x00001000,		/// Port output data (y = 0..15) offset:12 width:1 
		ODR11 = 0x00000800,		/// Port output data (y = 0..15) offset:11 width:1 
		ODR10 = 0x00000400,		/// Port output data (y = 0..15) offset:10 width:1 
		ODR9 = 0x00000200,		/// Port output data (y = 0..15) offset:9 width:1 
		ODR8 = 0x00000100,		/// Port output data (y = 0..15) offset:8 width:1 
		ODR7 = 0x00000080,		/// Port output data (y = 0..15) offset:7 width:1 
		ODR6 = 0x00000040,		/// Port output data (y = 0..15) offset:6 width:1 
		ODR5 = 0x00000020,		/// Port output data (y = 0..15) offset:5 width:1 
		ODR4 = 0x00000010,		/// Port output data (y = 0..15) offset:4 width:1 
		ODR3 = 0x00000008,		/// Port output data (y = 0..15) offset:3 width:1 
		ODR2 = 0x00000004,		/// Port output data (y = 0..15) offset:2 width:1 
		ODR1 = 0x00000002,		/// Port output data (y = 0..15) offset:1 width:1 
		ODR0 = 0x00000001,		/// Port output data (y = 0..15) offset:0 width:1 
	}
	/// GPIO port bit set/reset register
	enum mask_BSRR {
		BR15 = 0x80000000,		/// Port x reset bit y (y = 0..15) offset:31 width:1 
		BR14 = 0x40000000,		/// Port x reset bit y (y = 0..15) offset:30 width:1 
		BR13 = 0x20000000,		/// Port x reset bit y (y = 0..15) offset:29 width:1 
		BR12 = 0x10000000,		/// Port x reset bit y (y = 0..15) offset:28 width:1 
		BR11 = 0x08000000,		/// Port x reset bit y (y = 0..15) offset:27 width:1 
		BR10 = 0x04000000,		/// Port x reset bit y (y = 0..15) offset:26 width:1 
		BR9 = 0x02000000,		/// Port x reset bit y (y = 0..15) offset:25 width:1 
		BR8 = 0x01000000,		/// Port x reset bit y (y = 0..15) offset:24 width:1 
		BR7 = 0x00800000,		/// Port x reset bit y (y = 0..15) offset:23 width:1 
		BR6 = 0x00400000,		/// Port x reset bit y (y = 0..15) offset:22 width:1 
		BR5 = 0x00200000,		/// Port x reset bit y (y = 0..15) offset:21 width:1 
		BR4 = 0x00100000,		/// Port x reset bit y (y = 0..15) offset:20 width:1 
		BR3 = 0x00080000,		/// Port x reset bit y (y = 0..15) offset:19 width:1 
		BR2 = 0x00040000,		/// Port x reset bit y (y = 0..15) offset:18 width:1 
		BR1 = 0x00020000,		/// Port x reset bit y (y = 0..15) offset:17 width:1 
		BR0 = 0x00010000,		/// Port x set bit y (y= 0..15) offset:16 width:1 
		BS15 = 0x00008000,		/// Port x set bit y (y= 0..15) offset:15 width:1 
		BS14 = 0x00004000,		/// Port x set bit y (y= 0..15) offset:14 width:1 
		BS13 = 0x00002000,		/// Port x set bit y (y= 0..15) offset:13 width:1 
		BS12 = 0x00001000,		/// Port x set bit y (y= 0..15) offset:12 width:1 
		BS11 = 0x00000800,		/// Port x set bit y (y= 0..15) offset:11 width:1 
		BS10 = 0x00000400,		/// Port x set bit y (y= 0..15) offset:10 width:1 
		BS9 = 0x00000200,		/// Port x set bit y (y= 0..15) offset:9 width:1 
		BS8 = 0x00000100,		/// Port x set bit y (y= 0..15) offset:8 width:1 
		BS7 = 0x00000080,		/// Port x set bit y (y= 0..15) offset:7 width:1 
		BS6 = 0x00000040,		/// Port x set bit y (y= 0..15) offset:6 width:1 
		BS5 = 0x00000020,		/// Port x set bit y (y= 0..15) offset:5 width:1 
		BS4 = 0x00000010,		/// Port x set bit y (y= 0..15) offset:4 width:1 
		BS3 = 0x00000008,		/// Port x set bit y (y= 0..15) offset:3 width:1 
		BS2 = 0x00000004,		/// Port x set bit y (y= 0..15) offset:2 width:1 
		BS1 = 0x00000002,		/// Port x set bit y (y= 0..15) offset:1 width:1 
		BS0 = 0x00000001,		/// Port x set bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO port configuration lock register
	enum mask_LCKR {
		LCKK = 0x00010000,		/// Port x lock bit y (y= 0..15) offset:16 width:1 
		LCK15 = 0x00008000,		/// Port x lock bit y (y= 0..15) offset:15 width:1 
		LCK14 = 0x00004000,		/// Port x lock bit y (y= 0..15) offset:14 width:1 
		LCK13 = 0x00002000,		/// Port x lock bit y (y= 0..15) offset:13 width:1 
		LCK12 = 0x00001000,		/// Port x lock bit y (y= 0..15) offset:12 width:1 
		LCK11 = 0x00000800,		/// Port x lock bit y (y= 0..15) offset:11 width:1 
		LCK10 = 0x00000400,		/// Port x lock bit y (y= 0..15) offset:10 width:1 
		LCK9 = 0x00000200,		/// Port x lock bit y (y= 0..15) offset:9 width:1 
		LCK8 = 0x00000100,		/// Port x lock bit y (y= 0..15) offset:8 width:1 
		LCK7 = 0x00000080,		/// Port x lock bit y (y= 0..15) offset:7 width:1 
		LCK6 = 0x00000040,		/// Port x lock bit y (y= 0..15) offset:6 width:1 
		LCK5 = 0x00000020,		/// Port x lock bit y (y= 0..15) offset:5 width:1 
		LCK4 = 0x00000010,		/// Port x lock bit y (y= 0..15) offset:4 width:1 
		LCK3 = 0x00000008,		/// Port x lock bit y (y= 0..15) offset:3 width:1 
		LCK2 = 0x00000004,		/// Port x lock bit y (y= 0..15) offset:2 width:1 
		LCK1 = 0x00000002,		/// Port x lock bit y (y= 0..15) offset:1 width:1 
		LCK0 = 0x00000001,		/// Port x lock bit y (y= 0..15) offset:0 width:1 
	}
	/// GPIO alternate function low register
	enum mask_AFRL {
		AFRL7 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:28 width:4 
		AFRL6 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 0..7) offset:24 width:4 
		AFRL5 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 0..7) offset:20 width:4 
		AFRL4 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 0..7) offset:16 width:4 
		AFRL3 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 0..7) offset:12 width:4 
		AFRL2 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 0..7) offset:8 width:4 
		AFRL1 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 0..7) offset:4 width:4 
		AFRL0 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 0..7) offset:0 width:4 
	}
	/// GPIO alternate function high register
	enum mask_AFRH {
		AFRH15 = 0xF0000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:28 width:4 
		AFRH14 = 0x0F000000,		/// Alternate function selection for port x bit y (y = 8..15) offset:24 width:4 
		AFRH13 = 0x00F00000,		/// Alternate function selection for port x bit y (y = 8..15) offset:20 width:4 
		AFRH12 = 0x000F0000,		/// Alternate function selection for port x bit y (y = 8..15) offset:16 width:4 
		AFRH11 = 0x0000F000,		/// Alternate function selection for port x bit y (y = 8..15) offset:12 width:4 
		AFRH10 = 0x00000F00,		/// Alternate function selection for port x bit y (y = 8..15) offset:8 width:4 
		AFRH9 = 0x000000F0,		/// Alternate function selection for port x bit y (y = 8..15) offset:4 width:4 
		AFRH8 = 0x0000000F,		/// Alternate function selection for port x bit y (y = 8..15) offset:0 width:4 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MODER,uint) MODER;		/// GPIO port mode register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_OTYPER,uint) OTYPER;		/// GPIO port output type register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OSPEEDR,uint) OSPEEDR;		/// GPIO port output speed register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_PUPDR,uint) PUPDR;		/// GPIO port pull-up/pull-down register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_IDR,uint) IDR;		/// GPIO port input data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_ODR,uint) ODR;		/// GPIO port output data register offset:0x00000014
	@Register("write-only",0x18)	WO_Reg!(mask_BSRR,uint) BSRR;		/// GPIO port bit set/reset register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_LCKR,uint) LCKR;		/// GPIO port configuration lock register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_AFRL,uint) AFRL;		/// GPIO alternate function low register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_AFRH,uint) AFRH;		/// GPIO alternate function high register offset:0x00000024
}
/// System configuration controller address:0x40013800
struct SYSCFG_Type{
	@disable this();
	/// memory remap register
	enum mask_MEMRM {
		MEM_MODE = 0x00000003,		/// MEM_MODE offset:0 width:2 
	}
	/// peripheral mode configuration register
	enum mask_PMC {
		ADC1DC2 = 0x00010000,		/// ADC1DC2 offset:16 width:1 
	}
	/// external interrupt configuration register 1
	enum mask_EXTICR1 {
		EXTI3 = 0x0000F000,		/// EXTI x configuration (x = 0 to 3) offset:12 width:4 
		EXTI2 = 0x00000F00,		/// EXTI x configuration (x = 0 to 3) offset:8 width:4 
		EXTI1 = 0x000000F0,		/// EXTI x configuration (x = 0 to 3) offset:4 width:4 
		EXTI0 = 0x0000000F,		/// EXTI x configuration (x = 0 to 3) offset:0 width:4 
	}
	/// external interrupt configuration register 2
	enum mask_EXTICR2 {
		EXTI7 = 0x0000F000,		/// EXTI x configuration (x = 4 to 7) offset:12 width:4 
		EXTI6 = 0x00000F00,		/// EXTI x configuration (x = 4 to 7) offset:8 width:4 
		EXTI5 = 0x000000F0,		/// EXTI x configuration (x = 4 to 7) offset:4 width:4 
		EXTI4 = 0x0000000F,		/// EXTI x configuration (x = 4 to 7) offset:0 width:4 
	}
	/// external interrupt configuration register 3
	enum mask_EXTICR3 {
		EXTI11 = 0x0000F000,		/// EXTI x configuration (x = 8 to 11) offset:12 width:4 
		EXTI10 = 0x00000F00,		/// EXTI10 offset:8 width:4 
		EXTI9 = 0x000000F0,		/// EXTI x configuration (x = 8 to 11) offset:4 width:4 
		EXTI8 = 0x0000000F,		/// EXTI x configuration (x = 8 to 11) offset:0 width:4 
	}
	/// external interrupt configuration register 4
	enum mask_EXTICR4 {
		EXTI15 = 0x0000F000,		/// EXTI x configuration (x = 12 to 15) offset:12 width:4 
		EXTI14 = 0x00000F00,		/// EXTI x configuration (x = 12 to 15) offset:8 width:4 
		EXTI13 = 0x000000F0,		/// EXTI x configuration (x = 12 to 15) offset:4 width:4 
		EXTI12 = 0x0000000F,		/// EXTI x configuration (x = 12 to 15) offset:0 width:4 
	}
	/// Compensation cell control register
	enum mask_CMPCR {
		READY = 0x00000100,		/// READY offset:8 width:1 
		CMP_PD = 0x00000001,		/// Compensation cell power-down offset:0 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_MEMRM,uint) MEMRM;		/// memory remap register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_PMC,uint) PMC;		/// peripheral mode configuration register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_EXTICR1,uint) EXTICR1;		/// external interrupt configuration register 1 offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_EXTICR2,uint) EXTICR2;		/// external interrupt configuration register 2 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_EXTICR3,uint) EXTICR3;		/// external interrupt configuration register 3 offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_EXTICR4,uint) EXTICR4;		/// external interrupt configuration register 4 offset:0x00000014
	uint[0x02]		Reserved0;
	@Register("read-only",0x20)	RO_Reg!(mask_CMPCR,uint) CMPCR;		/// Compensation cell control register offset:0x00000020
}
/// Serial peripheral interface address:0x40013000
struct SPI1_Type{
	@disable this();
	enum Interrupt {
		SPI1 = 35,		/// SPI1 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		BIDIMODE = 0x00008000,		/// Bidirectional data mode enable offset:15 width:1 
		BIDIOE = 0x00004000,		/// Output enable in bidirectional mode offset:14 width:1 
		CRCEN = 0x00002000,		/// Hardware CRC calculation enable offset:13 width:1 
		CRCNEXT = 0x00001000,		/// CRC transfer next offset:12 width:1 
		DFF = 0x00000800,		/// Data frame format offset:11 width:1 
		RXONLY = 0x00000400,		/// Receive only offset:10 width:1 
		SSM = 0x00000200,		/// Software slave management offset:9 width:1 
		SSI = 0x00000100,		/// Internal slave select offset:8 width:1 
		LSBFIRST = 0x00000080,		/// Frame format offset:7 width:1 
		SPE = 0x00000040,		/// SPI enable offset:6 width:1 
		BR = 0x00000038,		/// Baud rate control offset:3 width:3 
		MSTR = 0x00000004,		/// Master selection offset:2 width:1 
		CPOL = 0x00000002,		/// Clock polarity offset:1 width:1 
		CPHA = 0x00000001,		/// Clock phase offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TXEIE = 0x00000080,		/// Tx buffer empty interrupt enable offset:7 width:1 
		RXNEIE = 0x00000040,		/// RX buffer not empty interrupt enable offset:6 width:1 
		ERRIE = 0x00000020,		/// Error interrupt enable offset:5 width:1 
		FRF = 0x00000010,		/// Frame format offset:4 width:1 
		SSOE = 0x00000004,		/// SS output enable offset:2 width:1 
		TXDMAEN = 0x00000002,		/// Tx buffer DMA enable offset:1 width:1 
		RXDMAEN = 0x00000001,		/// Rx buffer DMA enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		TIFRFE = 0x00000100,		/// TI frame format error offset:8 width:1 
		BSY = 0x00000080,		/// Busy flag offset:7 width:1 
		OVR = 0x00000040,		/// Overrun flag offset:6 width:1 
		MODF = 0x00000020,		/// Mode fault offset:5 width:1 
		CRCERR = 0x00000010,		/// CRC error flag offset:4 width:1 
		UDR = 0x00000008,		/// Underrun flag offset:3 width:1 
		CHSIDE = 0x00000004,		/// Channel side offset:2 width:1 
		TXE = 0x00000002,		/// Transmit buffer empty offset:1 width:1 
		RXNE = 0x00000001,		/// Receive buffer not empty offset:0 width:1 
	}
	/// data register
	enum mask_DR {
		DR = 0x0000FFFF,		/// Data register offset:0 width:16 
	}
	/// CRC polynomial register
	enum mask_CRCPR {
		CRCPOLY = 0x0000FFFF,		/// CRC polynomial register offset:0 width:16 
	}
	/// RX CRC register
	enum mask_RXCRCR {
		RxCRC = 0x0000FFFF,		/// Rx CRC register offset:0 width:16 
	}
	/// TX CRC register
	enum mask_TXCRCR {
		TxCRC = 0x0000FFFF,		/// Tx CRC register offset:0 width:16 
	}
	/// I2S configuration register
	enum mask_I2SCFGR {
		I2SMOD = 0x00000800,		/// I2S mode selection offset:11 width:1 
		I2SE = 0x00000400,		/// I2S Enable offset:10 width:1 
		I2SCFG = 0x00000300,		/// I2S configuration mode offset:8 width:2 
		PCMSYNC = 0x00000080,		/// PCM frame synchronization offset:7 width:1 
		I2SSTD = 0x00000030,		/// I2S standard selection offset:4 width:2 
		CKPOL = 0x00000008,		/// Steady state clock polarity offset:3 width:1 
		DATLEN = 0x00000006,		/// Data length to be transferred offset:1 width:2 
		CHLEN = 0x00000001,		/// Channel length (number of bits per audio channel) offset:0 width:1 
	}
	/// I2S prescaler register
	enum mask_I2SPR {
		MCKOE = 0x00000200,		/// Master clock output enable offset:9 width:1 
		ODD = 0x00000100,		/// Odd factor for the prescaler offset:8 width:1 
		I2SDIV = 0x000000FF,		/// I2S Linear prescaler offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DR,uint) DR;		/// data register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CRCPR,uint) CRCPR;		/// CRC polynomial register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RXCRCR,uint) RXCRCR;		/// RX CRC register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_TXCRCR,uint) TXCRCR;		/// TX CRC register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_I2SCFGR,uint) I2SCFGR;		/// I2S configuration register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_I2SPR,uint) I2SPR;		/// I2S prescaler register offset:0x00000020
}
/// Nullable.null address:0x40003800
struct SPI2_Type{
	@disable this();
	enum Interrupt {
		SPI2 = 36,		/// SPI2 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		BIDIMODE = 0x00008000,		/// Bidirectional data mode enable offset:15 width:1 
		BIDIOE = 0x00004000,		/// Output enable in bidirectional mode offset:14 width:1 
		CRCEN = 0x00002000,		/// Hardware CRC calculation enable offset:13 width:1 
		CRCNEXT = 0x00001000,		/// CRC transfer next offset:12 width:1 
		DFF = 0x00000800,		/// Data frame format offset:11 width:1 
		RXONLY = 0x00000400,		/// Receive only offset:10 width:1 
		SSM = 0x00000200,		/// Software slave management offset:9 width:1 
		SSI = 0x00000100,		/// Internal slave select offset:8 width:1 
		LSBFIRST = 0x00000080,		/// Frame format offset:7 width:1 
		SPE = 0x00000040,		/// SPI enable offset:6 width:1 
		BR = 0x00000038,		/// Baud rate control offset:3 width:3 
		MSTR = 0x00000004,		/// Master selection offset:2 width:1 
		CPOL = 0x00000002,		/// Clock polarity offset:1 width:1 
		CPHA = 0x00000001,		/// Clock phase offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TXEIE = 0x00000080,		/// Tx buffer empty interrupt enable offset:7 width:1 
		RXNEIE = 0x00000040,		/// RX buffer not empty interrupt enable offset:6 width:1 
		ERRIE = 0x00000020,		/// Error interrupt enable offset:5 width:1 
		FRF = 0x00000010,		/// Frame format offset:4 width:1 
		SSOE = 0x00000004,		/// SS output enable offset:2 width:1 
		TXDMAEN = 0x00000002,		/// Tx buffer DMA enable offset:1 width:1 
		RXDMAEN = 0x00000001,		/// Rx buffer DMA enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		TIFRFE = 0x00000100,		/// TI frame format error offset:8 width:1 
		BSY = 0x00000080,		/// Busy flag offset:7 width:1 
		OVR = 0x00000040,		/// Overrun flag offset:6 width:1 
		MODF = 0x00000020,		/// Mode fault offset:5 width:1 
		CRCERR = 0x00000010,		/// CRC error flag offset:4 width:1 
		UDR = 0x00000008,		/// Underrun flag offset:3 width:1 
		CHSIDE = 0x00000004,		/// Channel side offset:2 width:1 
		TXE = 0x00000002,		/// Transmit buffer empty offset:1 width:1 
		RXNE = 0x00000001,		/// Receive buffer not empty offset:0 width:1 
	}
	/// data register
	enum mask_DR {
		DR = 0x0000FFFF,		/// Data register offset:0 width:16 
	}
	/// CRC polynomial register
	enum mask_CRCPR {
		CRCPOLY = 0x0000FFFF,		/// CRC polynomial register offset:0 width:16 
	}
	/// RX CRC register
	enum mask_RXCRCR {
		RxCRC = 0x0000FFFF,		/// Rx CRC register offset:0 width:16 
	}
	/// TX CRC register
	enum mask_TXCRCR {
		TxCRC = 0x0000FFFF,		/// Tx CRC register offset:0 width:16 
	}
	/// I2S configuration register
	enum mask_I2SCFGR {
		I2SMOD = 0x00000800,		/// I2S mode selection offset:11 width:1 
		I2SE = 0x00000400,		/// I2S Enable offset:10 width:1 
		I2SCFG = 0x00000300,		/// I2S configuration mode offset:8 width:2 
		PCMSYNC = 0x00000080,		/// PCM frame synchronization offset:7 width:1 
		I2SSTD = 0x00000030,		/// I2S standard selection offset:4 width:2 
		CKPOL = 0x00000008,		/// Steady state clock polarity offset:3 width:1 
		DATLEN = 0x00000006,		/// Data length to be transferred offset:1 width:2 
		CHLEN = 0x00000001,		/// Channel length (number of bits per audio channel) offset:0 width:1 
	}
	/// I2S prescaler register
	enum mask_I2SPR {
		MCKOE = 0x00000200,		/// Master clock output enable offset:9 width:1 
		ODD = 0x00000100,		/// Odd factor for the prescaler offset:8 width:1 
		I2SDIV = 0x000000FF,		/// I2S Linear prescaler offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DR,uint) DR;		/// data register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CRCPR,uint) CRCPR;		/// CRC polynomial register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RXCRCR,uint) RXCRCR;		/// RX CRC register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_TXCRCR,uint) TXCRCR;		/// TX CRC register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_I2SCFGR,uint) I2SCFGR;		/// I2S configuration register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_I2SPR,uint) I2SPR;		/// I2S prescaler register offset:0x00000020
}
/// Nullable.null address:0x40003C00
struct SPI3_Type{
	@disable this();
	enum Interrupt {
		SPI3 = 51,		/// SPI3 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		BIDIMODE = 0x00008000,		/// Bidirectional data mode enable offset:15 width:1 
		BIDIOE = 0x00004000,		/// Output enable in bidirectional mode offset:14 width:1 
		CRCEN = 0x00002000,		/// Hardware CRC calculation enable offset:13 width:1 
		CRCNEXT = 0x00001000,		/// CRC transfer next offset:12 width:1 
		DFF = 0x00000800,		/// Data frame format offset:11 width:1 
		RXONLY = 0x00000400,		/// Receive only offset:10 width:1 
		SSM = 0x00000200,		/// Software slave management offset:9 width:1 
		SSI = 0x00000100,		/// Internal slave select offset:8 width:1 
		LSBFIRST = 0x00000080,		/// Frame format offset:7 width:1 
		SPE = 0x00000040,		/// SPI enable offset:6 width:1 
		BR = 0x00000038,		/// Baud rate control offset:3 width:3 
		MSTR = 0x00000004,		/// Master selection offset:2 width:1 
		CPOL = 0x00000002,		/// Clock polarity offset:1 width:1 
		CPHA = 0x00000001,		/// Clock phase offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TXEIE = 0x00000080,		/// Tx buffer empty interrupt enable offset:7 width:1 
		RXNEIE = 0x00000040,		/// RX buffer not empty interrupt enable offset:6 width:1 
		ERRIE = 0x00000020,		/// Error interrupt enable offset:5 width:1 
		FRF = 0x00000010,		/// Frame format offset:4 width:1 
		SSOE = 0x00000004,		/// SS output enable offset:2 width:1 
		TXDMAEN = 0x00000002,		/// Tx buffer DMA enable offset:1 width:1 
		RXDMAEN = 0x00000001,		/// Rx buffer DMA enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		TIFRFE = 0x00000100,		/// TI frame format error offset:8 width:1 
		BSY = 0x00000080,		/// Busy flag offset:7 width:1 
		OVR = 0x00000040,		/// Overrun flag offset:6 width:1 
		MODF = 0x00000020,		/// Mode fault offset:5 width:1 
		CRCERR = 0x00000010,		/// CRC error flag offset:4 width:1 
		UDR = 0x00000008,		/// Underrun flag offset:3 width:1 
		CHSIDE = 0x00000004,		/// Channel side offset:2 width:1 
		TXE = 0x00000002,		/// Transmit buffer empty offset:1 width:1 
		RXNE = 0x00000001,		/// Receive buffer not empty offset:0 width:1 
	}
	/// data register
	enum mask_DR {
		DR = 0x0000FFFF,		/// Data register offset:0 width:16 
	}
	/// CRC polynomial register
	enum mask_CRCPR {
		CRCPOLY = 0x0000FFFF,		/// CRC polynomial register offset:0 width:16 
	}
	/// RX CRC register
	enum mask_RXCRCR {
		RxCRC = 0x0000FFFF,		/// Rx CRC register offset:0 width:16 
	}
	/// TX CRC register
	enum mask_TXCRCR {
		TxCRC = 0x0000FFFF,		/// Tx CRC register offset:0 width:16 
	}
	/// I2S configuration register
	enum mask_I2SCFGR {
		I2SMOD = 0x00000800,		/// I2S mode selection offset:11 width:1 
		I2SE = 0x00000400,		/// I2S Enable offset:10 width:1 
		I2SCFG = 0x00000300,		/// I2S configuration mode offset:8 width:2 
		PCMSYNC = 0x00000080,		/// PCM frame synchronization offset:7 width:1 
		I2SSTD = 0x00000030,		/// I2S standard selection offset:4 width:2 
		CKPOL = 0x00000008,		/// Steady state clock polarity offset:3 width:1 
		DATLEN = 0x00000006,		/// Data length to be transferred offset:1 width:2 
		CHLEN = 0x00000001,		/// Channel length (number of bits per audio channel) offset:0 width:1 
	}
	/// I2S prescaler register
	enum mask_I2SPR {
		MCKOE = 0x00000200,		/// Master clock output enable offset:9 width:1 
		ODD = 0x00000100,		/// Odd factor for the prescaler offset:8 width:1 
		I2SDIV = 0x000000FF,		/// I2S Linear prescaler offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DR,uint) DR;		/// data register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CRCPR,uint) CRCPR;		/// CRC polynomial register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RXCRCR,uint) RXCRCR;		/// RX CRC register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_TXCRCR,uint) TXCRCR;		/// TX CRC register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_I2SCFGR,uint) I2SCFGR;		/// I2S configuration register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_I2SPR,uint) I2SPR;		/// I2S prescaler register offset:0x00000020
}
/// Nullable.null address:0x40003400
struct I2S2ext_Type{
	@disable this();
	/// control register 1
	enum mask_CR1 {
		BIDIMODE = 0x00008000,		/// Bidirectional data mode enable offset:15 width:1 
		BIDIOE = 0x00004000,		/// Output enable in bidirectional mode offset:14 width:1 
		CRCEN = 0x00002000,		/// Hardware CRC calculation enable offset:13 width:1 
		CRCNEXT = 0x00001000,		/// CRC transfer next offset:12 width:1 
		DFF = 0x00000800,		/// Data frame format offset:11 width:1 
		RXONLY = 0x00000400,		/// Receive only offset:10 width:1 
		SSM = 0x00000200,		/// Software slave management offset:9 width:1 
		SSI = 0x00000100,		/// Internal slave select offset:8 width:1 
		LSBFIRST = 0x00000080,		/// Frame format offset:7 width:1 
		SPE = 0x00000040,		/// SPI enable offset:6 width:1 
		BR = 0x00000038,		/// Baud rate control offset:3 width:3 
		MSTR = 0x00000004,		/// Master selection offset:2 width:1 
		CPOL = 0x00000002,		/// Clock polarity offset:1 width:1 
		CPHA = 0x00000001,		/// Clock phase offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TXEIE = 0x00000080,		/// Tx buffer empty interrupt enable offset:7 width:1 
		RXNEIE = 0x00000040,		/// RX buffer not empty interrupt enable offset:6 width:1 
		ERRIE = 0x00000020,		/// Error interrupt enable offset:5 width:1 
		FRF = 0x00000010,		/// Frame format offset:4 width:1 
		SSOE = 0x00000004,		/// SS output enable offset:2 width:1 
		TXDMAEN = 0x00000002,		/// Tx buffer DMA enable offset:1 width:1 
		RXDMAEN = 0x00000001,		/// Rx buffer DMA enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		TIFRFE = 0x00000100,		/// TI frame format error offset:8 width:1 
		BSY = 0x00000080,		/// Busy flag offset:7 width:1 
		OVR = 0x00000040,		/// Overrun flag offset:6 width:1 
		MODF = 0x00000020,		/// Mode fault offset:5 width:1 
		CRCERR = 0x00000010,		/// CRC error flag offset:4 width:1 
		UDR = 0x00000008,		/// Underrun flag offset:3 width:1 
		CHSIDE = 0x00000004,		/// Channel side offset:2 width:1 
		TXE = 0x00000002,		/// Transmit buffer empty offset:1 width:1 
		RXNE = 0x00000001,		/// Receive buffer not empty offset:0 width:1 
	}
	/// data register
	enum mask_DR {
		DR = 0x0000FFFF,		/// Data register offset:0 width:16 
	}
	/// CRC polynomial register
	enum mask_CRCPR {
		CRCPOLY = 0x0000FFFF,		/// CRC polynomial register offset:0 width:16 
	}
	/// RX CRC register
	enum mask_RXCRCR {
		RxCRC = 0x0000FFFF,		/// Rx CRC register offset:0 width:16 
	}
	/// TX CRC register
	enum mask_TXCRCR {
		TxCRC = 0x0000FFFF,		/// Tx CRC register offset:0 width:16 
	}
	/// I2S configuration register
	enum mask_I2SCFGR {
		I2SMOD = 0x00000800,		/// I2S mode selection offset:11 width:1 
		I2SE = 0x00000400,		/// I2S Enable offset:10 width:1 
		I2SCFG = 0x00000300,		/// I2S configuration mode offset:8 width:2 
		PCMSYNC = 0x00000080,		/// PCM frame synchronization offset:7 width:1 
		I2SSTD = 0x00000030,		/// I2S standard selection offset:4 width:2 
		CKPOL = 0x00000008,		/// Steady state clock polarity offset:3 width:1 
		DATLEN = 0x00000006,		/// Data length to be transferred offset:1 width:2 
		CHLEN = 0x00000001,		/// Channel length (number of bits per audio channel) offset:0 width:1 
	}
	/// I2S prescaler register
	enum mask_I2SPR {
		MCKOE = 0x00000200,		/// Master clock output enable offset:9 width:1 
		ODD = 0x00000100,		/// Odd factor for the prescaler offset:8 width:1 
		I2SDIV = 0x000000FF,		/// I2S Linear prescaler offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DR,uint) DR;		/// data register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CRCPR,uint) CRCPR;		/// CRC polynomial register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RXCRCR,uint) RXCRCR;		/// RX CRC register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_TXCRCR,uint) TXCRCR;		/// TX CRC register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_I2SCFGR,uint) I2SCFGR;		/// I2S configuration register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_I2SPR,uint) I2SPR;		/// I2S prescaler register offset:0x00000020
}
/// Nullable.null address:0x40004000
struct I2S3ext_Type{
	@disable this();
	/// control register 1
	enum mask_CR1 {
		BIDIMODE = 0x00008000,		/// Bidirectional data mode enable offset:15 width:1 
		BIDIOE = 0x00004000,		/// Output enable in bidirectional mode offset:14 width:1 
		CRCEN = 0x00002000,		/// Hardware CRC calculation enable offset:13 width:1 
		CRCNEXT = 0x00001000,		/// CRC transfer next offset:12 width:1 
		DFF = 0x00000800,		/// Data frame format offset:11 width:1 
		RXONLY = 0x00000400,		/// Receive only offset:10 width:1 
		SSM = 0x00000200,		/// Software slave management offset:9 width:1 
		SSI = 0x00000100,		/// Internal slave select offset:8 width:1 
		LSBFIRST = 0x00000080,		/// Frame format offset:7 width:1 
		SPE = 0x00000040,		/// SPI enable offset:6 width:1 
		BR = 0x00000038,		/// Baud rate control offset:3 width:3 
		MSTR = 0x00000004,		/// Master selection offset:2 width:1 
		CPOL = 0x00000002,		/// Clock polarity offset:1 width:1 
		CPHA = 0x00000001,		/// Clock phase offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TXEIE = 0x00000080,		/// Tx buffer empty interrupt enable offset:7 width:1 
		RXNEIE = 0x00000040,		/// RX buffer not empty interrupt enable offset:6 width:1 
		ERRIE = 0x00000020,		/// Error interrupt enable offset:5 width:1 
		FRF = 0x00000010,		/// Frame format offset:4 width:1 
		SSOE = 0x00000004,		/// SS output enable offset:2 width:1 
		TXDMAEN = 0x00000002,		/// Tx buffer DMA enable offset:1 width:1 
		RXDMAEN = 0x00000001,		/// Rx buffer DMA enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		TIFRFE = 0x00000100,		/// TI frame format error offset:8 width:1 
		BSY = 0x00000080,		/// Busy flag offset:7 width:1 
		OVR = 0x00000040,		/// Overrun flag offset:6 width:1 
		MODF = 0x00000020,		/// Mode fault offset:5 width:1 
		CRCERR = 0x00000010,		/// CRC error flag offset:4 width:1 
		UDR = 0x00000008,		/// Underrun flag offset:3 width:1 
		CHSIDE = 0x00000004,		/// Channel side offset:2 width:1 
		TXE = 0x00000002,		/// Transmit buffer empty offset:1 width:1 
		RXNE = 0x00000001,		/// Receive buffer not empty offset:0 width:1 
	}
	/// data register
	enum mask_DR {
		DR = 0x0000FFFF,		/// Data register offset:0 width:16 
	}
	/// CRC polynomial register
	enum mask_CRCPR {
		CRCPOLY = 0x0000FFFF,		/// CRC polynomial register offset:0 width:16 
	}
	/// RX CRC register
	enum mask_RXCRCR {
		RxCRC = 0x0000FFFF,		/// Rx CRC register offset:0 width:16 
	}
	/// TX CRC register
	enum mask_TXCRCR {
		TxCRC = 0x0000FFFF,		/// Tx CRC register offset:0 width:16 
	}
	/// I2S configuration register
	enum mask_I2SCFGR {
		I2SMOD = 0x00000800,		/// I2S mode selection offset:11 width:1 
		I2SE = 0x00000400,		/// I2S Enable offset:10 width:1 
		I2SCFG = 0x00000300,		/// I2S configuration mode offset:8 width:2 
		PCMSYNC = 0x00000080,		/// PCM frame synchronization offset:7 width:1 
		I2SSTD = 0x00000030,		/// I2S standard selection offset:4 width:2 
		CKPOL = 0x00000008,		/// Steady state clock polarity offset:3 width:1 
		DATLEN = 0x00000006,		/// Data length to be transferred offset:1 width:2 
		CHLEN = 0x00000001,		/// Channel length (number of bits per audio channel) offset:0 width:1 
	}
	/// I2S prescaler register
	enum mask_I2SPR {
		MCKOE = 0x00000200,		/// Master clock output enable offset:9 width:1 
		ODD = 0x00000100,		/// Odd factor for the prescaler offset:8 width:1 
		I2SDIV = 0x000000FF,		/// I2S Linear prescaler offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DR,uint) DR;		/// data register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CRCPR,uint) CRCPR;		/// CRC polynomial register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RXCRCR,uint) RXCRCR;		/// RX CRC register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_TXCRCR,uint) TXCRCR;		/// TX CRC register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_I2SCFGR,uint) I2SCFGR;		/// I2S configuration register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_I2SPR,uint) I2SPR;		/// I2S prescaler register offset:0x00000020
}
/// Secure digital input/output interface address:0x40012C00
struct SDIO_Type{
	@disable this();
	enum Interrupt {
		SDIO = 49,		/// SDIO global interrupt
	};
	/// power control register
	enum mask_POWER {
		PWRCTRL = 0x00000003,		/// PWRCTRL offset:0 width:2 
	}
	/// SDI clock control register
	enum mask_CLKCR {
		HWFC_EN = 0x00004000,		/// HW Flow Control enable offset:14 width:1 
		NEGEDGE = 0x00002000,		/// SDIO_CK dephasing selection bit offset:13 width:1 
		WIDBUS = 0x00001800,		/// Wide bus mode enable bit offset:11 width:2 
		BYPASS = 0x00000400,		/// Clock divider bypass enable bit offset:10 width:1 
		PWRSAV = 0x00000200,		/// Power saving configuration bit offset:9 width:1 
		CLKEN = 0x00000100,		/// Clock enable bit offset:8 width:1 
		CLKDIV = 0x000000FF,		/// Clock divide factor offset:0 width:8 
	}
	/// argument register
	enum mask_ARG {
		CMDARG = 0xFFFFFFFF,		/// Command argument offset:0 width:32 
	}
	/// command register
	enum mask_CMD {
		CE_ATACMD = 0x00004000,		/// CE-ATA command offset:14 width:1 
		nIEN = 0x00002000,		/// not Interrupt Enable offset:13 width:1 
		ENCMDcompl = 0x00001000,		/// Enable CMD completion offset:12 width:1 
		SDIOSuspend = 0x00000800,		/// SD I/O suspend command offset:11 width:1 
		CPSMEN = 0x00000400,		/// Command path state machine (CPSM) Enable bit offset:10 width:1 
		WAITPEND = 0x00000200,		/// CPSM Waits for ends of data transfer (CmdPend internal signal). offset:9 width:1 
		WAITINT = 0x00000100,		/// CPSM waits for interrupt request offset:8 width:1 
		WAITRESP = 0x000000C0,		/// Wait for response bits offset:6 width:2 
		CMDINDEX = 0x0000003F,		/// Command index offset:0 width:6 
	}
	/// command response register
	enum mask_RESPCMD {
		RESPCMD = 0x0000003F,		/// Response command index offset:0 width:6 
	}
	/// response 1..4 register
	enum mask_RESP1 {
		CARDSTATUS1 = 0xFFFFFFFF,		/// Card Status offset:0 width:32 
	}
	/// response 1..4 register
	enum mask_RESP2 {
		CARDSTATUS2 = 0xFFFFFFFF,		/// Card Status offset:0 width:32 
	}
	/// response 1..4 register
	enum mask_RESP3 {
		CARDSTATUS3 = 0xFFFFFFFF,		/// Card Status offset:0 width:32 
	}
	/// response 1..4 register
	enum mask_RESP4 {
		CARDSTATUS4 = 0xFFFFFFFF,		/// Card Status offset:0 width:32 
	}
	/// data timer register
	enum mask_DTIMER {
		DATATIME = 0xFFFFFFFF,		/// Data timeout period offset:0 width:32 
	}
	/// data length register
	enum mask_DLEN {
		DATALENGTH = 0x01FFFFFF,		/// Data length value offset:0 width:25 
	}
	/// data control register
	enum mask_DCTRL {
		SDIOEN = 0x00000800,		/// SD I/O enable functions offset:11 width:1 
		RWMOD = 0x00000400,		/// Read wait mode offset:10 width:1 
		RWSTOP = 0x00000200,		/// Read wait stop offset:9 width:1 
		RWSTART = 0x00000100,		/// Read wait start offset:8 width:1 
		DBLOCKSIZE = 0x000000F0,		/// Data block size offset:4 width:4 
		DMAEN = 0x00000008,		/// DMA enable bit offset:3 width:1 
		DTMODE = 0x00000004,		/// Data transfer mode selection 1: Stream or SDIO multibyte data transfer. offset:2 width:1 
		DTDIR = 0x00000002,		/// Data transfer direction selection offset:1 width:1 
		DTEN = 0x00000001,		/// DTEN offset:0 width:1 
	}
	/// data counter register
	enum mask_DCOUNT {
		DATACOUNT = 0x01FFFFFF,		/// Data count value offset:0 width:25 
	}
	/// status register
	enum mask_STA {
		CEATAEND = 0x00800000,		/// CE-ATA command completion signal received for CMD61 offset:23 width:1 
		SDIOIT = 0x00400000,		/// SDIO interrupt received offset:22 width:1 
		RXDAVL = 0x00200000,		/// Data available in receive FIFO offset:21 width:1 
		TXDAVL = 0x00100000,		/// Data available in transmit FIFO offset:20 width:1 
		RXFIFOE = 0x00080000,		/// Receive FIFO empty offset:19 width:1 
		TXFIFOE = 0x00040000,		/// Transmit FIFO empty offset:18 width:1 
		RXFIFOF = 0x00020000,		/// Receive FIFO full offset:17 width:1 
		TXFIFOF = 0x00010000,		/// Transmit FIFO full offset:16 width:1 
		RXFIFOHF = 0x00008000,		/// Receive FIFO half full: there are at least 8 words in the FIFO offset:15 width:1 
		TXFIFOHE = 0x00004000,		/// Transmit FIFO half empty: at least 8 words can be written into the FIFO offset:14 width:1 
		RXACT = 0x00002000,		/// Data receive in progress offset:13 width:1 
		TXACT = 0x00001000,		/// Data transmit in progress offset:12 width:1 
		CMDACT = 0x00000800,		/// Command transfer in progress offset:11 width:1 
		DBCKEND = 0x00000400,		/// Data block sent/received (CRC check passed) offset:10 width:1 
		STBITERR = 0x00000200,		/// Start bit not detected on all data signals in wide bus mode offset:9 width:1 
		DATAEND = 0x00000100,		/// Data end (data counter, SDIDCOUNT, is zero) offset:8 width:1 
		CMDSENT = 0x00000080,		/// Command sent (no response required) offset:7 width:1 
		CMDREND = 0x00000040,		/// Command response received (CRC check passed) offset:6 width:1 
		RXOVERR = 0x00000020,		/// Received FIFO overrun error offset:5 width:1 
		TXUNDERR = 0x00000010,		/// Transmit FIFO underrun error offset:4 width:1 
		DTIMEOUT = 0x00000008,		/// Data timeout offset:3 width:1 
		CTIMEOUT = 0x00000004,		/// Command response timeout offset:2 width:1 
		DCRCFAIL = 0x00000002,		/// Data block sent/received (CRC check failed) offset:1 width:1 
		CCRCFAIL = 0x00000001,		/// Command response received (CRC check failed) offset:0 width:1 
	}
	/// interrupt clear register
	enum mask_ICR {
		CEATAENDC = 0x00800000,		/// CEATAEND flag clear bit offset:23 width:1 
		SDIOITC = 0x00400000,		/// SDIOIT flag clear bit offset:22 width:1 
		DBCKENDC = 0x00000400,		/// DBCKEND flag clear bit offset:10 width:1 
		STBITERRC = 0x00000200,		/// STBITERR flag clear bit offset:9 width:1 
		DATAENDC = 0x00000100,		/// DATAEND flag clear bit offset:8 width:1 
		CMDSENTC = 0x00000080,		/// CMDSENT flag clear bit offset:7 width:1 
		CMDRENDC = 0x00000040,		/// CMDREND flag clear bit offset:6 width:1 
		RXOVERRC = 0x00000020,		/// RXOVERR flag clear bit offset:5 width:1 
		TXUNDERRC = 0x00000010,		/// TXUNDERR flag clear bit offset:4 width:1 
		DTIMEOUTC = 0x00000008,		/// DTIMEOUT flag clear bit offset:3 width:1 
		CTIMEOUTC = 0x00000004,		/// CTIMEOUT flag clear bit offset:2 width:1 
		DCRCFAILC = 0x00000002,		/// DCRCFAIL flag clear bit offset:1 width:1 
		CCRCFAILC = 0x00000001,		/// CCRCFAIL flag clear bit offset:0 width:1 
	}
	/// mask register
	enum mask_MASK {
		CEATAENDIE = 0x00800000,		/// CE-ATA command completion signal received interrupt enable offset:23 width:1 
		SDIOITIE = 0x00400000,		/// SDIO mode interrupt received interrupt enable offset:22 width:1 
		RXDAVLIE = 0x00200000,		/// Data available in Rx FIFO interrupt enable offset:21 width:1 
		TXDAVLIE = 0x00100000,		/// Data available in Tx FIFO interrupt enable offset:20 width:1 
		RXFIFOEIE = 0x00080000,		/// Rx FIFO empty interrupt enable offset:19 width:1 
		TXFIFOEIE = 0x00040000,		/// Tx FIFO empty interrupt enable offset:18 width:1 
		RXFIFOFIE = 0x00020000,		/// Rx FIFO full interrupt enable offset:17 width:1 
		TXFIFOFIE = 0x00010000,		/// Tx FIFO full interrupt enable offset:16 width:1 
		RXFIFOHFIE = 0x00008000,		/// Rx FIFO half full interrupt enable offset:15 width:1 
		TXFIFOHEIE = 0x00004000,		/// Tx FIFO half empty interrupt enable offset:14 width:1 
		RXACTIE = 0x00002000,		/// Data receive acting interrupt enable offset:13 width:1 
		TXACTIE = 0x00001000,		/// Data transmit acting interrupt enable offset:12 width:1 
		CMDACTIE = 0x00000800,		/// Command acting interrupt enable offset:11 width:1 
		DBCKENDIE = 0x00000400,		/// Data block end interrupt enable offset:10 width:1 
		STBITERRIE = 0x00000200,		/// Start bit error interrupt enable offset:9 width:1 
		DATAENDIE = 0x00000100,		/// Data end interrupt enable offset:8 width:1 
		CMDSENTIE = 0x00000080,		/// Command sent interrupt enable offset:7 width:1 
		CMDRENDIE = 0x00000040,		/// Command response received interrupt enable offset:6 width:1 
		RXOVERRIE = 0x00000020,		/// Rx FIFO overrun error interrupt enable offset:5 width:1 
		TXUNDERRIE = 0x00000010,		/// Tx FIFO underrun error interrupt enable offset:4 width:1 
		DTIMEOUTIE = 0x00000008,		/// Data timeout interrupt enable offset:3 width:1 
		CTIMEOUTIE = 0x00000004,		/// Command timeout interrupt enable offset:2 width:1 
		DCRCFAILIE = 0x00000002,		/// Data CRC fail interrupt enable offset:1 width:1 
		CCRCFAILIE = 0x00000001,		/// Command CRC fail interrupt enable offset:0 width:1 
	}
	/// FIFO counter register
	enum mask_FIFOCNT {
		FIFOCOUNT = 0x00FFFFFF,		/// Remaining number of words to be written to or read from the FIFO. offset:0 width:24 
	}
	/// data FIFO register
	enum mask_FIFO {
		FIFOData = 0xFFFFFFFF,		/// Receive and transmit FIFO data offset:0 width:32 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_POWER,uint) POWER;		/// power control register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CLKCR,uint) CLKCR;		/// SDI clock control register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_ARG,uint) ARG;		/// argument register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CMD,uint) CMD;		/// command register offset:0x0000000C
	@Register("read-only",0x10)	RO_Reg!(mask_RESPCMD,uint) RESPCMD;		/// command response register offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_RESP1,uint) RESP1;		/// response 1..4 register offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_RESP2,uint) RESP2;		/// response 1..4 register offset:0x00000018
	@Register("read-only",0x1C)	RO_Reg!(mask_RESP3,uint) RESP3;		/// response 1..4 register offset:0x0000001C
	@Register("read-only",0x20)	RO_Reg!(mask_RESP4,uint) RESP4;		/// response 1..4 register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_DTIMER,uint) DTIMER;		/// data timer register offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_DLEN,uint) DLEN;		/// data length register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_DCTRL,uint) DCTRL;		/// data control register offset:0x0000002C
	@Register("read-only",0x30)	RO_Reg!(mask_DCOUNT,uint) DCOUNT;		/// data counter register offset:0x00000030
	@Register("read-only",0x34)	RO_Reg!(mask_STA,uint) STA;		/// status register offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_ICR,uint) ICR;		/// interrupt clear register offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_MASK,uint) MASK;		/// mask register offset:0x0000003C
	uint[0x02]		Reserved0;
	@Register("read-only",0x48)	RO_Reg!(mask_FIFOCNT,uint) FIFOCNT;		/// FIFO counter register offset:0x00000048
	uint[0x0D]		Reserved1;
	@Register("read-write",0x80)	RW_Reg!(mask_FIFO,uint) FIFO;		/// data FIFO register offset:0x00000080
}
/// Analog-to-digital converter address:0x40012000
struct ADC1_Type{
	@disable this();
	enum Interrupt {
		ADC = 18,		/// ADC1 global interrupt
	};
	/// status register
	enum mask_SR {
		OVR = 0x00000020,		/// Overrun offset:5 width:1 
		STRT = 0x00000010,		/// Regular channel start flag offset:4 width:1 
		JSTRT = 0x00000008,		/// Injected channel start flag offset:3 width:1 
		JEOC = 0x00000004,		/// Injected channel end of conversion offset:2 width:1 
		EOC = 0x00000002,		/// Regular channel end of conversion offset:1 width:1 
		AWD = 0x00000001,		/// Analog watchdog flag offset:0 width:1 
	}
	/// control register 1
	enum mask_CR1 {
		OVRIE = 0x04000000,		/// Overrun interrupt enable offset:26 width:1 
		RES = 0x03000000,		/// Resolution offset:24 width:2 
		AWDEN = 0x00800000,		/// Analog watchdog enable on regular channels offset:23 width:1 
		JAWDEN = 0x00400000,		/// Analog watchdog enable on injected channels offset:22 width:1 
		DISCNUM = 0x0000E000,		/// Discontinuous mode channel count offset:13 width:3 
		JDISCEN = 0x00001000,		/// Discontinuous mode on injected channels offset:12 width:1 
		DISCEN = 0x00000800,		/// Discontinuous mode on regular channels offset:11 width:1 
		JAUTO = 0x00000400,		/// Automatic injected group conversion offset:10 width:1 
		AWDSGL = 0x00000200,		/// Enable the watchdog on a single channel in scan mode offset:9 width:1 
		SCAN = 0x00000100,		/// Scan mode offset:8 width:1 
		JEOCIE = 0x00000080,		/// Interrupt enable for injected channels offset:7 width:1 
		AWDIE = 0x00000040,		/// Analog watchdog interrupt enable offset:6 width:1 
		EOCIE = 0x00000020,		/// Interrupt enable for EOC offset:5 width:1 
		AWDCH = 0x0000001F,		/// Analog watchdog channel select bits offset:0 width:5 
	}
	/// control register 2
	enum mask_CR2 {
		SWSTART = 0x40000000,		/// Start conversion of regular channels offset:30 width:1 
		EXTEN = 0x30000000,		/// External trigger enable for regular channels offset:28 width:2 
		EXTSEL = 0x0F000000,		/// External event select for regular group offset:24 width:4 
		JSWSTART = 0x00400000,		/// Start conversion of injected channels offset:22 width:1 
		JEXTEN = 0x00300000,		/// External trigger enable for injected channels offset:20 width:2 
		JEXTSEL = 0x000F0000,		/// External event select for injected group offset:16 width:4 
		ALIGN = 0x00000800,		/// Data alignment offset:11 width:1 
		EOCS = 0x00000400,		/// End of conversion selection offset:10 width:1 
		DDS = 0x00000200,		/// DMA disable selection (for single ADC mode) offset:9 width:1 
		DMA = 0x00000100,		/// Direct memory access mode (for single ADC mode) offset:8 width:1 
		CONT = 0x00000002,		/// Continuous conversion offset:1 width:1 
		ADON = 0x00000001,		/// A/D Converter ON / OFF offset:0 width:1 
	}
	/// sample time register 1
	enum mask_SMPR1 {
		SMPx_x = 0xFFFFFFFF,		/// Sample time bits offset:0 width:32 
	}
	/// sample time register 2
	enum mask_SMPR2 {
		SMPx_x = 0xFFFFFFFF,		/// Sample time bits offset:0 width:32 
	}
	/// injected channel data offset register x
	enum mask_JOFR1 {
		JOFFSET1 = 0x00000FFF,		/// Data offset for injected channel x offset:0 width:12 
	}
	/// injected channel data offset register x
	enum mask_JOFR2 {
		JOFFSET2 = 0x00000FFF,		/// Data offset for injected channel x offset:0 width:12 
	}
	/// injected channel data offset register x
	enum mask_JOFR3 {
		JOFFSET3 = 0x00000FFF,		/// Data offset for injected channel x offset:0 width:12 
	}
	/// injected channel data offset register x
	enum mask_JOFR4 {
		JOFFSET4 = 0x00000FFF,		/// Data offset for injected channel x offset:0 width:12 
	}
	/// watchdog higher threshold register
	enum mask_HTR {
		HT = 0x00000FFF,		/// Analog watchdog higher threshold offset:0 width:12 
	}
	/// watchdog lower threshold register
	enum mask_LTR {
		LT = 0x00000FFF,		/// Analog watchdog lower threshold offset:0 width:12 
	}
	/// regular sequence register 1
	enum mask_SQR1 {
		L = 0x00F00000,		/// Regular channel sequence length offset:20 width:4 
		SQ16 = 0x000F8000,		/// 16th conversion in regular sequence offset:15 width:5 
		SQ15 = 0x00007C00,		/// 15th conversion in regular sequence offset:10 width:5 
		SQ14 = 0x000003E0,		/// 14th conversion in regular sequence offset:5 width:5 
		SQ13 = 0x0000001F,		/// 13th conversion in regular sequence offset:0 width:5 
	}
	/// regular sequence register 2
	enum mask_SQR2 {
		SQ12 = 0x3E000000,		/// 12th conversion in regular sequence offset:25 width:5 
		SQ11 = 0x01F00000,		/// 11th conversion in regular sequence offset:20 width:5 
		SQ10 = 0x000F8000,		/// 10th conversion in regular sequence offset:15 width:5 
		SQ9 = 0x00007C00,		/// 9th conversion in regular sequence offset:10 width:5 
		SQ8 = 0x000003E0,		/// 8th conversion in regular sequence offset:5 width:5 
		SQ7 = 0x0000001F,		/// 7th conversion in regular sequence offset:0 width:5 
	}
	/// regular sequence register 3
	enum mask_SQR3 {
		SQ6 = 0x3E000000,		/// 6th conversion in regular sequence offset:25 width:5 
		SQ5 = 0x01F00000,		/// 5th conversion in regular sequence offset:20 width:5 
		SQ4 = 0x000F8000,		/// 4th conversion in regular sequence offset:15 width:5 
		SQ3 = 0x00007C00,		/// 3rd conversion in regular sequence offset:10 width:5 
		SQ2 = 0x000003E0,		/// 2nd conversion in regular sequence offset:5 width:5 
		SQ1 = 0x0000001F,		/// 1st conversion in regular sequence offset:0 width:5 
	}
	/// injected sequence register
	enum mask_JSQR {
		JL = 0x00300000,		/// Injected sequence length offset:20 width:2 
		JSQ4 = 0x000F8000,		/// 4th conversion in injected sequence offset:15 width:5 
		JSQ3 = 0x00007C00,		/// 3rd conversion in injected sequence offset:10 width:5 
		JSQ2 = 0x000003E0,		/// 2nd conversion in injected sequence offset:5 width:5 
		JSQ1 = 0x0000001F,		/// 1st conversion in injected sequence offset:0 width:5 
	}
	/// injected data register x
	enum mask_JDR1 {
		JDATA = 0x0000FFFF,		/// Injected data offset:0 width:16 
	}
	/// injected data register x
	enum mask_JDR2 {
		JDATA = 0x0000FFFF,		/// Injected data offset:0 width:16 
	}
	/// injected data register x
	enum mask_JDR3 {
		JDATA = 0x0000FFFF,		/// Injected data offset:0 width:16 
	}
	/// injected data register x
	enum mask_JDR4 {
		JDATA = 0x0000FFFF,		/// Injected data offset:0 width:16 
	}
	/// regular data register
	enum mask_DR {
		DATA = 0x0000FFFF,		/// Regular data offset:0 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_SMPR1,uint) SMPR1;		/// sample time register 1 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SMPR2,uint) SMPR2;		/// sample time register 2 offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_JOFR1,uint) JOFR1;		/// injected channel data offset register x offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_JOFR2,uint) JOFR2;		/// injected channel data offset register x offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_JOFR3,uint) JOFR3;		/// injected channel data offset register x offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_JOFR4,uint) JOFR4;		/// injected channel data offset register x offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_HTR,uint) HTR;		/// watchdog higher threshold register offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_LTR,uint) LTR;		/// watchdog lower threshold register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_SQR1,uint) SQR1;		/// regular sequence register 1 offset:0x0000002C
	@Register("read-write",0x30)	RW_Reg!(mask_SQR2,uint) SQR2;		/// regular sequence register 2 offset:0x00000030
	@Register("read-write",0x34)	RW_Reg!(mask_SQR3,uint) SQR3;		/// regular sequence register 3 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_JSQR,uint) JSQR;		/// injected sequence register offset:0x00000038
	@Register("read-only",0x3C)	RO_Reg!(mask_JDR1,uint) JDR1;		/// injected data register x offset:0x0000003C
	@Register("read-only",0x40)	RO_Reg!(mask_JDR2,uint) JDR2;		/// injected data register x offset:0x00000040
	@Register("read-only",0x44)	RO_Reg!(mask_JDR3,uint) JDR3;		/// injected data register x offset:0x00000044
	@Register("read-only",0x48)	RO_Reg!(mask_JDR4,uint) JDR4;		/// injected data register x offset:0x00000048
	@Register("read-only",0x4C)	RO_Reg!(mask_DR,uint) DR;		/// regular data register offset:0x0000004C
}
/// Universal synchronous asynchronous receiver transmitter address:0x40011400
struct USART6_Type{
	@disable this();
	enum Interrupt {
		USART6 = 71,		/// USART6 global interrupt
	};
	/// Status register
	enum mask_SR {
		CTS = 0x00000200,		/// CTS flag offset:9 width:1 
		LBD = 0x00000100,		/// LIN break detection flag offset:8 width:1 
		TXE = 0x00000080,		/// Transmit data register empty offset:7 width:1 
		TC = 0x00000040,		/// Transmission complete offset:6 width:1 
		RXNE = 0x00000020,		/// Read data register not empty offset:5 width:1 
		IDLE = 0x00000010,		/// IDLE line detected offset:4 width:1 
		ORE = 0x00000008,		/// Overrun error offset:3 width:1 
		NF = 0x00000004,		/// Noise detected flag offset:2 width:1 
		FE = 0x00000002,		/// Framing error offset:1 width:1 
		PE = 0x00000001,		/// Parity error offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000001FF,		/// Data value offset:0 width:9 
	}
	/// Baud rate register
	enum mask_BRR {
		DIV_Mantissa = 0x0000FFF0,		/// mantissa of USARTDIV offset:4 width:12 
		DIV_Fraction = 0x0000000F,		/// fraction of USARTDIV offset:0 width:4 
	}
	/// Control register 1
	enum mask_CR1 {
		OVER8 = 0x00008000,		/// Oversampling mode offset:15 width:1 
		UE = 0x00002000,		/// USART enable offset:13 width:1 
		M = 0x00001000,		/// Word length offset:12 width:1 
		WAKE = 0x00000800,		/// Wakeup method offset:11 width:1 
		PCE = 0x00000400,		/// Parity control enable offset:10 width:1 
		PS = 0x00000200,		/// Parity selection offset:9 width:1 
		PEIE = 0x00000100,		/// PE interrupt enable offset:8 width:1 
		TXEIE = 0x00000080,		/// TXE interrupt enable offset:7 width:1 
		TCIE = 0x00000040,		/// Transmission complete interrupt enable offset:6 width:1 
		RXNEIE = 0x00000020,		/// RXNE interrupt enable offset:5 width:1 
		IDLEIE = 0x00000010,		/// IDLE interrupt enable offset:4 width:1 
		TE = 0x00000008,		/// Transmitter enable offset:3 width:1 
		RE = 0x00000004,		/// Receiver enable offset:2 width:1 
		RWU = 0x00000002,		/// Receiver wakeup offset:1 width:1 
		SBK = 0x00000001,		/// Send break offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LINEN = 0x00004000,		/// LIN mode enable offset:14 width:1 
		STOP = 0x00003000,		/// STOP bits offset:12 width:2 
		CLKEN = 0x00000800,		/// Clock enable offset:11 width:1 
		CPOL = 0x00000400,		/// Clock polarity offset:10 width:1 
		CPHA = 0x00000200,		/// Clock phase offset:9 width:1 
		LBCL = 0x00000100,		/// Last bit clock pulse offset:8 width:1 
		LBDIE = 0x00000040,		/// LIN break detection interrupt enable offset:6 width:1 
		LBDL = 0x00000020,		/// lin break detection length offset:5 width:1 
		ADD = 0x0000000F,		/// Address of the USART node offset:0 width:4 
	}
	/// Control register 3
	enum mask_CR3 {
		ONEBIT = 0x00000800,		/// One sample bit method enable offset:11 width:1 
		CTSIE = 0x00000400,		/// CTS interrupt enable offset:10 width:1 
		CTSE = 0x00000200,		/// CTS enable offset:9 width:1 
		RTSE = 0x00000100,		/// RTS enable offset:8 width:1 
		DMAT = 0x00000080,		/// DMA enable transmitter offset:7 width:1 
		DMAR = 0x00000040,		/// DMA enable receiver offset:6 width:1 
		SCEN = 0x00000020,		/// Smartcard mode enable offset:5 width:1 
		NACK = 0x00000010,		/// Smartcard NACK enable offset:4 width:1 
		HDSEL = 0x00000008,		/// Half-duplex selection offset:3 width:1 
		IRLP = 0x00000004,		/// IrDA low-power offset:2 width:1 
		IREN = 0x00000002,		/// IrDA mode enable offset:1 width:1 
		EIE = 0x00000001,		/// Error interrupt enable offset:0 width:1 
	}
	/// Guard time and prescaler register
	enum mask_GTPR {
		GT = 0x0000FF00,		/// Guard time value offset:8 width:8 
		PSC = 0x000000FF,		/// Prescaler value offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_SR,uint) SR;		/// Status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_BRR,uint) BRR;		/// Baud rate register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_CR3,uint) CR3;		/// Control register 3 offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_GTPR,uint) GTPR;		/// Guard time and prescaler register offset:0x00000018
}
/// Nullable.null address:0x40011000
struct USART1_Type{
	@disable this();
	enum Interrupt {
		USART1 = 37,		/// USART1 global interrupt
	};
	/// Status register
	enum mask_SR {
		CTS = 0x00000200,		/// CTS flag offset:9 width:1 
		LBD = 0x00000100,		/// LIN break detection flag offset:8 width:1 
		TXE = 0x00000080,		/// Transmit data register empty offset:7 width:1 
		TC = 0x00000040,		/// Transmission complete offset:6 width:1 
		RXNE = 0x00000020,		/// Read data register not empty offset:5 width:1 
		IDLE = 0x00000010,		/// IDLE line detected offset:4 width:1 
		ORE = 0x00000008,		/// Overrun error offset:3 width:1 
		NF = 0x00000004,		/// Noise detected flag offset:2 width:1 
		FE = 0x00000002,		/// Framing error offset:1 width:1 
		PE = 0x00000001,		/// Parity error offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000001FF,		/// Data value offset:0 width:9 
	}
	/// Baud rate register
	enum mask_BRR {
		DIV_Mantissa = 0x0000FFF0,		/// mantissa of USARTDIV offset:4 width:12 
		DIV_Fraction = 0x0000000F,		/// fraction of USARTDIV offset:0 width:4 
	}
	/// Control register 1
	enum mask_CR1 {
		OVER8 = 0x00008000,		/// Oversampling mode offset:15 width:1 
		UE = 0x00002000,		/// USART enable offset:13 width:1 
		M = 0x00001000,		/// Word length offset:12 width:1 
		WAKE = 0x00000800,		/// Wakeup method offset:11 width:1 
		PCE = 0x00000400,		/// Parity control enable offset:10 width:1 
		PS = 0x00000200,		/// Parity selection offset:9 width:1 
		PEIE = 0x00000100,		/// PE interrupt enable offset:8 width:1 
		TXEIE = 0x00000080,		/// TXE interrupt enable offset:7 width:1 
		TCIE = 0x00000040,		/// Transmission complete interrupt enable offset:6 width:1 
		RXNEIE = 0x00000020,		/// RXNE interrupt enable offset:5 width:1 
		IDLEIE = 0x00000010,		/// IDLE interrupt enable offset:4 width:1 
		TE = 0x00000008,		/// Transmitter enable offset:3 width:1 
		RE = 0x00000004,		/// Receiver enable offset:2 width:1 
		RWU = 0x00000002,		/// Receiver wakeup offset:1 width:1 
		SBK = 0x00000001,		/// Send break offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LINEN = 0x00004000,		/// LIN mode enable offset:14 width:1 
		STOP = 0x00003000,		/// STOP bits offset:12 width:2 
		CLKEN = 0x00000800,		/// Clock enable offset:11 width:1 
		CPOL = 0x00000400,		/// Clock polarity offset:10 width:1 
		CPHA = 0x00000200,		/// Clock phase offset:9 width:1 
		LBCL = 0x00000100,		/// Last bit clock pulse offset:8 width:1 
		LBDIE = 0x00000040,		/// LIN break detection interrupt enable offset:6 width:1 
		LBDL = 0x00000020,		/// lin break detection length offset:5 width:1 
		ADD = 0x0000000F,		/// Address of the USART node offset:0 width:4 
	}
	/// Control register 3
	enum mask_CR3 {
		ONEBIT = 0x00000800,		/// One sample bit method enable offset:11 width:1 
		CTSIE = 0x00000400,		/// CTS interrupt enable offset:10 width:1 
		CTSE = 0x00000200,		/// CTS enable offset:9 width:1 
		RTSE = 0x00000100,		/// RTS enable offset:8 width:1 
		DMAT = 0x00000080,		/// DMA enable transmitter offset:7 width:1 
		DMAR = 0x00000040,		/// DMA enable receiver offset:6 width:1 
		SCEN = 0x00000020,		/// Smartcard mode enable offset:5 width:1 
		NACK = 0x00000010,		/// Smartcard NACK enable offset:4 width:1 
		HDSEL = 0x00000008,		/// Half-duplex selection offset:3 width:1 
		IRLP = 0x00000004,		/// IrDA low-power offset:2 width:1 
		IREN = 0x00000002,		/// IrDA mode enable offset:1 width:1 
		EIE = 0x00000001,		/// Error interrupt enable offset:0 width:1 
	}
	/// Guard time and prescaler register
	enum mask_GTPR {
		GT = 0x0000FF00,		/// Guard time value offset:8 width:8 
		PSC = 0x000000FF,		/// Prescaler value offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_SR,uint) SR;		/// Status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_BRR,uint) BRR;		/// Baud rate register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_CR3,uint) CR3;		/// Control register 3 offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_GTPR,uint) GTPR;		/// Guard time and prescaler register offset:0x00000018
}
/// Nullable.null address:0x40004400
struct USART2_Type{
	@disable this();
	enum Interrupt {
		USART2 = 38,		/// USART2 global interrupt
	};
	/// Status register
	enum mask_SR {
		CTS = 0x00000200,		/// CTS flag offset:9 width:1 
		LBD = 0x00000100,		/// LIN break detection flag offset:8 width:1 
		TXE = 0x00000080,		/// Transmit data register empty offset:7 width:1 
		TC = 0x00000040,		/// Transmission complete offset:6 width:1 
		RXNE = 0x00000020,		/// Read data register not empty offset:5 width:1 
		IDLE = 0x00000010,		/// IDLE line detected offset:4 width:1 
		ORE = 0x00000008,		/// Overrun error offset:3 width:1 
		NF = 0x00000004,		/// Noise detected flag offset:2 width:1 
		FE = 0x00000002,		/// Framing error offset:1 width:1 
		PE = 0x00000001,		/// Parity error offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000001FF,		/// Data value offset:0 width:9 
	}
	/// Baud rate register
	enum mask_BRR {
		DIV_Mantissa = 0x0000FFF0,		/// mantissa of USARTDIV offset:4 width:12 
		DIV_Fraction = 0x0000000F,		/// fraction of USARTDIV offset:0 width:4 
	}
	/// Control register 1
	enum mask_CR1 {
		OVER8 = 0x00008000,		/// Oversampling mode offset:15 width:1 
		UE = 0x00002000,		/// USART enable offset:13 width:1 
		M = 0x00001000,		/// Word length offset:12 width:1 
		WAKE = 0x00000800,		/// Wakeup method offset:11 width:1 
		PCE = 0x00000400,		/// Parity control enable offset:10 width:1 
		PS = 0x00000200,		/// Parity selection offset:9 width:1 
		PEIE = 0x00000100,		/// PE interrupt enable offset:8 width:1 
		TXEIE = 0x00000080,		/// TXE interrupt enable offset:7 width:1 
		TCIE = 0x00000040,		/// Transmission complete interrupt enable offset:6 width:1 
		RXNEIE = 0x00000020,		/// RXNE interrupt enable offset:5 width:1 
		IDLEIE = 0x00000010,		/// IDLE interrupt enable offset:4 width:1 
		TE = 0x00000008,		/// Transmitter enable offset:3 width:1 
		RE = 0x00000004,		/// Receiver enable offset:2 width:1 
		RWU = 0x00000002,		/// Receiver wakeup offset:1 width:1 
		SBK = 0x00000001,		/// Send break offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LINEN = 0x00004000,		/// LIN mode enable offset:14 width:1 
		STOP = 0x00003000,		/// STOP bits offset:12 width:2 
		CLKEN = 0x00000800,		/// Clock enable offset:11 width:1 
		CPOL = 0x00000400,		/// Clock polarity offset:10 width:1 
		CPHA = 0x00000200,		/// Clock phase offset:9 width:1 
		LBCL = 0x00000100,		/// Last bit clock pulse offset:8 width:1 
		LBDIE = 0x00000040,		/// LIN break detection interrupt enable offset:6 width:1 
		LBDL = 0x00000020,		/// lin break detection length offset:5 width:1 
		ADD = 0x0000000F,		/// Address of the USART node offset:0 width:4 
	}
	/// Control register 3
	enum mask_CR3 {
		ONEBIT = 0x00000800,		/// One sample bit method enable offset:11 width:1 
		CTSIE = 0x00000400,		/// CTS interrupt enable offset:10 width:1 
		CTSE = 0x00000200,		/// CTS enable offset:9 width:1 
		RTSE = 0x00000100,		/// RTS enable offset:8 width:1 
		DMAT = 0x00000080,		/// DMA enable transmitter offset:7 width:1 
		DMAR = 0x00000040,		/// DMA enable receiver offset:6 width:1 
		SCEN = 0x00000020,		/// Smartcard mode enable offset:5 width:1 
		NACK = 0x00000010,		/// Smartcard NACK enable offset:4 width:1 
		HDSEL = 0x00000008,		/// Half-duplex selection offset:3 width:1 
		IRLP = 0x00000004,		/// IrDA low-power offset:2 width:1 
		IREN = 0x00000002,		/// IrDA mode enable offset:1 width:1 
		EIE = 0x00000001,		/// Error interrupt enable offset:0 width:1 
	}
	/// Guard time and prescaler register
	enum mask_GTPR {
		GT = 0x0000FF00,		/// Guard time value offset:8 width:8 
		PSC = 0x000000FF,		/// Prescaler value offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_SR,uint) SR;		/// Status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_BRR,uint) BRR;		/// Baud rate register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_CR3,uint) CR3;		/// Control register 3 offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_GTPR,uint) GTPR;		/// Guard time and prescaler register offset:0x00000018
}
/// Power control address:0x40007000
struct PWR_Type{
	@disable this();
	enum Interrupt {
		PVD = 1,		/// PVD through EXTI line detection interrupt
	};
	/// power control register
	enum mask_CR {
		VOS = 0x0000C000,		/// Regulator voltage scaling output selection offset:14 width:2 
		ADCDC1 = 0x00002000,		/// ADCDC1 offset:13 width:1 
		FPDS = 0x00000200,		/// Flash power down in Stop mode offset:9 width:1 
		DBP = 0x00000100,		/// Disable backup domain write protection offset:8 width:1 
		PLS = 0x000000E0,		/// PVD level selection offset:5 width:3 
		PVDE = 0x00000010,		/// Power voltage detector enable offset:4 width:1 
		CSBF = 0x00000008,		/// Clear standby flag offset:3 width:1 
		CWUF = 0x00000004,		/// Clear wakeup flag offset:2 width:1 
		PDDS = 0x00000002,		/// Power down deepsleep offset:1 width:1 
		LPDS = 0x00000001,		/// Low-power deep sleep offset:0 width:1 
	}
	/// power control/status register
	enum mask_CSR {
		WUF = 0x00000001,		/// Wakeup flag offset:0 width:1 
		SBF = 0x00000002,		/// Standby flag offset:1 width:1 
		PVDO = 0x00000004,		/// PVD output offset:2 width:1 
		BRR = 0x00000008,		/// Backup regulator ready offset:3 width:1 
		EWUP = 0x00000100,		/// Enable WKUP pin offset:8 width:1 
		BRE = 0x00000200,		/// Backup regulator enable offset:9 width:1 
		VOSRDY = 0x00004000,		/// Regulator voltage scaling output selection ready bit offset:14 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR,uint) CR;		/// power control register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CSR,uint) CSR;		/// power control/status register offset:0x00000004
}
/// Inter-integrated circuit address:0x40005C00
struct I2C3_Type{
	@disable this();
	enum Interrupt {
		I2C3_ER = 73,		/// I2C3 error interrupt
		I2C3_EV = 72,		/// I2C3 event interrupt
	};
	/// Control register 1
	enum mask_CR1 {
		SWRST = 0x00008000,		/// Software reset offset:15 width:1 
		ALERT = 0x00002000,		/// SMBus alert offset:13 width:1 
		PEC = 0x00001000,		/// Packet error checking offset:12 width:1 
		POS = 0x00000800,		/// Acknowledge/PEC Position (for data reception) offset:11 width:1 
		ACK = 0x00000400,		/// Acknowledge enable offset:10 width:1 
		STOP = 0x00000200,		/// Stop generation offset:9 width:1 
		START = 0x00000100,		/// Start generation offset:8 width:1 
		NOSTRETCH = 0x00000080,		/// Clock stretching disable (Slave mode) offset:7 width:1 
		ENGC = 0x00000040,		/// General call enable offset:6 width:1 
		ENPEC = 0x00000020,		/// PEC enable offset:5 width:1 
		ENARP = 0x00000010,		/// ARP enable offset:4 width:1 
		SMBTYPE = 0x00000008,		/// SMBus type offset:3 width:1 
		SMBUS = 0x00000002,		/// SMBus mode offset:1 width:1 
		PE = 0x00000001,		/// Peripheral enable offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LAST = 0x00001000,		/// DMA last transfer offset:12 width:1 
		DMAEN = 0x00000800,		/// DMA requests enable offset:11 width:1 
		ITBUFEN = 0x00000400,		/// Buffer interrupt enable offset:10 width:1 
		ITEVTEN = 0x00000200,		/// Event interrupt enable offset:9 width:1 
		ITERREN = 0x00000100,		/// Error interrupt enable offset:8 width:1 
		FREQ = 0x0000003F,		/// Peripheral clock frequency offset:0 width:6 
	}
	/// Own address register 1
	enum mask_OAR1 {
		ADDMODE = 0x00008000,		/// Addressing mode (slave mode) offset:15 width:1 
		ADD10 = 0x00000300,		/// Interface address offset:8 width:2 
		ADD7 = 0x000000FE,		/// Interface address offset:1 width:7 
		ADD0 = 0x00000001,		/// Interface address offset:0 width:1 
	}
	/// Own address register 2
	enum mask_OAR2 {
		ADD2 = 0x000000FE,		/// Interface address offset:1 width:7 
		ENDUAL = 0x00000001,		/// Dual addressing mode enable offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000000FF,		/// 8-bit data register offset:0 width:8 
	}
	/// Status register 1
	enum mask_SR1 {
		SMBALERT = 0x00008000,		/// SMBus alert offset:15 width:1 
		TIMEOUT = 0x00004000,		/// Timeout or Tlow error offset:14 width:1 
		PECERR = 0x00001000,		/// PEC Error in reception offset:12 width:1 
		OVR = 0x00000800,		/// Overrun/Underrun offset:11 width:1 
		AF = 0x00000400,		/// Acknowledge failure offset:10 width:1 
		ARLO = 0x00000200,		/// Arbitration lost (master mode) offset:9 width:1 
		BERR = 0x00000100,		/// Bus error offset:8 width:1 
		TxE = 0x00000080,		/// Data register empty (transmitters) offset:7 width:1 
		RxNE = 0x00000040,		/// Data register not empty (receivers) offset:6 width:1 
		STOPF = 0x00000010,		/// Stop detection (slave mode) offset:4 width:1 
		ADD10 = 0x00000008,		/// 10-bit header sent (Master mode) offset:3 width:1 
		BTF = 0x00000004,		/// Byte transfer finished offset:2 width:1 
		ADDR = 0x00000002,		/// Address sent (master mode)/matched (slave mode) offset:1 width:1 
		SB = 0x00000001,		/// Start bit (Master mode) offset:0 width:1 
	}
	/// Status register 2
	enum mask_SR2 {
		PEC = 0x0000FF00,		/// acket error checking register offset:8 width:8 
		DUALF = 0x00000080,		/// Dual flag (Slave mode) offset:7 width:1 
		SMBHOST = 0x00000040,		/// SMBus host header (Slave mode) offset:6 width:1 
		SMBDEFAULT = 0x00000020,		/// SMBus device default address (Slave mode) offset:5 width:1 
		GENCALL = 0x00000010,		/// General call address (Slave mode) offset:4 width:1 
		TRA = 0x00000004,		/// Transmitter/receiver offset:2 width:1 
		BUSY = 0x00000002,		/// Bus busy offset:1 width:1 
		MSL = 0x00000001,		/// Master/slave offset:0 width:1 
	}
	/// Clock control register
	enum mask_CCR {
		F_S = 0x00008000,		/// I2C master mode selection offset:15 width:1 
		DUTY = 0x00004000,		/// Fast mode duty cycle offset:14 width:1 
		CCR = 0x00000FFF,		/// Clock control register in Fast/Standard mode (Master mode) offset:0 width:12 
	}
	/// TRISE register
	enum mask_TRISE {
		TRISE = 0x0000003F,		/// Maximum rise time in Fast/Standard mode (Master mode) offset:0 width:6 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OAR1,uint) OAR1;		/// Own address register 1 offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_OAR2,uint) OAR2;		/// Own address register 2 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_SR1,uint) SR1;		/// Status register 1 offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_SR2,uint) SR2;		/// Status register 2 offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_CCR,uint) CCR;		/// Clock control register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_TRISE,uint) TRISE;		/// TRISE register offset:0x00000020
}
/// Nullable.null address:0x40005800
struct I2C2_Type{
	@disable this();
	enum Interrupt {
		I2C2_ER = 34,		/// I2C2 error interrupt
		I2C2_EV = 33,		/// I2C2 event interrupt
	};
	/// Control register 1
	enum mask_CR1 {
		SWRST = 0x00008000,		/// Software reset offset:15 width:1 
		ALERT = 0x00002000,		/// SMBus alert offset:13 width:1 
		PEC = 0x00001000,		/// Packet error checking offset:12 width:1 
		POS = 0x00000800,		/// Acknowledge/PEC Position (for data reception) offset:11 width:1 
		ACK = 0x00000400,		/// Acknowledge enable offset:10 width:1 
		STOP = 0x00000200,		/// Stop generation offset:9 width:1 
		START = 0x00000100,		/// Start generation offset:8 width:1 
		NOSTRETCH = 0x00000080,		/// Clock stretching disable (Slave mode) offset:7 width:1 
		ENGC = 0x00000040,		/// General call enable offset:6 width:1 
		ENPEC = 0x00000020,		/// PEC enable offset:5 width:1 
		ENARP = 0x00000010,		/// ARP enable offset:4 width:1 
		SMBTYPE = 0x00000008,		/// SMBus type offset:3 width:1 
		SMBUS = 0x00000002,		/// SMBus mode offset:1 width:1 
		PE = 0x00000001,		/// Peripheral enable offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LAST = 0x00001000,		/// DMA last transfer offset:12 width:1 
		DMAEN = 0x00000800,		/// DMA requests enable offset:11 width:1 
		ITBUFEN = 0x00000400,		/// Buffer interrupt enable offset:10 width:1 
		ITEVTEN = 0x00000200,		/// Event interrupt enable offset:9 width:1 
		ITERREN = 0x00000100,		/// Error interrupt enable offset:8 width:1 
		FREQ = 0x0000003F,		/// Peripheral clock frequency offset:0 width:6 
	}
	/// Own address register 1
	enum mask_OAR1 {
		ADDMODE = 0x00008000,		/// Addressing mode (slave mode) offset:15 width:1 
		ADD10 = 0x00000300,		/// Interface address offset:8 width:2 
		ADD7 = 0x000000FE,		/// Interface address offset:1 width:7 
		ADD0 = 0x00000001,		/// Interface address offset:0 width:1 
	}
	/// Own address register 2
	enum mask_OAR2 {
		ADD2 = 0x000000FE,		/// Interface address offset:1 width:7 
		ENDUAL = 0x00000001,		/// Dual addressing mode enable offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000000FF,		/// 8-bit data register offset:0 width:8 
	}
	/// Status register 1
	enum mask_SR1 {
		SMBALERT = 0x00008000,		/// SMBus alert offset:15 width:1 
		TIMEOUT = 0x00004000,		/// Timeout or Tlow error offset:14 width:1 
		PECERR = 0x00001000,		/// PEC Error in reception offset:12 width:1 
		OVR = 0x00000800,		/// Overrun/Underrun offset:11 width:1 
		AF = 0x00000400,		/// Acknowledge failure offset:10 width:1 
		ARLO = 0x00000200,		/// Arbitration lost (master mode) offset:9 width:1 
		BERR = 0x00000100,		/// Bus error offset:8 width:1 
		TxE = 0x00000080,		/// Data register empty (transmitters) offset:7 width:1 
		RxNE = 0x00000040,		/// Data register not empty (receivers) offset:6 width:1 
		STOPF = 0x00000010,		/// Stop detection (slave mode) offset:4 width:1 
		ADD10 = 0x00000008,		/// 10-bit header sent (Master mode) offset:3 width:1 
		BTF = 0x00000004,		/// Byte transfer finished offset:2 width:1 
		ADDR = 0x00000002,		/// Address sent (master mode)/matched (slave mode) offset:1 width:1 
		SB = 0x00000001,		/// Start bit (Master mode) offset:0 width:1 
	}
	/// Status register 2
	enum mask_SR2 {
		PEC = 0x0000FF00,		/// acket error checking register offset:8 width:8 
		DUALF = 0x00000080,		/// Dual flag (Slave mode) offset:7 width:1 
		SMBHOST = 0x00000040,		/// SMBus host header (Slave mode) offset:6 width:1 
		SMBDEFAULT = 0x00000020,		/// SMBus device default address (Slave mode) offset:5 width:1 
		GENCALL = 0x00000010,		/// General call address (Slave mode) offset:4 width:1 
		TRA = 0x00000004,		/// Transmitter/receiver offset:2 width:1 
		BUSY = 0x00000002,		/// Bus busy offset:1 width:1 
		MSL = 0x00000001,		/// Master/slave offset:0 width:1 
	}
	/// Clock control register
	enum mask_CCR {
		F_S = 0x00008000,		/// I2C master mode selection offset:15 width:1 
		DUTY = 0x00004000,		/// Fast mode duty cycle offset:14 width:1 
		CCR = 0x00000FFF,		/// Clock control register in Fast/Standard mode (Master mode) offset:0 width:12 
	}
	/// TRISE register
	enum mask_TRISE {
		TRISE = 0x0000003F,		/// Maximum rise time in Fast/Standard mode (Master mode) offset:0 width:6 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OAR1,uint) OAR1;		/// Own address register 1 offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_OAR2,uint) OAR2;		/// Own address register 2 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_SR1,uint) SR1;		/// Status register 1 offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_SR2,uint) SR2;		/// Status register 2 offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_CCR,uint) CCR;		/// Clock control register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_TRISE,uint) TRISE;		/// TRISE register offset:0x00000020
}
/// Nullable.null address:0x40005400
struct I2C1_Type{
	@disable this();
	enum Interrupt {
		I2C1_ER = 32,		/// I2C1 error interrupt
		I2C1_EV = 31,		/// I2C1 event interrupt
	};
	/// Control register 1
	enum mask_CR1 {
		SWRST = 0x00008000,		/// Software reset offset:15 width:1 
		ALERT = 0x00002000,		/// SMBus alert offset:13 width:1 
		PEC = 0x00001000,		/// Packet error checking offset:12 width:1 
		POS = 0x00000800,		/// Acknowledge/PEC Position (for data reception) offset:11 width:1 
		ACK = 0x00000400,		/// Acknowledge enable offset:10 width:1 
		STOP = 0x00000200,		/// Stop generation offset:9 width:1 
		START = 0x00000100,		/// Start generation offset:8 width:1 
		NOSTRETCH = 0x00000080,		/// Clock stretching disable (Slave mode) offset:7 width:1 
		ENGC = 0x00000040,		/// General call enable offset:6 width:1 
		ENPEC = 0x00000020,		/// PEC enable offset:5 width:1 
		ENARP = 0x00000010,		/// ARP enable offset:4 width:1 
		SMBTYPE = 0x00000008,		/// SMBus type offset:3 width:1 
		SMBUS = 0x00000002,		/// SMBus mode offset:1 width:1 
		PE = 0x00000001,		/// Peripheral enable offset:0 width:1 
	}
	/// Control register 2
	enum mask_CR2 {
		LAST = 0x00001000,		/// DMA last transfer offset:12 width:1 
		DMAEN = 0x00000800,		/// DMA requests enable offset:11 width:1 
		ITBUFEN = 0x00000400,		/// Buffer interrupt enable offset:10 width:1 
		ITEVTEN = 0x00000200,		/// Event interrupt enable offset:9 width:1 
		ITERREN = 0x00000100,		/// Error interrupt enable offset:8 width:1 
		FREQ = 0x0000003F,		/// Peripheral clock frequency offset:0 width:6 
	}
	/// Own address register 1
	enum mask_OAR1 {
		ADDMODE = 0x00008000,		/// Addressing mode (slave mode) offset:15 width:1 
		ADD10 = 0x00000300,		/// Interface address offset:8 width:2 
		ADD7 = 0x000000FE,		/// Interface address offset:1 width:7 
		ADD0 = 0x00000001,		/// Interface address offset:0 width:1 
	}
	/// Own address register 2
	enum mask_OAR2 {
		ADD2 = 0x000000FE,		/// Interface address offset:1 width:7 
		ENDUAL = 0x00000001,		/// Dual addressing mode enable offset:0 width:1 
	}
	/// Data register
	enum mask_DR {
		DR = 0x000000FF,		/// 8-bit data register offset:0 width:8 
	}
	/// Status register 1
	enum mask_SR1 {
		SMBALERT = 0x00008000,		/// SMBus alert offset:15 width:1 
		TIMEOUT = 0x00004000,		/// Timeout or Tlow error offset:14 width:1 
		PECERR = 0x00001000,		/// PEC Error in reception offset:12 width:1 
		OVR = 0x00000800,		/// Overrun/Underrun offset:11 width:1 
		AF = 0x00000400,		/// Acknowledge failure offset:10 width:1 
		ARLO = 0x00000200,		/// Arbitration lost (master mode) offset:9 width:1 
		BERR = 0x00000100,		/// Bus error offset:8 width:1 
		TxE = 0x00000080,		/// Data register empty (transmitters) offset:7 width:1 
		RxNE = 0x00000040,		/// Data register not empty (receivers) offset:6 width:1 
		STOPF = 0x00000010,		/// Stop detection (slave mode) offset:4 width:1 
		ADD10 = 0x00000008,		/// 10-bit header sent (Master mode) offset:3 width:1 
		BTF = 0x00000004,		/// Byte transfer finished offset:2 width:1 
		ADDR = 0x00000002,		/// Address sent (master mode)/matched (slave mode) offset:1 width:1 
		SB = 0x00000001,		/// Start bit (Master mode) offset:0 width:1 
	}
	/// Status register 2
	enum mask_SR2 {
		PEC = 0x0000FF00,		/// acket error checking register offset:8 width:8 
		DUALF = 0x00000080,		/// Dual flag (Slave mode) offset:7 width:1 
		SMBHOST = 0x00000040,		/// SMBus host header (Slave mode) offset:6 width:1 
		SMBDEFAULT = 0x00000020,		/// SMBus device default address (Slave mode) offset:5 width:1 
		GENCALL = 0x00000010,		/// General call address (Slave mode) offset:4 width:1 
		TRA = 0x00000004,		/// Transmitter/receiver offset:2 width:1 
		BUSY = 0x00000002,		/// Bus busy offset:1 width:1 
		MSL = 0x00000001,		/// Master/slave offset:0 width:1 
	}
	/// Clock control register
	enum mask_CCR {
		F_S = 0x00008000,		/// I2C master mode selection offset:15 width:1 
		DUTY = 0x00004000,		/// Fast mode duty cycle offset:14 width:1 
		CCR = 0x00000FFF,		/// Clock control register in Fast/Standard mode (Master mode) offset:0 width:12 
	}
	/// TRISE register
	enum mask_TRISE {
		TRISE = 0x0000003F,		/// Maximum rise time in Fast/Standard mode (Master mode) offset:0 width:6 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// Control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// Control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_OAR1,uint) OAR1;		/// Own address register 1 offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_OAR2,uint) OAR2;		/// Own address register 2 offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_SR1,uint) SR1;		/// Status register 1 offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_SR2,uint) SR2;		/// Status register 2 offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_CCR,uint) CCR;		/// Clock control register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_TRISE,uint) TRISE;		/// TRISE register offset:0x00000020
}
/// Independent watchdog address:0x40003000
struct IWDG_Type{
	@disable this();
	/// Key register
	enum mask_KR {
		KEY = 0x0000FFFF,		/// Key value offset:0 width:16 
	}
	/// Prescaler register
	enum mask_PR {
		PR = 0x00000007,		/// Prescaler divider offset:0 width:3 
	}
	/// Reload register
	enum mask_RLR {
		RL = 0x00000FFF,		/// Watchdog counter reload value offset:0 width:12 
	}
	/// Status register
	enum mask_SR {
		RVU = 0x00000002,		/// Watchdog counter reload value update offset:1 width:1 
		PVU = 0x00000001,		/// Watchdog prescaler value update offset:0 width:1 
	}
	@Register("write-only",0x00)	WO_Reg!(mask_KR,uint) KR;		/// Key register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_PR,uint) PR;		/// Prescaler register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_RLR,uint) RLR;		/// Reload register offset:0x00000008
	@Register("read-only",0x0C)	RO_Reg!(mask_SR,uint) SR;		/// Status register offset:0x0000000C
}
/// Window watchdog address:0x40002C00
struct WWDG_Type{
	@disable this();
	enum Interrupt {
		WWDG = 0,		/// Window Watchdog interrupt
	};
	/// Control register
	enum mask_CR {
		WDGA = 0x00000080,		/// Activation bit offset:7 width:1 
		T = 0x0000007F,		/// 7-bit counter (MSB to LSB) offset:0 width:7 
	}
	/// Configuration register
	enum mask_CFR {
		EWI = 0x00000200,		/// Early wakeup interrupt offset:9 width:1 
		WDGTB1 = 0x00000100,		/// Timer base offset:8 width:1 
		WDGTB0 = 0x00000080,		/// Timer base offset:7 width:1 
		W = 0x0000007F,		/// 7-bit window value offset:0 width:7 
	}
	/// Status register
	enum mask_SR {
		EWIF = 0x00000001,		/// Early wakeup interrupt flag offset:0 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR,uint) CR;		/// Control register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CFR,uint) CFR;		/// Configuration register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SR,uint) SR;		/// Status register offset:0x00000008
}
/// Real-time clock address:0x40002800
struct RTC_Type{
	@disable this();
	enum Interrupt {
		RTC_Alarm = 41,		/// RTC Alarms (A and B) through EXTI line interrupt
		RTC_WKUP = 3,		/// RTC Wakeup interrupt through the EXTI line
	};
	/// time register
	enum mask_TR {
		PM = 0x00400000,		/// AM/PM notation offset:22 width:1 
		HT = 0x00300000,		/// Hour tens in BCD format offset:20 width:2 
		HU = 0x000F0000,		/// Hour units in BCD format offset:16 width:4 
		MNT = 0x00007000,		/// Minute tens in BCD format offset:12 width:3 
		MNU = 0x00000F00,		/// Minute units in BCD format offset:8 width:4 
		ST = 0x00000070,		/// Second tens in BCD format offset:4 width:3 
		SU = 0x0000000F,		/// Second units in BCD format offset:0 width:4 
	}
	/// date register
	enum mask_DR {
		YT = 0x00F00000,		/// Year tens in BCD format offset:20 width:4 
		YU = 0x000F0000,		/// Year units in BCD format offset:16 width:4 
		WDU = 0x0000E000,		/// Week day units offset:13 width:3 
		MT = 0x00001000,		/// Month tens in BCD format offset:12 width:1 
		MU = 0x00000F00,		/// Month units in BCD format offset:8 width:4 
		DT = 0x00000030,		/// Date tens in BCD format offset:4 width:2 
		DU = 0x0000000F,		/// Date units in BCD format offset:0 width:4 
	}
	/// control register
	enum mask_CR {
		COE = 0x00800000,		/// Calibration output enable offset:23 width:1 
		OSEL = 0x00600000,		/// Output selection offset:21 width:2 
		POL = 0x00100000,		/// Output polarity offset:20 width:1 
		COSEL = 0x00080000,		/// Calibration Output selection offset:19 width:1 
		BKP = 0x00040000,		/// Backup offset:18 width:1 
		SUB1H = 0x00020000,		/// Subtract 1 hour (winter time change) offset:17 width:1 
		ADD1H = 0x00010000,		/// Add 1 hour (summer time change) offset:16 width:1 
		TSIE = 0x00008000,		/// Time-stamp interrupt enable offset:15 width:1 
		WUTIE = 0x00004000,		/// Wakeup timer interrupt enable offset:14 width:1 
		ALRBIE = 0x00002000,		/// Alarm B interrupt enable offset:13 width:1 
		ALRAIE = 0x00001000,		/// Alarm A interrupt enable offset:12 width:1 
		TSE = 0x00000800,		/// Time stamp enable offset:11 width:1 
		WUTE = 0x00000400,		/// Wakeup timer enable offset:10 width:1 
		ALRBE = 0x00000200,		/// Alarm B enable offset:9 width:1 
		ALRAE = 0x00000100,		/// Alarm A enable offset:8 width:1 
		DCE = 0x00000080,		/// Coarse digital calibration enable offset:7 width:1 
		FMT = 0x00000040,		/// Hour format offset:6 width:1 
		BYPSHAD = 0x00000020,		/// Bypass the shadow registers offset:5 width:1 
		REFCKON = 0x00000010,		/// Reference clock detection enable (50 or 60 Hz) offset:4 width:1 
		TSEDGE = 0x00000008,		/// Time-stamp event active edge offset:3 width:1 
		WCKSEL = 0x00000007,		/// Wakeup clock selection offset:0 width:3 
	}
	/// initialization and status register
	enum mask_ISR {
		ALRAWF = 0x00000001,		/// Alarm A write flag offset:0 width:1 
		ALRBWF = 0x00000002,		/// Alarm B write flag offset:1 width:1 
		WUTWF = 0x00000004,		/// Wakeup timer write flag offset:2 width:1 
		SHPF = 0x00000008,		/// Shift operation pending offset:3 width:1 
		INITS = 0x00000010,		/// Initialization status flag offset:4 width:1 
		RSF = 0x00000020,		/// Registers synchronization flag offset:5 width:1 
		INITF = 0x00000040,		/// Initialization flag offset:6 width:1 
		INIT = 0x00000080,		/// Initialization mode offset:7 width:1 
		ALRAF = 0x00000100,		/// Alarm A flag offset:8 width:1 
		ALRBF = 0x00000200,		/// Alarm B flag offset:9 width:1 
		WUTF = 0x00000400,		/// Wakeup timer flag offset:10 width:1 
		TSF = 0x00000800,		/// Time-stamp flag offset:11 width:1 
		TSOVF = 0x00001000,		/// Time-stamp overflow flag offset:12 width:1 
		TAMP1F = 0x00002000,		/// Tamper detection flag offset:13 width:1 
		TAMP2F = 0x00004000,		/// TAMPER2 detection flag offset:14 width:1 
		RECALPF = 0x00010000,		/// Recalibration pending Flag offset:16 width:1 
	}
	/// prescaler register
	enum mask_PRER {
		PREDIV_A = 0x007F0000,		/// Asynchronous prescaler factor offset:16 width:7 
		PREDIV_S = 0x00007FFF,		/// Synchronous prescaler factor offset:0 width:15 
	}
	/// wakeup timer register
	enum mask_WUTR {
		WUT = 0x0000FFFF,		/// Wakeup auto-reload value bits offset:0 width:16 
	}
	/// calibration register
	enum mask_CALIBR {
		DCS = 0x00000080,		/// Digital calibration sign offset:7 width:1 
		DC = 0x0000001F,		/// Digital calibration offset:0 width:5 
	}
	/// alarm A register
	enum mask_ALRMAR {
		MSK4 = 0x80000000,		/// Alarm A date mask offset:31 width:1 
		WDSEL = 0x40000000,		/// Week day selection offset:30 width:1 
		DT = 0x30000000,		/// Date tens in BCD format offset:28 width:2 
		DU = 0x0F000000,		/// Date units or day in BCD format offset:24 width:4 
		MSK3 = 0x00800000,		/// Alarm A hours mask offset:23 width:1 
		PM = 0x00400000,		/// AM/PM notation offset:22 width:1 
		HT = 0x00300000,		/// Hour tens in BCD format offset:20 width:2 
		HU = 0x000F0000,		/// Hour units in BCD format offset:16 width:4 
		MSK2 = 0x00008000,		/// Alarm A minutes mask offset:15 width:1 
		MNT = 0x00007000,		/// Minute tens in BCD format offset:12 width:3 
		MNU = 0x00000F00,		/// Minute units in BCD format offset:8 width:4 
		MSK1 = 0x00000080,		/// Alarm A seconds mask offset:7 width:1 
		ST = 0x00000070,		/// Second tens in BCD format offset:4 width:3 
		SU = 0x0000000F,		/// Second units in BCD format offset:0 width:4 
	}
	/// alarm B register
	enum mask_ALRMBR {
		MSK4 = 0x80000000,		/// Alarm B date mask offset:31 width:1 
		WDSEL = 0x40000000,		/// Week day selection offset:30 width:1 
		DT = 0x30000000,		/// Date tens in BCD format offset:28 width:2 
		DU = 0x0F000000,		/// Date units or day in BCD format offset:24 width:4 
		MSK3 = 0x00800000,		/// Alarm B hours mask offset:23 width:1 
		PM = 0x00400000,		/// AM/PM notation offset:22 width:1 
		HT = 0x00300000,		/// Hour tens in BCD format offset:20 width:2 
		HU = 0x000F0000,		/// Hour units in BCD format offset:16 width:4 
		MSK2 = 0x00008000,		/// Alarm B minutes mask offset:15 width:1 
		MNT = 0x00007000,		/// Minute tens in BCD format offset:12 width:3 
		MNU = 0x00000F00,		/// Minute units in BCD format offset:8 width:4 
		MSK1 = 0x00000080,		/// Alarm B seconds mask offset:7 width:1 
		ST = 0x00000070,		/// Second tens in BCD format offset:4 width:3 
		SU = 0x0000000F,		/// Second units in BCD format offset:0 width:4 
	}
	/// write protection register
	enum mask_WPR {
		KEY = 0x000000FF,		/// Write protection key offset:0 width:8 
	}
	/// sub second register
	enum mask_SSR {
		SS = 0x0000FFFF,		/// Sub second value offset:0 width:16 
	}
	/// shift control register
	enum mask_SHIFTR {
		ADD1S = 0x80000000,		/// Add one second offset:31 width:1 
		SUBFS = 0x00007FFF,		/// Subtract a fraction of a second offset:0 width:15 
	}
	/// time stamp time register
	enum mask_TSTR {
		PM = 0x00400000,		/// AM/PM notation offset:22 width:1 
		HT = 0x00300000,		/// Hour tens in BCD format offset:20 width:2 
		HU = 0x000F0000,		/// Hour units in BCD format offset:16 width:4 
		MNT = 0x00007000,		/// Minute tens in BCD format offset:12 width:3 
		MNU = 0x00000F00,		/// Minute units in BCD format offset:8 width:4 
		ST = 0x00000070,		/// Second tens in BCD format offset:4 width:3 
		SU = 0x0000000F,		/// Second units in BCD format offset:0 width:4 
	}
	/// time stamp date register
	enum mask_TSDR {
		WDU = 0x0000E000,		/// Week day units offset:13 width:3 
		MT = 0x00001000,		/// Month tens in BCD format offset:12 width:1 
		MU = 0x00000F00,		/// Month units in BCD format offset:8 width:4 
		DT = 0x00000030,		/// Date tens in BCD format offset:4 width:2 
		DU = 0x0000000F,		/// Date units in BCD format offset:0 width:4 
	}
	/// timestamp sub second register
	enum mask_TSSSR {
		SS = 0x0000FFFF,		/// Sub second value offset:0 width:16 
	}
	/// calibration register
	enum mask_CALR {
		CALP = 0x00008000,		/// Increase frequency of RTC by 488.5 ppm offset:15 width:1 
		CALW8 = 0x00004000,		/// Use an 8-second calibration cycle period offset:14 width:1 
		CALW16 = 0x00002000,		/// Use a 16-second calibration cycle period offset:13 width:1 
		CALM = 0x000001FF,		/// Calibration minus offset:0 width:9 
	}
	/// tamper and alternate function configuration register
	enum mask_TAFCR {
		ALARMOUTTYPE = 0x00040000,		/// AFO_ALARM output type offset:18 width:1 
		TSINSEL = 0x00020000,		/// TIMESTAMP mapping offset:17 width:1 
		TAMP1INSEL = 0x00010000,		/// TAMPER1 mapping offset:16 width:1 
		TAMPPUDIS = 0x00008000,		/// TAMPER pull-up disable offset:15 width:1 
		TAMPPRCH = 0x00006000,		/// Tamper precharge duration offset:13 width:2 
		TAMPFLT = 0x00001800,		/// Tamper filter count offset:11 width:2 
		TAMPFREQ = 0x00000700,		/// Tamper sampling frequency offset:8 width:3 
		TAMPTS = 0x00000080,		/// Activate timestamp on tamper detection event offset:7 width:1 
		TAMP2TRG = 0x00000010,		/// Active level for tamper 2 offset:4 width:1 
		TAMP2E = 0x00000008,		/// Tamper 2 detection enable offset:3 width:1 
		TAMPIE = 0x00000004,		/// Tamper interrupt enable offset:2 width:1 
		TAMP1TRG = 0x00000002,		/// Active level for tamper 1 offset:1 width:1 
		TAMP1E = 0x00000001,		/// Tamper 1 detection enable offset:0 width:1 
	}
	/// alarm A sub second register
	enum mask_ALRMASSR {
		MASKSS = 0x0F000000,		/// Mask the most-significant bits starting at this bit offset:24 width:4 
		SS = 0x00007FFF,		/// Sub seconds value offset:0 width:15 
	}
	/// alarm B sub second register
	enum mask_ALRMBSSR {
		MASKSS = 0x0F000000,		/// Mask the most-significant bits starting at this bit offset:24 width:4 
		SS = 0x00007FFF,		/// Sub seconds value offset:0 width:15 
	}
	/// backup register
	enum mask_BKP0R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP1R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP2R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP3R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP4R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP5R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP6R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP7R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP8R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP9R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP10R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP11R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP12R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP13R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP14R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP15R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP16R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP17R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP18R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	/// backup register
	enum mask_BKP19R {
		BKP = 0xFFFFFFFF,		/// BKP offset:0 width:32 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_TR,uint) TR;		/// time register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_DR,uint) DR;		/// date register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_CR,uint) CR;		/// control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_ISR,uint) ISR;		/// initialization and status register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_PRER,uint) PRER;		/// prescaler register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_WUTR,uint) WUTR;		/// wakeup timer register offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_CALIBR,uint) CALIBR;		/// calibration register offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_ALRMAR,uint) ALRMAR;		/// alarm A register offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_ALRMBR,uint) ALRMBR;		/// alarm B register offset:0x00000020
	@Register("write-only",0x24)	WO_Reg!(mask_WPR,uint) WPR;		/// write protection register offset:0x00000024
	@Register("read-only",0x28)	RO_Reg!(mask_SSR,uint) SSR;		/// sub second register offset:0x00000028
	@Register("write-only",0x2C)	WO_Reg!(mask_SHIFTR,uint) SHIFTR;		/// shift control register offset:0x0000002C
	@Register("read-only",0x30)	RO_Reg!(mask_TSTR,uint) TSTR;		/// time stamp time register offset:0x00000030
	@Register("read-only",0x34)	RO_Reg!(mask_TSDR,uint) TSDR;		/// time stamp date register offset:0x00000034
	@Register("read-only",0x38)	RO_Reg!(mask_TSSSR,uint) TSSSR;		/// timestamp sub second register offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CALR,uint) CALR;		/// calibration register offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_TAFCR,uint) TAFCR;		/// tamper and alternate function configuration register offset:0x00000040
	@Register("read-write",0x44)	RW_Reg!(mask_ALRMASSR,uint) ALRMASSR;		/// alarm A sub second register offset:0x00000044
	@Register("read-write",0x48)	RW_Reg!(mask_ALRMBSSR,uint) ALRMBSSR;		/// alarm B sub second register offset:0x00000048
	uint[0x01]		Reserved0;
	@Register("read-write",0x50)	RW_Reg!(mask_BKP0R,uint) BKP0R;		/// backup register offset:0x00000050
	@Register("read-write",0x54)	RW_Reg!(mask_BKP1R,uint) BKP1R;		/// backup register offset:0x00000054
	@Register("read-write",0x58)	RW_Reg!(mask_BKP2R,uint) BKP2R;		/// backup register offset:0x00000058
	@Register("read-write",0x5C)	RW_Reg!(mask_BKP3R,uint) BKP3R;		/// backup register offset:0x0000005C
	@Register("read-write",0x60)	RW_Reg!(mask_BKP4R,uint) BKP4R;		/// backup register offset:0x00000060
	@Register("read-write",0x64)	RW_Reg!(mask_BKP5R,uint) BKP5R;		/// backup register offset:0x00000064
	@Register("read-write",0x68)	RW_Reg!(mask_BKP6R,uint) BKP6R;		/// backup register offset:0x00000068
	@Register("read-write",0x6C)	RW_Reg!(mask_BKP7R,uint) BKP7R;		/// backup register offset:0x0000006C
	@Register("read-write",0x70)	RW_Reg!(mask_BKP8R,uint) BKP8R;		/// backup register offset:0x00000070
	@Register("read-write",0x74)	RW_Reg!(mask_BKP9R,uint) BKP9R;		/// backup register offset:0x00000074
	@Register("read-write",0x78)	RW_Reg!(mask_BKP10R,uint) BKP10R;		/// backup register offset:0x00000078
	@Register("read-write",0x7C)	RW_Reg!(mask_BKP11R,uint) BKP11R;		/// backup register offset:0x0000007C
	@Register("read-write",0x80)	RW_Reg!(mask_BKP12R,uint) BKP12R;		/// backup register offset:0x00000080
	@Register("read-write",0x84)	RW_Reg!(mask_BKP13R,uint) BKP13R;		/// backup register offset:0x00000084
	@Register("read-write",0x88)	RW_Reg!(mask_BKP14R,uint) BKP14R;		/// backup register offset:0x00000088
	@Register("read-write",0x8C)	RW_Reg!(mask_BKP15R,uint) BKP15R;		/// backup register offset:0x0000008C
	@Register("read-write",0x90)	RW_Reg!(mask_BKP16R,uint) BKP16R;		/// backup register offset:0x00000090
	@Register("read-write",0x94)	RW_Reg!(mask_BKP17R,uint) BKP17R;		/// backup register offset:0x00000094
	@Register("read-write",0x98)	RW_Reg!(mask_BKP18R,uint) BKP18R;		/// backup register offset:0x00000098
	@Register("read-write",0x9C)	RW_Reg!(mask_BKP19R,uint) BKP19R;		/// backup register offset:0x0000009C
}
/// Advanced-timers address:0x40010000
struct TIM1_Type{
	@disable this();
	enum Interrupt {
		TIM1_CC = 27,		/// TIM1 Capture Compare interrupt
		TIM1_TRG_COM_TIM11 = 26,		/// TIM1 Trigger and Commutation interrupts and TIM11 global interrupt
		TIM1_UP_TIM10 = 25,		/// TIM1 Update interrupt and TIM10 global interrupt
		TIM1_BRK_TIM9 = 24,		/// TIM1 Break interrupt and TIM9 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		CMS = 0x00000060,		/// Center-aligned mode selection offset:5 width:2 
		DIR = 0x00000010,		/// Direction offset:4 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		OIS4 = 0x00004000,		/// Output Idle state 4 offset:14 width:1 
		OIS3N = 0x00002000,		/// Output Idle state 3 offset:13 width:1 
		OIS3 = 0x00001000,		/// Output Idle state 3 offset:12 width:1 
		OIS2N = 0x00000800,		/// Output Idle state 2 offset:11 width:1 
		OIS2 = 0x00000400,		/// Output Idle state 2 offset:10 width:1 
		OIS1N = 0x00000200,		/// Output Idle state 1 offset:9 width:1 
		OIS1 = 0x00000100,		/// Output Idle state 1 offset:8 width:1 
		TI1S = 0x00000080,		/// TI1 selection offset:7 width:1 
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
		CCDS = 0x00000008,		/// Capture/compare DMA selection offset:3 width:1 
		CCUS = 0x00000004,		/// Capture/compare control update selection offset:2 width:1 
		CCPC = 0x00000001,		/// Capture/compare preloaded control offset:0 width:1 
	}
	/// slave mode control register
	enum mask_SMCR {
		ETP = 0x00008000,		/// External trigger polarity offset:15 width:1 
		ECE = 0x00004000,		/// External clock enable offset:14 width:1 
		ETPS = 0x00003000,		/// External trigger prescaler offset:12 width:2 
		ETF = 0x00000F00,		/// External trigger filter offset:8 width:4 
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TDE = 0x00004000,		/// Trigger DMA request enable offset:14 width:1 
		COMDE = 0x00002000,		/// COM DMA request enable offset:13 width:1 
		CC4DE = 0x00001000,		/// Capture/Compare 4 DMA request enable offset:12 width:1 
		CC3DE = 0x00000800,		/// Capture/Compare 3 DMA request enable offset:11 width:1 
		CC2DE = 0x00000400,		/// Capture/Compare 2 DMA request enable offset:10 width:1 
		CC1DE = 0x00000200,		/// Capture/Compare 1 DMA request enable offset:9 width:1 
		UDE = 0x00000100,		/// Update DMA request enable offset:8 width:1 
		BIE = 0x00000080,		/// Break interrupt enable offset:7 width:1 
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		COMIE = 0x00000020,		/// COM interrupt enable offset:5 width:1 
		CC4IE = 0x00000010,		/// Capture/Compare 4 interrupt enable offset:4 width:1 
		CC3IE = 0x00000008,		/// Capture/Compare 3 interrupt enable offset:3 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC4OF = 0x00001000,		/// Capture/Compare 4 overcapture flag offset:12 width:1 
		CC3OF = 0x00000800,		/// Capture/Compare 3 overcapture flag offset:11 width:1 
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		BIF = 0x00000080,		/// Break interrupt flag offset:7 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		COMIF = 0x00000020,		/// COM interrupt flag offset:5 width:1 
		CC4IF = 0x00000010,		/// Capture/Compare 4 interrupt flag offset:4 width:1 
		CC3IF = 0x00000008,		/// Capture/Compare 3 interrupt flag offset:3 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		BG = 0x00000080,		/// Break generation offset:7 width:1 
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		COMG = 0x00000020,		/// Capture/Compare control update generation offset:5 width:1 
		CC4G = 0x00000010,		/// Capture/compare 4 generation offset:4 width:1 
		CC3G = 0x00000008,		/// Capture/compare 3 generation offset:3 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2CE = 0x00008000,		/// Output Compare 2 clear enable offset:15 width:1 
		OC2M = 0x00007000,		/// Output Compare 2 mode offset:12 width:3 
		OC2PE = 0x00000800,		/// Output Compare 2 preload enable offset:11 width:1 
		OC2FE = 0x00000400,		/// Output Compare 2 fast enable offset:10 width:1 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		OC1CE = 0x00000080,		/// Output Compare 1 clear enable offset:7 width:1 
		OC1M = 0x00000070,		/// Output Compare 1 mode offset:4 width:3 
		OC1PE = 0x00000008,		/// Output Compare 1 preload enable offset:3 width:1 
		OC1FE = 0x00000004,		/// Output Compare 1 fast enable offset:2 width:1 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x0000F000,		/// Input capture 2 filter offset:12 width:4 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (output mode)
	enum mask_CCMR2_Output {
		OC4CE = 0x00008000,		/// Output compare 4 clear enable offset:15 width:1 
		OC4M = 0x00007000,		/// Output compare 4 mode offset:12 width:3 
		OC4PE = 0x00000800,		/// Output compare 4 preload enable offset:11 width:1 
		OC4FE = 0x00000400,		/// Output compare 4 fast enable offset:10 width:1 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		OC3CE = 0x00000080,		/// Output compare 3 clear enable offset:7 width:1 
		OC3M = 0x00000070,		/// Output compare 3 mode offset:4 width:3 
		OC3PE = 0x00000008,		/// Output compare 3 preload enable offset:3 width:1 
		OC3FE = 0x00000004,		/// Output compare 3 fast enable offset:2 width:1 
		CC3S = 0x00000003,		/// Capture/Compare 3 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (input mode)
	enum mask_CCMR2_Input {
		IC4F = 0x0000F000,		/// Input capture 4 filter offset:12 width:4 
		IC4PSC = 0x00000C00,		/// Input capture 4 prescaler offset:10 width:2 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		IC3F = 0x000000F0,		/// Input capture 3 filter offset:4 width:4 
		IC3PSC = 0x0000000C,		/// Input capture 3 prescaler offset:2 width:2 
		CC3S = 0x00000003,		/// Capture/compare 3 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC4P = 0x00002000,		/// Capture/Compare 3 output Polarity offset:13 width:1 
		CC4E = 0x00001000,		/// Capture/Compare 4 output enable offset:12 width:1 
		CC3NP = 0x00000800,		/// Capture/Compare 3 output Polarity offset:11 width:1 
		CC3NE = 0x00000400,		/// Capture/Compare 3 complementary output enable offset:10 width:1 
		CC3P = 0x00000200,		/// Capture/Compare 3 output Polarity offset:9 width:1 
		CC3E = 0x00000100,		/// Capture/Compare 3 output enable offset:8 width:1 
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2NE = 0x00000040,		/// Capture/Compare 2 complementary output enable offset:6 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1NE = 0x00000004,		/// Capture/Compare 1 complementary output enable offset:2 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT = 0x0000FFFF,		/// counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR = 0x0000FFFF,		/// Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1 = 0x0000FFFF,		/// Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2 = 0x0000FFFF,		/// Capture/Compare 2 value offset:0 width:16 
	}
	/// capture/compare register 3
	enum mask_CCR3 {
		CCR3 = 0x0000FFFF,		/// Capture/Compare value offset:0 width:16 
	}
	/// capture/compare register 4
	enum mask_CCR4 {
		CCR4 = 0x0000FFFF,		/// Capture/Compare value offset:0 width:16 
	}
	/// DMA control register
	enum mask_DCR {
		DBL = 0x00001F00,		/// DMA burst length offset:8 width:5 
		DBA = 0x0000001F,		/// DMA base address offset:0 width:5 
	}
	/// DMA address for full transfer
	enum mask_DMAR {
		DMAB = 0x0000FFFF,		/// DMA register for burst accesses offset:0 width:16 
	}
	/// repetition counter register
	enum mask_RCR {
		REP = 0x000000FF,		/// Repetition counter value offset:0 width:8 
	}
	/// break and dead-time register
	enum mask_BDTR {
		MOE = 0x00008000,		/// Main output enable offset:15 width:1 
		AOE = 0x00004000,		/// Automatic output enable offset:14 width:1 
		BKP = 0x00002000,		/// Break polarity offset:13 width:1 
		BKE = 0x00001000,		/// Break enable offset:12 width:1 
		OSSR = 0x00000800,		/// Off-state selection for Run mode offset:11 width:1 
		OSSI = 0x00000400,		/// Off-state selection for Idle mode offset:10 width:1 
		LOCK = 0x00000300,		/// Lock configuration offset:8 width:2 
		DTG = 0x000000FF,		/// Dead-time generator setup offset:0 width:8 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	union {
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Output,uint) CCMR2_Output;		/// capture/compare mode register 2 (output mode) offset:0x0000001C
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Input,uint) CCMR2_Input;		/// capture/compare mode register 2 (input mode) offset:0x0000001C
	}
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	@Register("read-write",0x30)	RW_Reg!(mask_RCR,uint) RCR;		/// repetition counter register offset:0x00000030
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CCR3,uint) CCR3;		/// capture/compare register 3 offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_CCR4,uint) CCR4;		/// capture/compare register 4 offset:0x00000040
	@Register("read-write",0x44)	RW_Reg!(mask_BDTR,uint) BDTR;		/// break and dead-time register offset:0x00000044
	@Register("read-write",0x48)	RW_Reg!(mask_DCR,uint) DCR;		/// DMA control register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_DMAR,uint) DMAR;		/// DMA address for full transfer offset:0x0000004C
}
/// General purpose timers address:0x40000000
struct TIM2_Type{
	@disable this();
	enum Interrupt {
		TIM2 = 28,		/// TIM2 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		CMS = 0x00000060,		/// Center-aligned mode selection offset:5 width:2 
		DIR = 0x00000010,		/// Direction offset:4 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TI1S = 0x00000080,		/// TI1 selection offset:7 width:1 
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
		CCDS = 0x00000008,		/// Capture/compare DMA selection offset:3 width:1 
	}
	/// slave mode control register
	enum mask_SMCR {
		ETP = 0x00008000,		/// External trigger polarity offset:15 width:1 
		ECE = 0x00004000,		/// External clock enable offset:14 width:1 
		ETPS = 0x00003000,		/// External trigger prescaler offset:12 width:2 
		ETF = 0x00000F00,		/// External trigger filter offset:8 width:4 
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TDE = 0x00004000,		/// Trigger DMA request enable offset:14 width:1 
		CC4DE = 0x00001000,		/// Capture/Compare 4 DMA request enable offset:12 width:1 
		CC3DE = 0x00000800,		/// Capture/Compare 3 DMA request enable offset:11 width:1 
		CC2DE = 0x00000400,		/// Capture/Compare 2 DMA request enable offset:10 width:1 
		CC1DE = 0x00000200,		/// Capture/Compare 1 DMA request enable offset:9 width:1 
		UDE = 0x00000100,		/// Update DMA request enable offset:8 width:1 
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		CC4IE = 0x00000010,		/// Capture/Compare 4 interrupt enable offset:4 width:1 
		CC3IE = 0x00000008,		/// Capture/Compare 3 interrupt enable offset:3 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC4OF = 0x00001000,		/// Capture/Compare 4 overcapture flag offset:12 width:1 
		CC3OF = 0x00000800,		/// Capture/Compare 3 overcapture flag offset:11 width:1 
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		CC4IF = 0x00000010,		/// Capture/Compare 4 interrupt flag offset:4 width:1 
		CC3IF = 0x00000008,		/// Capture/Compare 3 interrupt flag offset:3 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		CC4G = 0x00000010,		/// Capture/compare 4 generation offset:4 width:1 
		CC3G = 0x00000008,		/// Capture/compare 3 generation offset:3 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2CE = 0x00008000,		/// OC2CE offset:15 width:1 
		OC2M = 0x00007000,		/// OC2M offset:12 width:3 
		OC2PE = 0x00000800,		/// OC2PE offset:11 width:1 
		OC2FE = 0x00000400,		/// OC2FE offset:10 width:1 
		CC2S = 0x00000300,		/// CC2S offset:8 width:2 
		OC1CE = 0x00000080,		/// OC1CE offset:7 width:1 
		OC1M = 0x00000070,		/// OC1M offset:4 width:3 
		OC1PE = 0x00000008,		/// OC1PE offset:3 width:1 
		OC1FE = 0x00000004,		/// OC1FE offset:2 width:1 
		CC1S = 0x00000003,		/// CC1S offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x0000F000,		/// Input capture 2 filter offset:12 width:4 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (output mode)
	enum mask_CCMR2_Output {
		O24CE = 0x00008000,		/// O24CE offset:15 width:1 
		OC4M = 0x00007000,		/// OC4M offset:12 width:3 
		OC4PE = 0x00000800,		/// OC4PE offset:11 width:1 
		OC4FE = 0x00000400,		/// OC4FE offset:10 width:1 
		CC4S = 0x00000300,		/// CC4S offset:8 width:2 
		OC3CE = 0x00000080,		/// OC3CE offset:7 width:1 
		OC3M = 0x00000070,		/// OC3M offset:4 width:3 
		OC3PE = 0x00000008,		/// OC3PE offset:3 width:1 
		OC3FE = 0x00000004,		/// OC3FE offset:2 width:1 
		CC3S = 0x00000003,		/// CC3S offset:0 width:2 
	}
	/// capture/compare mode register 2 (input mode)
	enum mask_CCMR2_Input {
		IC4F = 0x0000F000,		/// Input capture 4 filter offset:12 width:4 
		IC4PSC = 0x00000C00,		/// Input capture 4 prescaler offset:10 width:2 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		IC3F = 0x000000F0,		/// Input capture 3 filter offset:4 width:4 
		IC3PSC = 0x0000000C,		/// Input capture 3 prescaler offset:2 width:2 
		CC3S = 0x00000003,		/// Capture/compare 3 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC4NP = 0x00008000,		/// Capture/Compare 4 output Polarity offset:15 width:1 
		CC4P = 0x00002000,		/// Capture/Compare 3 output Polarity offset:13 width:1 
		CC4E = 0x00001000,		/// Capture/Compare 4 output enable offset:12 width:1 
		CC3NP = 0x00000800,		/// Capture/Compare 3 output Polarity offset:11 width:1 
		CC3P = 0x00000200,		/// Capture/Compare 3 output Polarity offset:9 width:1 
		CC3E = 0x00000100,		/// Capture/Compare 3 output enable offset:8 width:1 
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT_H = 0xFFFF0000,		/// High counter value offset:16 width:16 
		CNT_L = 0x0000FFFF,		/// Low counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR_H = 0xFFFF0000,		/// High Auto-reload value offset:16 width:16 
		ARR_L = 0x0000FFFF,		/// Low Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1_H = 0xFFFF0000,		/// High Capture/Compare 1 value offset:16 width:16 
		CCR1_L = 0x0000FFFF,		/// Low Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2_H = 0xFFFF0000,		/// High Capture/Compare 2 value offset:16 width:16 
		CCR2_L = 0x0000FFFF,		/// Low Capture/Compare 2 value offset:0 width:16 
	}
	/// capture/compare register 3
	enum mask_CCR3 {
		CCR3_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR3_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// capture/compare register 4
	enum mask_CCR4 {
		CCR4_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR4_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// DMA control register
	enum mask_DCR {
		DBL = 0x00001F00,		/// DMA burst length offset:8 width:5 
		DBA = 0x0000001F,		/// DMA base address offset:0 width:5 
	}
	/// DMA address for full transfer
	enum mask_DMAR {
		DMAB = 0x0000FFFF,		/// DMA register for burst accesses offset:0 width:16 
	}
	/// TIM5 option register
	enum mask_OR {
		ITR1_RMP = 0x00000C00,		/// Timer Input 4 remap offset:10 width:2 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	union {
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Output,uint) CCMR2_Output;		/// capture/compare mode register 2 (output mode) offset:0x0000001C
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Input,uint) CCMR2_Input;		/// capture/compare mode register 2 (input mode) offset:0x0000001C
	}
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved0;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CCR3,uint) CCR3;		/// capture/compare register 3 offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_CCR4,uint) CCR4;		/// capture/compare register 4 offset:0x00000040
	uint[0x01]		Reserved1;
	@Register("read-write",0x48)	RW_Reg!(mask_DCR,uint) DCR;		/// DMA control register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_DMAR,uint) DMAR;		/// DMA address for full transfer offset:0x0000004C
	@Register("read-write",0x50)	RW_Reg!(mask_OR,uint) OR;		/// TIM5 option register offset:0x00000050
}
/// General purpose timers address:0x40000400
struct TIM3_Type{
	@disable this();
	enum Interrupt {
		TIM3 = 29,		/// TIM3 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		CMS = 0x00000060,		/// Center-aligned mode selection offset:5 width:2 
		DIR = 0x00000010,		/// Direction offset:4 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TI1S = 0x00000080,		/// TI1 selection offset:7 width:1 
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
		CCDS = 0x00000008,		/// Capture/compare DMA selection offset:3 width:1 
	}
	/// slave mode control register
	enum mask_SMCR {
		ETP = 0x00008000,		/// External trigger polarity offset:15 width:1 
		ECE = 0x00004000,		/// External clock enable offset:14 width:1 
		ETPS = 0x00003000,		/// External trigger prescaler offset:12 width:2 
		ETF = 0x00000F00,		/// External trigger filter offset:8 width:4 
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TDE = 0x00004000,		/// Trigger DMA request enable offset:14 width:1 
		CC4DE = 0x00001000,		/// Capture/Compare 4 DMA request enable offset:12 width:1 
		CC3DE = 0x00000800,		/// Capture/Compare 3 DMA request enable offset:11 width:1 
		CC2DE = 0x00000400,		/// Capture/Compare 2 DMA request enable offset:10 width:1 
		CC1DE = 0x00000200,		/// Capture/Compare 1 DMA request enable offset:9 width:1 
		UDE = 0x00000100,		/// Update DMA request enable offset:8 width:1 
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		CC4IE = 0x00000010,		/// Capture/Compare 4 interrupt enable offset:4 width:1 
		CC3IE = 0x00000008,		/// Capture/Compare 3 interrupt enable offset:3 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC4OF = 0x00001000,		/// Capture/Compare 4 overcapture flag offset:12 width:1 
		CC3OF = 0x00000800,		/// Capture/Compare 3 overcapture flag offset:11 width:1 
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		CC4IF = 0x00000010,		/// Capture/Compare 4 interrupt flag offset:4 width:1 
		CC3IF = 0x00000008,		/// Capture/Compare 3 interrupt flag offset:3 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		CC4G = 0x00000010,		/// Capture/compare 4 generation offset:4 width:1 
		CC3G = 0x00000008,		/// Capture/compare 3 generation offset:3 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2CE = 0x00008000,		/// OC2CE offset:15 width:1 
		OC2M = 0x00007000,		/// OC2M offset:12 width:3 
		OC2PE = 0x00000800,		/// OC2PE offset:11 width:1 
		OC2FE = 0x00000400,		/// OC2FE offset:10 width:1 
		CC2S = 0x00000300,		/// CC2S offset:8 width:2 
		OC1CE = 0x00000080,		/// OC1CE offset:7 width:1 
		OC1M = 0x00000070,		/// OC1M offset:4 width:3 
		OC1PE = 0x00000008,		/// OC1PE offset:3 width:1 
		OC1FE = 0x00000004,		/// OC1FE offset:2 width:1 
		CC1S = 0x00000003,		/// CC1S offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x0000F000,		/// Input capture 2 filter offset:12 width:4 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (output mode)
	enum mask_CCMR2_Output {
		O24CE = 0x00008000,		/// O24CE offset:15 width:1 
		OC4M = 0x00007000,		/// OC4M offset:12 width:3 
		OC4PE = 0x00000800,		/// OC4PE offset:11 width:1 
		OC4FE = 0x00000400,		/// OC4FE offset:10 width:1 
		CC4S = 0x00000300,		/// CC4S offset:8 width:2 
		OC3CE = 0x00000080,		/// OC3CE offset:7 width:1 
		OC3M = 0x00000070,		/// OC3M offset:4 width:3 
		OC3PE = 0x00000008,		/// OC3PE offset:3 width:1 
		OC3FE = 0x00000004,		/// OC3FE offset:2 width:1 
		CC3S = 0x00000003,		/// CC3S offset:0 width:2 
	}
	/// capture/compare mode register 2 (input mode)
	enum mask_CCMR2_Input {
		IC4F = 0x0000F000,		/// Input capture 4 filter offset:12 width:4 
		IC4PSC = 0x00000C00,		/// Input capture 4 prescaler offset:10 width:2 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		IC3F = 0x000000F0,		/// Input capture 3 filter offset:4 width:4 
		IC3PSC = 0x0000000C,		/// Input capture 3 prescaler offset:2 width:2 
		CC3S = 0x00000003,		/// Capture/compare 3 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC4NP = 0x00008000,		/// Capture/Compare 4 output Polarity offset:15 width:1 
		CC4P = 0x00002000,		/// Capture/Compare 3 output Polarity offset:13 width:1 
		CC4E = 0x00001000,		/// Capture/Compare 4 output enable offset:12 width:1 
		CC3NP = 0x00000800,		/// Capture/Compare 3 output Polarity offset:11 width:1 
		CC3P = 0x00000200,		/// Capture/Compare 3 output Polarity offset:9 width:1 
		CC3E = 0x00000100,		/// Capture/Compare 3 output enable offset:8 width:1 
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT_H = 0xFFFF0000,		/// High counter value offset:16 width:16 
		CNT_L = 0x0000FFFF,		/// Low counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR_H = 0xFFFF0000,		/// High Auto-reload value offset:16 width:16 
		ARR_L = 0x0000FFFF,		/// Low Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1_H = 0xFFFF0000,		/// High Capture/Compare 1 value offset:16 width:16 
		CCR1_L = 0x0000FFFF,		/// Low Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2_H = 0xFFFF0000,		/// High Capture/Compare 2 value offset:16 width:16 
		CCR2_L = 0x0000FFFF,		/// Low Capture/Compare 2 value offset:0 width:16 
	}
	/// capture/compare register 3
	enum mask_CCR3 {
		CCR3_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR3_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// capture/compare register 4
	enum mask_CCR4 {
		CCR4_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR4_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// DMA control register
	enum mask_DCR {
		DBL = 0x00001F00,		/// DMA burst length offset:8 width:5 
		DBA = 0x0000001F,		/// DMA base address offset:0 width:5 
	}
	/// DMA address for full transfer
	enum mask_DMAR {
		DMAB = 0x0000FFFF,		/// DMA register for burst accesses offset:0 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	union {
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Output,uint) CCMR2_Output;		/// capture/compare mode register 2 (output mode) offset:0x0000001C
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Input,uint) CCMR2_Input;		/// capture/compare mode register 2 (input mode) offset:0x0000001C
	}
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved0;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CCR3,uint) CCR3;		/// capture/compare register 3 offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_CCR4,uint) CCR4;		/// capture/compare register 4 offset:0x00000040
	uint[0x01]		Reserved1;
	@Register("read-write",0x48)	RW_Reg!(mask_DCR,uint) DCR;		/// DMA control register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_DMAR,uint) DMAR;		/// DMA address for full transfer offset:0x0000004C
}
/// Nullable.null address:0x40000800
struct TIM4_Type{
	@disable this();
	enum Interrupt {
		TIM4 = 30,		/// TIM4 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		CMS = 0x00000060,		/// Center-aligned mode selection offset:5 width:2 
		DIR = 0x00000010,		/// Direction offset:4 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TI1S = 0x00000080,		/// TI1 selection offset:7 width:1 
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
		CCDS = 0x00000008,		/// Capture/compare DMA selection offset:3 width:1 
	}
	/// slave mode control register
	enum mask_SMCR {
		ETP = 0x00008000,		/// External trigger polarity offset:15 width:1 
		ECE = 0x00004000,		/// External clock enable offset:14 width:1 
		ETPS = 0x00003000,		/// External trigger prescaler offset:12 width:2 
		ETF = 0x00000F00,		/// External trigger filter offset:8 width:4 
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TDE = 0x00004000,		/// Trigger DMA request enable offset:14 width:1 
		CC4DE = 0x00001000,		/// Capture/Compare 4 DMA request enable offset:12 width:1 
		CC3DE = 0x00000800,		/// Capture/Compare 3 DMA request enable offset:11 width:1 
		CC2DE = 0x00000400,		/// Capture/Compare 2 DMA request enable offset:10 width:1 
		CC1DE = 0x00000200,		/// Capture/Compare 1 DMA request enable offset:9 width:1 
		UDE = 0x00000100,		/// Update DMA request enable offset:8 width:1 
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		CC4IE = 0x00000010,		/// Capture/Compare 4 interrupt enable offset:4 width:1 
		CC3IE = 0x00000008,		/// Capture/Compare 3 interrupt enable offset:3 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC4OF = 0x00001000,		/// Capture/Compare 4 overcapture flag offset:12 width:1 
		CC3OF = 0x00000800,		/// Capture/Compare 3 overcapture flag offset:11 width:1 
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		CC4IF = 0x00000010,		/// Capture/Compare 4 interrupt flag offset:4 width:1 
		CC3IF = 0x00000008,		/// Capture/Compare 3 interrupt flag offset:3 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		CC4G = 0x00000010,		/// Capture/compare 4 generation offset:4 width:1 
		CC3G = 0x00000008,		/// Capture/compare 3 generation offset:3 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2CE = 0x00008000,		/// OC2CE offset:15 width:1 
		OC2M = 0x00007000,		/// OC2M offset:12 width:3 
		OC2PE = 0x00000800,		/// OC2PE offset:11 width:1 
		OC2FE = 0x00000400,		/// OC2FE offset:10 width:1 
		CC2S = 0x00000300,		/// CC2S offset:8 width:2 
		OC1CE = 0x00000080,		/// OC1CE offset:7 width:1 
		OC1M = 0x00000070,		/// OC1M offset:4 width:3 
		OC1PE = 0x00000008,		/// OC1PE offset:3 width:1 
		OC1FE = 0x00000004,		/// OC1FE offset:2 width:1 
		CC1S = 0x00000003,		/// CC1S offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x0000F000,		/// Input capture 2 filter offset:12 width:4 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (output mode)
	enum mask_CCMR2_Output {
		O24CE = 0x00008000,		/// O24CE offset:15 width:1 
		OC4M = 0x00007000,		/// OC4M offset:12 width:3 
		OC4PE = 0x00000800,		/// OC4PE offset:11 width:1 
		OC4FE = 0x00000400,		/// OC4FE offset:10 width:1 
		CC4S = 0x00000300,		/// CC4S offset:8 width:2 
		OC3CE = 0x00000080,		/// OC3CE offset:7 width:1 
		OC3M = 0x00000070,		/// OC3M offset:4 width:3 
		OC3PE = 0x00000008,		/// OC3PE offset:3 width:1 
		OC3FE = 0x00000004,		/// OC3FE offset:2 width:1 
		CC3S = 0x00000003,		/// CC3S offset:0 width:2 
	}
	/// capture/compare mode register 2 (input mode)
	enum mask_CCMR2_Input {
		IC4F = 0x0000F000,		/// Input capture 4 filter offset:12 width:4 
		IC4PSC = 0x00000C00,		/// Input capture 4 prescaler offset:10 width:2 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		IC3F = 0x000000F0,		/// Input capture 3 filter offset:4 width:4 
		IC3PSC = 0x0000000C,		/// Input capture 3 prescaler offset:2 width:2 
		CC3S = 0x00000003,		/// Capture/compare 3 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC4NP = 0x00008000,		/// Capture/Compare 4 output Polarity offset:15 width:1 
		CC4P = 0x00002000,		/// Capture/Compare 3 output Polarity offset:13 width:1 
		CC4E = 0x00001000,		/// Capture/Compare 4 output enable offset:12 width:1 
		CC3NP = 0x00000800,		/// Capture/Compare 3 output Polarity offset:11 width:1 
		CC3P = 0x00000200,		/// Capture/Compare 3 output Polarity offset:9 width:1 
		CC3E = 0x00000100,		/// Capture/Compare 3 output enable offset:8 width:1 
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT_H = 0xFFFF0000,		/// High counter value offset:16 width:16 
		CNT_L = 0x0000FFFF,		/// Low counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR_H = 0xFFFF0000,		/// High Auto-reload value offset:16 width:16 
		ARR_L = 0x0000FFFF,		/// Low Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1_H = 0xFFFF0000,		/// High Capture/Compare 1 value offset:16 width:16 
		CCR1_L = 0x0000FFFF,		/// Low Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2_H = 0xFFFF0000,		/// High Capture/Compare 2 value offset:16 width:16 
		CCR2_L = 0x0000FFFF,		/// Low Capture/Compare 2 value offset:0 width:16 
	}
	/// capture/compare register 3
	enum mask_CCR3 {
		CCR3_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR3_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// capture/compare register 4
	enum mask_CCR4 {
		CCR4_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR4_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// DMA control register
	enum mask_DCR {
		DBL = 0x00001F00,		/// DMA burst length offset:8 width:5 
		DBA = 0x0000001F,		/// DMA base address offset:0 width:5 
	}
	/// DMA address for full transfer
	enum mask_DMAR {
		DMAB = 0x0000FFFF,		/// DMA register for burst accesses offset:0 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	union {
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Output,uint) CCMR2_Output;		/// capture/compare mode register 2 (output mode) offset:0x0000001C
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Input,uint) CCMR2_Input;		/// capture/compare mode register 2 (input mode) offset:0x0000001C
	}
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved0;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CCR3,uint) CCR3;		/// capture/compare register 3 offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_CCR4,uint) CCR4;		/// capture/compare register 4 offset:0x00000040
	uint[0x01]		Reserved1;
	@Register("read-write",0x48)	RW_Reg!(mask_DCR,uint) DCR;		/// DMA control register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_DMAR,uint) DMAR;		/// DMA address for full transfer offset:0x0000004C
}
/// General-purpose-timers address:0x40000C00
struct TIM5_Type{
	@disable this();
	enum Interrupt {
		TIM5 = 50,		/// TIM5 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		CMS = 0x00000060,		/// Center-aligned mode selection offset:5 width:2 
		DIR = 0x00000010,		/// Direction offset:4 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		TI1S = 0x00000080,		/// TI1 selection offset:7 width:1 
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
		CCDS = 0x00000008,		/// Capture/compare DMA selection offset:3 width:1 
	}
	/// slave mode control register
	enum mask_SMCR {
		ETP = 0x00008000,		/// External trigger polarity offset:15 width:1 
		ECE = 0x00004000,		/// External clock enable offset:14 width:1 
		ETPS = 0x00003000,		/// External trigger prescaler offset:12 width:2 
		ETF = 0x00000F00,		/// External trigger filter offset:8 width:4 
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TDE = 0x00004000,		/// Trigger DMA request enable offset:14 width:1 
		CC4DE = 0x00001000,		/// Capture/Compare 4 DMA request enable offset:12 width:1 
		CC3DE = 0x00000800,		/// Capture/Compare 3 DMA request enable offset:11 width:1 
		CC2DE = 0x00000400,		/// Capture/Compare 2 DMA request enable offset:10 width:1 
		CC1DE = 0x00000200,		/// Capture/Compare 1 DMA request enable offset:9 width:1 
		UDE = 0x00000100,		/// Update DMA request enable offset:8 width:1 
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		CC4IE = 0x00000010,		/// Capture/Compare 4 interrupt enable offset:4 width:1 
		CC3IE = 0x00000008,		/// Capture/Compare 3 interrupt enable offset:3 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC4OF = 0x00001000,		/// Capture/Compare 4 overcapture flag offset:12 width:1 
		CC3OF = 0x00000800,		/// Capture/Compare 3 overcapture flag offset:11 width:1 
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		CC4IF = 0x00000010,		/// Capture/Compare 4 interrupt flag offset:4 width:1 
		CC3IF = 0x00000008,		/// Capture/Compare 3 interrupt flag offset:3 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		CC4G = 0x00000010,		/// Capture/compare 4 generation offset:4 width:1 
		CC3G = 0x00000008,		/// Capture/compare 3 generation offset:3 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2CE = 0x00008000,		/// OC2CE offset:15 width:1 
		OC2M = 0x00007000,		/// OC2M offset:12 width:3 
		OC2PE = 0x00000800,		/// OC2PE offset:11 width:1 
		OC2FE = 0x00000400,		/// OC2FE offset:10 width:1 
		CC2S = 0x00000300,		/// CC2S offset:8 width:2 
		OC1CE = 0x00000080,		/// OC1CE offset:7 width:1 
		OC1M = 0x00000070,		/// OC1M offset:4 width:3 
		OC1PE = 0x00000008,		/// OC1PE offset:3 width:1 
		OC1FE = 0x00000004,		/// OC1FE offset:2 width:1 
		CC1S = 0x00000003,		/// CC1S offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x0000F000,		/// Input capture 2 filter offset:12 width:4 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 2 (output mode)
	enum mask_CCMR2_Output {
		O24CE = 0x00008000,		/// O24CE offset:15 width:1 
		OC4M = 0x00007000,		/// OC4M offset:12 width:3 
		OC4PE = 0x00000800,		/// OC4PE offset:11 width:1 
		OC4FE = 0x00000400,		/// OC4FE offset:10 width:1 
		CC4S = 0x00000300,		/// CC4S offset:8 width:2 
		OC3CE = 0x00000080,		/// OC3CE offset:7 width:1 
		OC3M = 0x00000070,		/// OC3M offset:4 width:3 
		OC3PE = 0x00000008,		/// OC3PE offset:3 width:1 
		OC3FE = 0x00000004,		/// OC3FE offset:2 width:1 
		CC3S = 0x00000003,		/// CC3S offset:0 width:2 
	}
	/// capture/compare mode register 2 (input mode)
	enum mask_CCMR2_Input {
		IC4F = 0x0000F000,		/// Input capture 4 filter offset:12 width:4 
		IC4PSC = 0x00000C00,		/// Input capture 4 prescaler offset:10 width:2 
		CC4S = 0x00000300,		/// Capture/Compare 4 selection offset:8 width:2 
		IC3F = 0x000000F0,		/// Input capture 3 filter offset:4 width:4 
		IC3PSC = 0x0000000C,		/// Input capture 3 prescaler offset:2 width:2 
		CC3S = 0x00000003,		/// Capture/compare 3 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC4NP = 0x00008000,		/// Capture/Compare 4 output Polarity offset:15 width:1 
		CC4P = 0x00002000,		/// Capture/Compare 3 output Polarity offset:13 width:1 
		CC4E = 0x00001000,		/// Capture/Compare 4 output enable offset:12 width:1 
		CC3NP = 0x00000800,		/// Capture/Compare 3 output Polarity offset:11 width:1 
		CC3P = 0x00000200,		/// Capture/Compare 3 output Polarity offset:9 width:1 
		CC3E = 0x00000100,		/// Capture/Compare 3 output enable offset:8 width:1 
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT_H = 0xFFFF0000,		/// High counter value offset:16 width:16 
		CNT_L = 0x0000FFFF,		/// Low counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR_H = 0xFFFF0000,		/// High Auto-reload value offset:16 width:16 
		ARR_L = 0x0000FFFF,		/// Low Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1_H = 0xFFFF0000,		/// High Capture/Compare 1 value offset:16 width:16 
		CCR1_L = 0x0000FFFF,		/// Low Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2_H = 0xFFFF0000,		/// High Capture/Compare 2 value offset:16 width:16 
		CCR2_L = 0x0000FFFF,		/// Low Capture/Compare 2 value offset:0 width:16 
	}
	/// capture/compare register 3
	enum mask_CCR3 {
		CCR3_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR3_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// capture/compare register 4
	enum mask_CCR4 {
		CCR4_H = 0xFFFF0000,		/// High Capture/Compare value offset:16 width:16 
		CCR4_L = 0x0000FFFF,		/// Low Capture/Compare value offset:0 width:16 
	}
	/// DMA control register
	enum mask_DCR {
		DBL = 0x00001F00,		/// DMA burst length offset:8 width:5 
		DBA = 0x0000001F,		/// DMA base address offset:0 width:5 
	}
	/// DMA address for full transfer
	enum mask_DMAR {
		DMAB = 0x0000FFFF,		/// DMA register for burst accesses offset:0 width:16 
	}
	/// TIM5 option register
	enum mask_OR {
		IT4_RMP = 0x000000C0,		/// Timer Input 4 remap offset:6 width:2 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	union {
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Output,uint) CCMR2_Output;		/// capture/compare mode register 2 (output mode) offset:0x0000001C
		@Register("read-write",0x1C)	RW_Reg!(mask_CCMR2_Input,uint) CCMR2_Input;		/// capture/compare mode register 2 (input mode) offset:0x0000001C
	}
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved0;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_CCR3,uint) CCR3;		/// capture/compare register 3 offset:0x0000003C
	@Register("read-write",0x40)	RW_Reg!(mask_CCR4,uint) CCR4;		/// capture/compare register 4 offset:0x00000040
	uint[0x01]		Reserved1;
	@Register("read-write",0x48)	RW_Reg!(mask_DCR,uint) DCR;		/// DMA control register offset:0x00000048
	@Register("read-write",0x4C)	RW_Reg!(mask_DMAR,uint) DMAR;		/// DMA address for full transfer offset:0x0000004C
	@Register("read-write",0x50)	RW_Reg!(mask_OR,uint) OR;		/// TIM5 option register offset:0x00000050
}
/// General purpose timers address:0x40014000
struct TIM9_Type{
	@disable this();
	enum Interrupt {
		TIM1_BRK_TIM9 = 24,		/// TIM1 Break interrupt and TIM9 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		OPM = 0x00000008,		/// One-pulse mode offset:3 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// control register 2
	enum mask_CR2 {
		MMS = 0x00000070,		/// Master mode selection offset:4 width:3 
	}
	/// slave mode control register
	enum mask_SMCR {
		MSM = 0x00000080,		/// Master/Slave mode offset:7 width:1 
		TS = 0x00000070,		/// Trigger selection offset:4 width:3 
		SMS = 0x00000007,		/// Slave mode selection offset:0 width:3 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		TIE = 0x00000040,		/// Trigger interrupt enable offset:6 width:1 
		CC2IE = 0x00000004,		/// Capture/Compare 2 interrupt enable offset:2 width:1 
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC2OF = 0x00000400,		/// Capture/compare 2 overcapture flag offset:10 width:1 
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		TIF = 0x00000040,		/// Trigger interrupt flag offset:6 width:1 
		CC2IF = 0x00000004,		/// Capture/Compare 2 interrupt flag offset:2 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		TG = 0x00000040,		/// Trigger generation offset:6 width:1 
		CC2G = 0x00000004,		/// Capture/compare 2 generation offset:2 width:1 
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC2M = 0x00007000,		/// Output Compare 2 mode offset:12 width:3 
		OC2PE = 0x00000800,		/// Output Compare 2 preload enable offset:11 width:1 
		OC2FE = 0x00000400,		/// Output Compare 2 fast enable offset:10 width:1 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		OC1M = 0x00000070,		/// Output Compare 1 mode offset:4 width:3 
		OC1PE = 0x00000008,		/// Output Compare 1 preload enable offset:3 width:1 
		OC1FE = 0x00000004,		/// Output Compare 1 fast enable offset:2 width:1 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC2F = 0x00007000,		/// Input capture 2 filter offset:12 width:3 
		IC2PCS = 0x00000C00,		/// Input capture 2 prescaler offset:10 width:2 
		CC2S = 0x00000300,		/// Capture/Compare 2 selection offset:8 width:2 
		IC1F = 0x00000070,		/// Input capture 1 filter offset:4 width:3 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC2NP = 0x00000080,		/// Capture/Compare 2 output Polarity offset:7 width:1 
		CC2P = 0x00000020,		/// Capture/Compare 2 output Polarity offset:5 width:1 
		CC2E = 0x00000010,		/// Capture/Compare 2 output enable offset:4 width:1 
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT = 0x0000FFFF,		/// counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR = 0x0000FFFF,		/// Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1 = 0x0000FFFF,		/// Capture/Compare 1 value offset:0 width:16 
	}
	/// capture/compare register 2
	enum mask_CCR2 {
		CCR2 = 0x0000FFFF,		/// Capture/Compare 2 value offset:0 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CR2,uint) CR2;		/// control register 2 offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_SMCR,uint) SMCR;		/// slave mode control register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	uint[0x01]		Reserved0;
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved1;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_CCR2,uint) CCR2;		/// capture/compare register 2 offset:0x00000038
}
/// General-purpose-timers address:0x40014400
struct TIM10_Type{
	@disable this();
	enum Interrupt {
		TIM1_UP_TIM10 = 25,		/// TIM1 Update interrupt and TIM10 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC1M = 0x00000070,		/// Output Compare 1 mode offset:4 width:3 
		OC1PE = 0x00000008,		/// Output Compare 1 preload enable offset:3 width:1 
		OC1FE = 0x00000004,		/// Output Compare 1 fast enable offset:2 width:1 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT = 0x0000FFFF,		/// counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR = 0x0000FFFF,		/// Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1 = 0x0000FFFF,		/// Capture/Compare 1 value offset:0 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	uint[0x02]		Reserved0;
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	uint[0x01]		Reserved1;
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved2;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
}
/// General-purpose-timers address:0x40014800
struct TIM11_Type{
	@disable this();
	enum Interrupt {
		TIM1_TRG_COM_TIM11 = 26,		/// TIM1 Trigger and Commutation interrupts and TIM11 global interrupt
	};
	/// control register 1
	enum mask_CR1 {
		CKD = 0x00000300,		/// Clock division offset:8 width:2 
		ARPE = 0x00000080,		/// Auto-reload preload enable offset:7 width:1 
		URS = 0x00000004,		/// Update request source offset:2 width:1 
		UDIS = 0x00000002,		/// Update disable offset:1 width:1 
		CEN = 0x00000001,		/// Counter enable offset:0 width:1 
	}
	/// DMA/Interrupt enable register
	enum mask_DIER {
		CC1IE = 0x00000002,		/// Capture/Compare 1 interrupt enable offset:1 width:1 
		UIE = 0x00000001,		/// Update interrupt enable offset:0 width:1 
	}
	/// status register
	enum mask_SR {
		CC1OF = 0x00000200,		/// Capture/Compare 1 overcapture flag offset:9 width:1 
		CC1IF = 0x00000002,		/// Capture/compare 1 interrupt flag offset:1 width:1 
		UIF = 0x00000001,		/// Update interrupt flag offset:0 width:1 
	}
	/// event generation register
	enum mask_EGR {
		CC1G = 0x00000002,		/// Capture/compare 1 generation offset:1 width:1 
		UG = 0x00000001,		/// Update generation offset:0 width:1 
	}
	/// capture/compare mode register 1 (output mode)
	enum mask_CCMR1_Output {
		OC1M = 0x00000070,		/// Output Compare 1 mode offset:4 width:3 
		OC1PE = 0x00000008,		/// Output Compare 1 preload enable offset:3 width:1 
		OC1FE = 0x00000004,		/// Output Compare 1 fast enable offset:2 width:1 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare mode register 1 (input mode)
	enum mask_CCMR1_Input {
		IC1F = 0x000000F0,		/// Input capture 1 filter offset:4 width:4 
		ICPCS = 0x0000000C,		/// Input capture 1 prescaler offset:2 width:2 
		CC1S = 0x00000003,		/// Capture/Compare 1 selection offset:0 width:2 
	}
	/// capture/compare enable register
	enum mask_CCER {
		CC1NP = 0x00000008,		/// Capture/Compare 1 output Polarity offset:3 width:1 
		CC1P = 0x00000002,		/// Capture/Compare 1 output Polarity offset:1 width:1 
		CC1E = 0x00000001,		/// Capture/Compare 1 output enable offset:0 width:1 
	}
	/// counter
	enum mask_CNT {
		CNT = 0x0000FFFF,		/// counter value offset:0 width:16 
	}
	/// prescaler
	enum mask_PSC {
		PSC = 0x0000FFFF,		/// Prescaler value offset:0 width:16 
	}
	/// auto-reload register
	enum mask_ARR {
		ARR = 0x0000FFFF,		/// Auto-reload value offset:0 width:16 
	}
	/// capture/compare register 1
	enum mask_CCR1 {
		CCR1 = 0x0000FFFF,		/// Capture/Compare 1 value offset:0 width:16 
	}
	/// option register
	enum mask_OR {
		RMP = 0x00000003,		/// Input 1 remapping capability offset:0 width:2 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CR1,uint) CR1;		/// control register 1 offset:0x00000000
	uint[0x02]		Reserved0;
	@Register("read-write",0x0C)	RW_Reg!(mask_DIER,uint) DIER;		/// DMA/Interrupt enable register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SR,uint) SR;		/// status register offset:0x00000010
	@Register("write-only",0x14)	WO_Reg!(mask_EGR,uint) EGR;		/// event generation register offset:0x00000014
	union {
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Output,uint) CCMR1_Output;		/// capture/compare mode register 1 (output mode) offset:0x00000018
		@Register("read-write",0x18)	RW_Reg!(mask_CCMR1_Input,uint) CCMR1_Input;		/// capture/compare mode register 1 (input mode) offset:0x00000018
	}
	uint[0x01]		Reserved1;
	@Register("read-write",0x20)	RW_Reg!(mask_CCER,uint) CCER;		/// capture/compare enable register offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_CNT,uint) CNT;		/// counter offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_PSC,uint) PSC;		/// prescaler offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_ARR,uint) ARR;		/// auto-reload register offset:0x0000002C
	uint[0x01]		Reserved2;
	@Register("read-write",0x34)	RW_Reg!(mask_CCR1,uint) CCR1;		/// capture/compare register 1 offset:0x00000034
	uint[0x06]		Reserved3;
	@Register("read-write",0x50)	RW_Reg!(mask_OR,uint) OR;		/// option register offset:0x00000050
}
/// Cryptographic processor address:0x40023000
struct CRC_Type{
	@disable this();
	/// Data register
	enum mask_DR {
		DR = 0xFFFFFFFF,		/// Data Register offset:0 width:32 
	}
	/// Independent Data register
	enum mask_IDR {
		IDR = 0x000000FF,		/// Independent Data register offset:0 width:8 
	}
	/// Control register
	enum mask_CR {
		CR = 0x00000001,		/// Control regidter offset:0 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_DR,uint) DR;		/// Data register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_IDR,uint) IDR;		/// Independent Data register offset:0x00000004
	@Register("write-only",0x08)	WO_Reg!(mask_CR,uint) CR;		/// Control register offset:0x00000008
}
/// USB on the go full speed address:0x50000000
struct OTG_FS_GLOBAL_Type{
	@disable this();
	enum Interrupt {
		OTG_FS_WKUP = 42,		/// USB On-The-Go FS Wakeup through EXTI line interrupt
		OTG_FS = 67,		/// USB On The Go FS global interrupt
	};
	/// OTG_FS control and status register (OTG_FS_GOTGCTL)
	enum mask_FS_GOTGCTL {
		SRQSCS = 0x00000001,		/// Session request success offset:0 width:1 
		SRQ = 0x00000002,		/// Session request offset:1 width:1 
		HNGSCS = 0x00000100,		/// Host negotiation success offset:8 width:1 
		HNPRQ = 0x00000200,		/// HNP request offset:9 width:1 
		HSHNPEN = 0x00000400,		/// Host set HNP enable offset:10 width:1 
		DHNPEN = 0x00000800,		/// Device HNP enabled offset:11 width:1 
		CIDSTS = 0x00010000,		/// Connector ID status offset:16 width:1 
		DBCT = 0x00020000,		/// Long/short debounce time offset:17 width:1 
		ASVLD = 0x00040000,		/// A-session valid offset:18 width:1 
		BSVLD = 0x00080000,		/// B-session valid offset:19 width:1 
	}
	/// OTG_FS interrupt register (OTG_FS_GOTGINT)
	enum mask_FS_GOTGINT {
		SEDET = 0x00000004,		/// Session end detected offset:2 width:1 
		SRSSCHG = 0x00000100,		/// Session request success status change offset:8 width:1 
		HNSSCHG = 0x00000200,		/// Host negotiation success status change offset:9 width:1 
		HNGDET = 0x00020000,		/// Host negotiation detected offset:17 width:1 
		ADTOCHG = 0x00040000,		/// A-device timeout change offset:18 width:1 
		DBCDNE = 0x00080000,		/// Debounce done offset:19 width:1 
	}
	/// OTG_FS AHB configuration register (OTG_FS_GAHBCFG)
	enum mask_FS_GAHBCFG {
		GINT = 0x00000001,		/// Global interrupt mask offset:0 width:1 
		TXFELVL = 0x00000080,		/// TxFIFO empty level offset:7 width:1 
		PTXFELVL = 0x00000100,		/// Periodic TxFIFO empty level offset:8 width:1 
	}
	/// OTG_FS USB configuration register (OTG_FS_GUSBCFG)
	enum mask_FS_GUSBCFG {
		TOCAL = 0x00000007,		/// FS timeout calibration offset:0 width:3 
		PHYSEL = 0x00000040,		/// Full Speed serial transceiver select offset:6 width:1 
		SRPCAP = 0x00000100,		/// SRP-capable offset:8 width:1 
		HNPCAP = 0x00000200,		/// HNP-capable offset:9 width:1 
		TRDT = 0x00003C00,		/// USB turnaround time offset:10 width:4 
		FHMOD = 0x20000000,		/// Force host mode offset:29 width:1 
		FDMOD = 0x40000000,		/// Force device mode offset:30 width:1 
		CTXPKT = 0x80000000,		/// Corrupt Tx packet offset:31 width:1 
	}
	/// OTG_FS reset register (OTG_FS_GRSTCTL)
	enum mask_FS_GRSTCTL {
		CSRST = 0x00000001,		/// Core soft reset offset:0 width:1 
		HSRST = 0x00000002,		/// HCLK soft reset offset:1 width:1 
		FCRST = 0x00000004,		/// Host frame counter reset offset:2 width:1 
		RXFFLSH = 0x00000010,		/// RxFIFO flush offset:4 width:1 
		TXFFLSH = 0x00000020,		/// TxFIFO flush offset:5 width:1 
		TXFNUM = 0x000007C0,		/// TxFIFO number offset:6 width:5 
		AHBIDL = 0x80000000,		/// AHB master idle offset:31 width:1 
	}
	/// OTG_FS core interrupt register (OTG_FS_GINTSTS)
	enum mask_FS_GINTSTS {
		CMOD = 0x00000001,		/// Current mode of operation offset:0 width:1 
		MMIS = 0x00000002,		/// Mode mismatch interrupt offset:1 width:1 
		OTGINT = 0x00000004,		/// OTG interrupt offset:2 width:1 
		SOF = 0x00000008,		/// Start of frame offset:3 width:1 
		RXFLVL = 0x00000010,		/// RxFIFO non-empty offset:4 width:1 
		NPTXFE = 0x00000020,		/// Non-periodic TxFIFO empty offset:5 width:1 
		GINAKEFF = 0x00000040,		/// Global IN non-periodic NAK effective offset:6 width:1 
		GOUTNAKEFF = 0x00000080,		/// Global OUT NAK effective offset:7 width:1 
		ESUSP = 0x00000400,		/// Early suspend offset:10 width:1 
		USBSUSP = 0x00000800,		/// USB suspend offset:11 width:1 
		USBRST = 0x00001000,		/// USB reset offset:12 width:1 
		ENUMDNE = 0x00002000,		/// Enumeration done offset:13 width:1 
		ISOODRP = 0x00004000,		/// Isochronous OUT packet dropped interrupt offset:14 width:1 
		EOPF = 0x00008000,		/// End of periodic frame interrupt offset:15 width:1 
		IEPINT = 0x00040000,		/// IN endpoint interrupt offset:18 width:1 
		OEPINT = 0x00080000,		/// OUT endpoint interrupt offset:19 width:1 
		IISOIXFR = 0x00100000,		/// Incomplete isochronous IN transfer offset:20 width:1 
		IPXFR_INCOMPISOOUT = 0x00200000,		/// Incomplete periodic transfer(Host mode)/Incomplete isochronous OUT transfer(Device mode) offset:21 width:1 
		HPRTINT = 0x01000000,		/// Host port interrupt offset:24 width:1 
		HCINT = 0x02000000,		/// Host channels interrupt offset:25 width:1 
		PTXFE = 0x04000000,		/// Periodic TxFIFO empty offset:26 width:1 
		CIDSCHG = 0x10000000,		/// Connector ID status change offset:28 width:1 
		DISCINT = 0x20000000,		/// Disconnect detected interrupt offset:29 width:1 
		SRQINT = 0x40000000,		/// Session request/new session detected interrupt offset:30 width:1 
		WKUPINT = 0x80000000,		/// Resume/remote wakeup detected interrupt offset:31 width:1 
	}
	/// OTG_FS interrupt mask register (OTG_FS_GINTMSK)
	enum mask_FS_GINTMSK {
		MMISM = 0x00000002,		/// Mode mismatch interrupt mask offset:1 width:1 
		OTGINT = 0x00000004,		/// OTG interrupt mask offset:2 width:1 
		SOFM = 0x00000008,		/// Start of frame mask offset:3 width:1 
		RXFLVLM = 0x00000010,		/// Receive FIFO non-empty mask offset:4 width:1 
		NPTXFEM = 0x00000020,		/// Non-periodic TxFIFO empty mask offset:5 width:1 
		GINAKEFFM = 0x00000040,		/// Global non-periodic IN NAK effective mask offset:6 width:1 
		GONAKEFFM = 0x00000080,		/// Global OUT NAK effective mask offset:7 width:1 
		ESUSPM = 0x00000400,		/// Early suspend mask offset:10 width:1 
		USBSUSPM = 0x00000800,		/// USB suspend mask offset:11 width:1 
		USBRST = 0x00001000,		/// USB reset mask offset:12 width:1 
		ENUMDNEM = 0x00002000,		/// Enumeration done mask offset:13 width:1 
		ISOODRPM = 0x00004000,		/// Isochronous OUT packet dropped interrupt mask offset:14 width:1 
		EOPFM = 0x00008000,		/// End of periodic frame interrupt mask offset:15 width:1 
		EPMISM = 0x00020000,		/// Endpoint mismatch interrupt mask offset:17 width:1 
		IEPINT = 0x00040000,		/// IN endpoints interrupt mask offset:18 width:1 
		OEPINT = 0x00080000,		/// OUT endpoints interrupt mask offset:19 width:1 
		IISOIXFRM = 0x00100000,		/// Incomplete isochronous IN transfer mask offset:20 width:1 
		IPXFRM_IISOOXFRM = 0x00200000,		/// Incomplete periodic transfer mask(Host mode)/Incomplete isochronous OUT transfer mask(Device mode) offset:21 width:1 
		PRTIM = 0x01000000,		/// Host port interrupt mask offset:24 width:1 
		HCIM = 0x02000000,		/// Host channels interrupt mask offset:25 width:1 
		PTXFEM = 0x04000000,		/// Periodic TxFIFO empty mask offset:26 width:1 
		CIDSCHGM = 0x10000000,		/// Connector ID status change mask offset:28 width:1 
		DISCINT = 0x20000000,		/// Disconnect detected interrupt mask offset:29 width:1 
		SRQIM = 0x40000000,		/// Session request/new session detected interrupt mask offset:30 width:1 
		WUIM = 0x80000000,		/// Resume/remote wakeup detected interrupt mask offset:31 width:1 
	}
	/// OTG_FS Receive status debug read(Device mode)
	enum mask_FS_GRXSTSR_Device {
		EPNUM = 0x0000000F,		/// Endpoint number offset:0 width:4 
		BCNT = 0x00007FF0,		/// Byte count offset:4 width:11 
		DPID = 0x00018000,		/// Data PID offset:15 width:2 
		PKTSTS = 0x001E0000,		/// Packet status offset:17 width:4 
		FRMNUM = 0x01E00000,		/// Frame number offset:21 width:4 
	}
	/// OTG_FS Receive status debug read(Host mode)
	enum mask_FS_GRXSTSR_Host {
		EPNUM = 0x0000000F,		/// Endpoint number offset:0 width:4 
		BCNT = 0x00007FF0,		/// Byte count offset:4 width:11 
		DPID = 0x00018000,		/// Data PID offset:15 width:2 
		PKTSTS = 0x001E0000,		/// Packet status offset:17 width:4 
		FRMNUM = 0x01E00000,		/// Frame number offset:21 width:4 
	}
	/// OTG_FS Receive FIFO size register (OTG_FS_GRXFSIZ)
	enum mask_FS_GRXFSIZ {
		RXFD = 0x0000FFFF,		/// RxFIFO depth offset:0 width:16 
	}
	/// OTG_FS non-periodic transmit FIFO size register (Device mode)
	enum mask_FS_GNPTXFSIZ_Device {
		TX0FSA = 0x0000FFFF,		/// Endpoint 0 transmit RAM start address offset:0 width:16 
		TX0FD = 0xFFFF0000,		/// Endpoint 0 TxFIFO depth offset:16 width:16 
	}
	/// OTG_FS non-periodic transmit FIFO size register (Host mode)
	enum mask_FS_GNPTXFSIZ_Host {
		NPTXFSA = 0x0000FFFF,		/// Non-periodic transmit RAM start address offset:0 width:16 
		NPTXFD = 0xFFFF0000,		/// Non-periodic TxFIFO depth offset:16 width:16 
	}
	/// OTG_FS non-periodic transmit FIFO/queue status register (OTG_FS_GNPTXSTS)
	enum mask_FS_GNPTXSTS {
		NPTXFSAV = 0x0000FFFF,		/// Non-periodic TxFIFO space available offset:0 width:16 
		NPTQXSAV = 0x00FF0000,		/// Non-periodic transmit request queue space available offset:16 width:8 
		NPTXQTOP = 0x7F000000,		/// Top of the non-periodic transmit request queue offset:24 width:7 
	}
	/// OTG_FS general core configuration register (OTG_FS_GCCFG)
	enum mask_FS_GCCFG {
		PWRDWN = 0x00010000,		/// Power down offset:16 width:1 
		VBUSASEN = 0x00040000,		/// Enable the VBUS sensing device offset:18 width:1 
		VBUSBSEN = 0x00080000,		/// Enable the VBUS sensing device offset:19 width:1 
		SOFOUTEN = 0x00100000,		/// SOF output enable offset:20 width:1 
	}
	/// core ID register
	enum mask_FS_CID {
		PRODUCT_ID = 0xFFFFFFFF,		/// Product ID field offset:0 width:32 
	}
	/// OTG_FS Host periodic transmit FIFO size register (OTG_FS_HPTXFSIZ)
	enum mask_FS_HPTXFSIZ {
		PTXSA = 0x0000FFFF,		/// Host periodic TxFIFO start address offset:0 width:16 
		PTXFSIZ = 0xFFFF0000,		/// Host periodic TxFIFO depth offset:16 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF2)
	enum mask_FS_DIEPTXF1 {
		INEPTXSA = 0x0000FFFF,		/// IN endpoint FIFO2 transmit RAM start address offset:0 width:16 
		INEPTXFD = 0xFFFF0000,		/// IN endpoint TxFIFO depth offset:16 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF3)
	enum mask_FS_DIEPTXF2 {
		INEPTXSA = 0x0000FFFF,		/// IN endpoint FIFO3 transmit RAM start address offset:0 width:16 
		INEPTXFD = 0xFFFF0000,		/// IN endpoint TxFIFO depth offset:16 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF4)
	enum mask_FS_DIEPTXF3 {
		INEPTXSA = 0x0000FFFF,		/// IN endpoint FIFO4 transmit RAM start address offset:0 width:16 
		INEPTXFD = 0xFFFF0000,		/// IN endpoint TxFIFO depth offset:16 width:16 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_FS_GOTGCTL,uint) FS_GOTGCTL;		/// OTG_FS control and status register (OTG_FS_GOTGCTL) offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_FS_GOTGINT,uint) FS_GOTGINT;		/// OTG_FS interrupt register (OTG_FS_GOTGINT) offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_FS_GAHBCFG,uint) FS_GAHBCFG;		/// OTG_FS AHB configuration register (OTG_FS_GAHBCFG) offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_FS_GUSBCFG,uint) FS_GUSBCFG;		/// OTG_FS USB configuration register (OTG_FS_GUSBCFG) offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_FS_GRSTCTL,uint) FS_GRSTCTL;		/// OTG_FS reset register (OTG_FS_GRSTCTL) offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_FS_GINTSTS,uint) FS_GINTSTS;		/// OTG_FS core interrupt register (OTG_FS_GINTSTS) offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_FS_GINTMSK,uint) FS_GINTMSK;		/// OTG_FS interrupt mask register (OTG_FS_GINTMSK) offset:0x00000018
	union {
		@Register("read-only",0x1C)	RO_Reg!(mask_FS_GRXSTSR_Device,uint) FS_GRXSTSR_Device;		/// OTG_FS Receive status debug read(Device mode) offset:0x0000001C
		@Register("read-only",0x1C)	RO_Reg!(mask_FS_GRXSTSR_Host,uint) FS_GRXSTSR_Host;		/// OTG_FS Receive status debug read(Host mode) offset:0x0000001C
	}
	uint[0x01]		Reserved0;
	@Register("read-write",0x24)	RW_Reg!(mask_FS_GRXFSIZ,uint) FS_GRXFSIZ;		/// OTG_FS Receive FIFO size register (OTG_FS_GRXFSIZ) offset:0x00000024
	union {
		@Register("read-write",0x28)	RW_Reg!(mask_FS_GNPTXFSIZ_Device,uint) FS_GNPTXFSIZ_Device;		/// OTG_FS non-periodic transmit FIFO size register (Device mode) offset:0x00000028
		@Register("read-write",0x28)	RW_Reg!(mask_FS_GNPTXFSIZ_Host,uint) FS_GNPTXFSIZ_Host;		/// OTG_FS non-periodic transmit FIFO size register (Host mode) offset:0x00000028
	}
	@Register("read-only",0x2C)	RO_Reg!(mask_FS_GNPTXSTS,uint) FS_GNPTXSTS;		/// OTG_FS non-periodic transmit FIFO/queue status register (OTG_FS_GNPTXSTS) offset:0x0000002C
	uint[0x02]		Reserved1;
	@Register("read-write",0x38)	RW_Reg!(mask_FS_GCCFG,uint) FS_GCCFG;		/// OTG_FS general core configuration register (OTG_FS_GCCFG) offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_FS_CID,uint) FS_CID;		/// core ID register offset:0x0000003C
	uint[0x30]		Reserved2;
	@Register("read-write",0x100)	RW_Reg!(mask_FS_HPTXFSIZ,uint) FS_HPTXFSIZ;		/// OTG_FS Host periodic transmit FIFO size register (OTG_FS_HPTXFSIZ) offset:0x00000100
	@Register("read-write",0x104)	RW_Reg!(mask_FS_DIEPTXF1,uint) FS_DIEPTXF1;		/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF2) offset:0x00000104
	@Register("read-write",0x108)	RW_Reg!(mask_FS_DIEPTXF2,uint) FS_DIEPTXF2;		/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF3) offset:0x00000108
	@Register("read-write",0x10C)	RW_Reg!(mask_FS_DIEPTXF3,uint) FS_DIEPTXF3;		/// OTG_FS device IN endpoint transmit FIFO size register (OTG_FS_DIEPTXF4) offset:0x0000010C
}
/// USB on the go full speed address:0x50000400
struct OTG_FS_HOST_Type{
	@disable this();
	/// OTG_FS host configuration register (OTG_FS_HCFG)
	enum mask_FS_HCFG {
		FSLSPCS = 0x00000003,		/// FS/LS PHY clock select offset:0 width:2 
		FSLSS = 0x00000004,		/// FS- and LS-only support offset:2 width:1 
	}
	/// OTG_FS Host frame interval register
	enum mask_HFIR {
		FRIVL = 0x0000FFFF,		/// Frame interval offset:0 width:16 
	}
	/// OTG_FS host frame number/frame time remaining register (OTG_FS_HFNUM)
	enum mask_FS_HFNUM {
		FRNUM = 0x0000FFFF,		/// Frame number offset:0 width:16 
		FTREM = 0xFFFF0000,		/// Frame time remaining offset:16 width:16 
	}
	/// OTG_FS_Host periodic transmit FIFO/queue status register (OTG_FS_HPTXSTS)
	enum mask_FS_HPTXSTS {
		PTXFSAVL = 0x0000FFFF,		/// Periodic transmit data FIFO space available offset:0 width:16 
		PTXQSAV = 0x00FF0000,		/// Periodic transmit request queue space available offset:16 width:8 
		PTXQTOP = 0xFF000000,		/// Top of the periodic transmit request queue offset:24 width:8 
	}
	/// OTG_FS Host all channels interrupt register
	enum mask_HAINT {
		HAINT = 0x0000FFFF,		/// Channel interrupts offset:0 width:16 
	}
	/// OTG_FS host all channels interrupt mask register
	enum mask_HAINTMSK {
		HAINTM = 0x0000FFFF,		/// Channel interrupt mask offset:0 width:16 
	}
	/// OTG_FS host port control and status register (OTG_FS_HPRT)
	enum mask_FS_HPRT {
		PCSTS = 0x00000001,		/// Port connect status offset:0 width:1 
		PCDET = 0x00000002,		/// Port connect detected offset:1 width:1 
		PENA = 0x00000004,		/// Port enable offset:2 width:1 
		PENCHNG = 0x00000008,		/// Port enable/disable change offset:3 width:1 
		POCA = 0x00000010,		/// Port overcurrent active offset:4 width:1 
		POCCHNG = 0x00000020,		/// Port overcurrent change offset:5 width:1 
		PRES = 0x00000040,		/// Port resume offset:6 width:1 
		PSUSP = 0x00000080,		/// Port suspend offset:7 width:1 
		PRST = 0x00000100,		/// Port reset offset:8 width:1 
		PLSTS = 0x00000C00,		/// Port line status offset:10 width:2 
		PPWR = 0x00001000,		/// Port power offset:12 width:1 
		PTCTL = 0x0001E000,		/// Port test control offset:13 width:4 
		PSPD = 0x00060000,		/// Port speed offset:17 width:2 
	}
	/// OTG_FS host channel-0 characteristics register (OTG_FS_HCCHAR0)
	enum mask_FS_HCCHAR0 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-1 characteristics register (OTG_FS_HCCHAR1)
	enum mask_FS_HCCHAR1 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-2 characteristics register (OTG_FS_HCCHAR2)
	enum mask_FS_HCCHAR2 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-3 characteristics register (OTG_FS_HCCHAR3)
	enum mask_FS_HCCHAR3 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-4 characteristics register (OTG_FS_HCCHAR4)
	enum mask_FS_HCCHAR4 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-5 characteristics register (OTG_FS_HCCHAR5)
	enum mask_FS_HCCHAR5 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-6 characteristics register (OTG_FS_HCCHAR6)
	enum mask_FS_HCCHAR6 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-7 characteristics register (OTG_FS_HCCHAR7)
	enum mask_FS_HCCHAR7 {
		MPSIZ = 0x000007FF,		/// Maximum packet size offset:0 width:11 
		EPNUM = 0x00007800,		/// Endpoint number offset:11 width:4 
		EPDIR = 0x00008000,		/// Endpoint direction offset:15 width:1 
		LSDEV = 0x00020000,		/// Low-speed device offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		MCNT = 0x00300000,		/// Multicount offset:20 width:2 
		DAD = 0x1FC00000,		/// Device address offset:22 width:7 
		ODDFRM = 0x20000000,		/// Odd frame offset:29 width:1 
		CHDIS = 0x40000000,		/// Channel disable offset:30 width:1 
		CHENA = 0x80000000,		/// Channel enable offset:31 width:1 
	}
	/// OTG_FS host channel-0 interrupt register (OTG_FS_HCINT0)
	enum mask_FS_HCINT0 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-1 interrupt register (OTG_FS_HCINT1)
	enum mask_FS_HCINT1 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-2 interrupt register (OTG_FS_HCINT2)
	enum mask_FS_HCINT2 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-3 interrupt register (OTG_FS_HCINT3)
	enum mask_FS_HCINT3 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-4 interrupt register (OTG_FS_HCINT4)
	enum mask_FS_HCINT4 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-5 interrupt register (OTG_FS_HCINT5)
	enum mask_FS_HCINT5 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-6 interrupt register (OTG_FS_HCINT6)
	enum mask_FS_HCINT6 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-7 interrupt register (OTG_FS_HCINT7)
	enum mask_FS_HCINT7 {
		XFRC = 0x00000001,		/// Transfer completed offset:0 width:1 
		CHH = 0x00000002,		/// Channel halted offset:1 width:1 
		STALL = 0x00000008,		/// STALL response received interrupt offset:3 width:1 
		NAK = 0x00000010,		/// NAK response received interrupt offset:4 width:1 
		ACK = 0x00000020,		/// ACK response received/transmitted interrupt offset:5 width:1 
		TXERR = 0x00000080,		/// Transaction error offset:7 width:1 
		BBERR = 0x00000100,		/// Babble error offset:8 width:1 
		FRMOR = 0x00000200,		/// Frame overrun offset:9 width:1 
		DTERR = 0x00000400,		/// Data toggle error offset:10 width:1 
	}
	/// OTG_FS host channel-0 mask register (OTG_FS_HCINTMSK0)
	enum mask_FS_HCINTMSK0 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-1 mask register (OTG_FS_HCINTMSK1)
	enum mask_FS_HCINTMSK1 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-2 mask register (OTG_FS_HCINTMSK2)
	enum mask_FS_HCINTMSK2 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-3 mask register (OTG_FS_HCINTMSK3)
	enum mask_FS_HCINTMSK3 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-4 mask register (OTG_FS_HCINTMSK4)
	enum mask_FS_HCINTMSK4 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-5 mask register (OTG_FS_HCINTMSK5)
	enum mask_FS_HCINTMSK5 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-6 mask register (OTG_FS_HCINTMSK6)
	enum mask_FS_HCINTMSK6 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-7 mask register (OTG_FS_HCINTMSK7)
	enum mask_FS_HCINTMSK7 {
		XFRCM = 0x00000001,		/// Transfer completed mask offset:0 width:1 
		CHHM = 0x00000002,		/// Channel halted mask offset:1 width:1 
		STALLM = 0x00000008,		/// STALL response received interrupt mask offset:3 width:1 
		NAKM = 0x00000010,		/// NAK response received interrupt mask offset:4 width:1 
		ACKM = 0x00000020,		/// ACK response received/transmitted interrupt mask offset:5 width:1 
		NYET = 0x00000040,		/// response received interrupt mask offset:6 width:1 
		TXERRM = 0x00000080,		/// Transaction error mask offset:7 width:1 
		BBERRM = 0x00000100,		/// Babble error mask offset:8 width:1 
		FRMORM = 0x00000200,		/// Frame overrun mask offset:9 width:1 
		DTERRM = 0x00000400,		/// Data toggle error mask offset:10 width:1 
	}
	/// OTG_FS host channel-0 transfer size register
	enum mask_FS_HCTSIZ0 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-1 transfer size register
	enum mask_FS_HCTSIZ1 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-2 transfer size register
	enum mask_FS_HCTSIZ2 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-3 transfer size register
	enum mask_FS_HCTSIZ3 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-x transfer size register
	enum mask_FS_HCTSIZ4 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-5 transfer size register
	enum mask_FS_HCTSIZ5 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-6 transfer size register
	enum mask_FS_HCTSIZ6 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	/// OTG_FS host channel-7 transfer size register
	enum mask_FS_HCTSIZ7 {
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		DPID = 0x60000000,		/// Data PID offset:29 width:2 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_FS_HCFG,uint) FS_HCFG;		/// OTG_FS host configuration register (OTG_FS_HCFG) offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_HFIR,uint) HFIR;		/// OTG_FS Host frame interval register offset:0x00000004
	@Register("read-only",0x08)	RO_Reg!(mask_FS_HFNUM,uint) FS_HFNUM;		/// OTG_FS host frame number/frame time remaining register (OTG_FS_HFNUM) offset:0x00000008
	uint[0x01]		Reserved0;
	@Register("read-write",0x10)	RW_Reg!(mask_FS_HPTXSTS,uint) FS_HPTXSTS;		/// OTG_FS_Host periodic transmit FIFO/queue status register (OTG_FS_HPTXSTS) offset:0x00000010
	@Register("read-only",0x14)	RO_Reg!(mask_HAINT,uint) HAINT;		/// OTG_FS Host all channels interrupt register offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_HAINTMSK,uint) HAINTMSK;		/// OTG_FS host all channels interrupt mask register offset:0x00000018
	uint[0x09]		Reserved1;
	@Register("read-write",0x40)	RW_Reg!(mask_FS_HPRT,uint) FS_HPRT;		/// OTG_FS host port control and status register (OTG_FS_HPRT) offset:0x00000040
	uint[0x2F]		Reserved2;
	@Register("read-write",0x100)	RW_Reg!(mask_FS_HCCHAR0,uint) FS_HCCHAR0;		/// OTG_FS host channel-0 characteristics register (OTG_FS_HCCHAR0) offset:0x00000100
	uint[0x01]		Reserved3;
	@Register("read-write",0x108)	RW_Reg!(mask_FS_HCINT0,uint) FS_HCINT0;		/// OTG_FS host channel-0 interrupt register (OTG_FS_HCINT0) offset:0x00000108
	@Register("read-write",0x10C)	RW_Reg!(mask_FS_HCINTMSK0,uint) FS_HCINTMSK0;		/// OTG_FS host channel-0 mask register (OTG_FS_HCINTMSK0) offset:0x0000010C
	@Register("read-write",0x110)	RW_Reg!(mask_FS_HCTSIZ0,uint) FS_HCTSIZ0;		/// OTG_FS host channel-0 transfer size register offset:0x00000110
	uint[0x03]		Reserved4;
	@Register("read-write",0x120)	RW_Reg!(mask_FS_HCCHAR1,uint) FS_HCCHAR1;		/// OTG_FS host channel-1 characteristics register (OTG_FS_HCCHAR1) offset:0x00000120
	uint[0x01]		Reserved5;
	@Register("read-write",0x128)	RW_Reg!(mask_FS_HCINT1,uint) FS_HCINT1;		/// OTG_FS host channel-1 interrupt register (OTG_FS_HCINT1) offset:0x00000128
	@Register("read-write",0x12C)	RW_Reg!(mask_FS_HCINTMSK1,uint) FS_HCINTMSK1;		/// OTG_FS host channel-1 mask register (OTG_FS_HCINTMSK1) offset:0x0000012C
	@Register("read-write",0x130)	RW_Reg!(mask_FS_HCTSIZ1,uint) FS_HCTSIZ1;		/// OTG_FS host channel-1 transfer size register offset:0x00000130
	uint[0x03]		Reserved6;
	@Register("read-write",0x140)	RW_Reg!(mask_FS_HCCHAR2,uint) FS_HCCHAR2;		/// OTG_FS host channel-2 characteristics register (OTG_FS_HCCHAR2) offset:0x00000140
	uint[0x01]		Reserved7;
	@Register("read-write",0x148)	RW_Reg!(mask_FS_HCINT2,uint) FS_HCINT2;		/// OTG_FS host channel-2 interrupt register (OTG_FS_HCINT2) offset:0x00000148
	@Register("read-write",0x14C)	RW_Reg!(mask_FS_HCINTMSK2,uint) FS_HCINTMSK2;		/// OTG_FS host channel-2 mask register (OTG_FS_HCINTMSK2) offset:0x0000014C
	@Register("read-write",0x150)	RW_Reg!(mask_FS_HCTSIZ2,uint) FS_HCTSIZ2;		/// OTG_FS host channel-2 transfer size register offset:0x00000150
	uint[0x03]		Reserved8;
	@Register("read-write",0x160)	RW_Reg!(mask_FS_HCCHAR3,uint) FS_HCCHAR3;		/// OTG_FS host channel-3 characteristics register (OTG_FS_HCCHAR3) offset:0x00000160
	uint[0x01]		Reserved9;
	@Register("read-write",0x168)	RW_Reg!(mask_FS_HCINT3,uint) FS_HCINT3;		/// OTG_FS host channel-3 interrupt register (OTG_FS_HCINT3) offset:0x00000168
	@Register("read-write",0x16C)	RW_Reg!(mask_FS_HCINTMSK3,uint) FS_HCINTMSK3;		/// OTG_FS host channel-3 mask register (OTG_FS_HCINTMSK3) offset:0x0000016C
	@Register("read-write",0x170)	RW_Reg!(mask_FS_HCTSIZ3,uint) FS_HCTSIZ3;		/// OTG_FS host channel-3 transfer size register offset:0x00000170
	uint[0x03]		Reserved10;
	@Register("read-write",0x180)	RW_Reg!(mask_FS_HCCHAR4,uint) FS_HCCHAR4;		/// OTG_FS host channel-4 characteristics register (OTG_FS_HCCHAR4) offset:0x00000180
	uint[0x01]		Reserved11;
	@Register("read-write",0x188)	RW_Reg!(mask_FS_HCINT4,uint) FS_HCINT4;		/// OTG_FS host channel-4 interrupt register (OTG_FS_HCINT4) offset:0x00000188
	@Register("read-write",0x18C)	RW_Reg!(mask_FS_HCINTMSK4,uint) FS_HCINTMSK4;		/// OTG_FS host channel-4 mask register (OTG_FS_HCINTMSK4) offset:0x0000018C
	@Register("read-write",0x190)	RW_Reg!(mask_FS_HCTSIZ4,uint) FS_HCTSIZ4;		/// OTG_FS host channel-x transfer size register offset:0x00000190
	uint[0x03]		Reserved12;
	@Register("read-write",0x1A0)	RW_Reg!(mask_FS_HCCHAR5,uint) FS_HCCHAR5;		/// OTG_FS host channel-5 characteristics register (OTG_FS_HCCHAR5) offset:0x000001A0
	uint[0x01]		Reserved13;
	@Register("read-write",0x1A8)	RW_Reg!(mask_FS_HCINT5,uint) FS_HCINT5;		/// OTG_FS host channel-5 interrupt register (OTG_FS_HCINT5) offset:0x000001A8
	@Register("read-write",0x1AC)	RW_Reg!(mask_FS_HCINTMSK5,uint) FS_HCINTMSK5;		/// OTG_FS host channel-5 mask register (OTG_FS_HCINTMSK5) offset:0x000001AC
	@Register("read-write",0x1B0)	RW_Reg!(mask_FS_HCTSIZ5,uint) FS_HCTSIZ5;		/// OTG_FS host channel-5 transfer size register offset:0x000001B0
	uint[0x03]		Reserved14;
	@Register("read-write",0x1C0)	RW_Reg!(mask_FS_HCCHAR6,uint) FS_HCCHAR6;		/// OTG_FS host channel-6 characteristics register (OTG_FS_HCCHAR6) offset:0x000001C0
	uint[0x01]		Reserved15;
	@Register("read-write",0x1C8)	RW_Reg!(mask_FS_HCINT6,uint) FS_HCINT6;		/// OTG_FS host channel-6 interrupt register (OTG_FS_HCINT6) offset:0x000001C8
	@Register("read-write",0x1CC)	RW_Reg!(mask_FS_HCINTMSK6,uint) FS_HCINTMSK6;		/// OTG_FS host channel-6 mask register (OTG_FS_HCINTMSK6) offset:0x000001CC
	@Register("read-write",0x1D0)	RW_Reg!(mask_FS_HCTSIZ6,uint) FS_HCTSIZ6;		/// OTG_FS host channel-6 transfer size register offset:0x000001D0
	uint[0x03]		Reserved16;
	@Register("read-write",0x1E0)	RW_Reg!(mask_FS_HCCHAR7,uint) FS_HCCHAR7;		/// OTG_FS host channel-7 characteristics register (OTG_FS_HCCHAR7) offset:0x000001E0
	uint[0x01]		Reserved17;
	@Register("read-write",0x1E8)	RW_Reg!(mask_FS_HCINT7,uint) FS_HCINT7;		/// OTG_FS host channel-7 interrupt register (OTG_FS_HCINT7) offset:0x000001E8
	@Register("read-write",0x1EC)	RW_Reg!(mask_FS_HCINTMSK7,uint) FS_HCINTMSK7;		/// OTG_FS host channel-7 mask register (OTG_FS_HCINTMSK7) offset:0x000001EC
	@Register("read-write",0x1F0)	RW_Reg!(mask_FS_HCTSIZ7,uint) FS_HCTSIZ7;		/// OTG_FS host channel-7 transfer size register offset:0x000001F0
}
/// USB on the go full speed address:0x50000800
struct OTG_FS_DEVICE_Type{
	@disable this();
	/// OTG_FS device configuration register (OTG_FS_DCFG)
	enum mask_FS_DCFG {
		DSPD = 0x00000003,		/// Device speed offset:0 width:2 
		NZLSOHSK = 0x00000004,		/// Non-zero-length status OUT handshake offset:2 width:1 
		DAD = 0x000007F0,		/// Device address offset:4 width:7 
		PFIVL = 0x00001800,		/// Periodic frame interval offset:11 width:2 
	}
	/// OTG_FS device control register (OTG_FS_DCTL)
	enum mask_FS_DCTL {
		RWUSIG = 0x00000001,		/// Remote wakeup signaling offset:0 width:1 
		SDIS = 0x00000002,		/// Soft disconnect offset:1 width:1 
		GINSTS = 0x00000004,		/// Global IN NAK status offset:2 width:1 
		GONSTS = 0x00000008,		/// Global OUT NAK status offset:3 width:1 
		TCTL = 0x00000070,		/// Test control offset:4 width:3 
		SGINAK = 0x00000080,		/// Set global IN NAK offset:7 width:1 
		CGINAK = 0x00000100,		/// Clear global IN NAK offset:8 width:1 
		SGONAK = 0x00000200,		/// Set global OUT NAK offset:9 width:1 
		CGONAK = 0x00000400,		/// Clear global OUT NAK offset:10 width:1 
		POPRGDNE = 0x00000800,		/// Power-on programming done offset:11 width:1 
	}
	/// OTG_FS device status register (OTG_FS_DSTS)
	enum mask_FS_DSTS {
		SUSPSTS = 0x00000001,		/// Suspend status offset:0 width:1 
		ENUMSPD = 0x00000006,		/// Enumerated speed offset:1 width:2 
		EERR = 0x00000008,		/// Erratic error offset:3 width:1 
		FNSOF = 0x003FFF00,		/// Frame number of the received SOF offset:8 width:14 
	}
	/// OTG_FS device IN endpoint common interrupt mask register (OTG_FS_DIEPMSK)
	enum mask_FS_DIEPMSK {
		XFRCM = 0x00000001,		/// Transfer completed interrupt mask offset:0 width:1 
		EPDM = 0x00000002,		/// Endpoint disabled interrupt mask offset:1 width:1 
		TOM = 0x00000008,		/// Timeout condition mask (Non-isochronous endpoints) offset:3 width:1 
		ITTXFEMSK = 0x00000010,		/// IN token received when TxFIFO empty mask offset:4 width:1 
		INEPNMM = 0x00000020,		/// IN token received with EP mismatch mask offset:5 width:1 
		INEPNEM = 0x00000040,		/// IN endpoint NAK effective mask offset:6 width:1 
	}
	/// OTG_FS device OUT endpoint common interrupt mask register (OTG_FS_DOEPMSK)
	enum mask_FS_DOEPMSK {
		XFRCM = 0x00000001,		/// Transfer completed interrupt mask offset:0 width:1 
		EPDM = 0x00000002,		/// Endpoint disabled interrupt mask offset:1 width:1 
		STUPM = 0x00000008,		/// SETUP phase done mask offset:3 width:1 
		OTEPDM = 0x00000010,		/// OUT token received when endpoint disabled mask offset:4 width:1 
	}
	/// OTG_FS device all endpoints interrupt register (OTG_FS_DAINT)
	enum mask_FS_DAINT {
		IEPINT = 0x0000FFFF,		/// IN endpoint interrupt bits offset:0 width:16 
		OEPINT = 0xFFFF0000,		/// OUT endpoint interrupt bits offset:16 width:16 
	}
	/// OTG_FS all endpoints interrupt mask register (OTG_FS_DAINTMSK)
	enum mask_FS_DAINTMSK {
		IEPM = 0x0000FFFF,		/// IN EP interrupt mask bits offset:0 width:16 
		OEPINT = 0xFFFF0000,		/// OUT endpoint interrupt bits offset:16 width:16 
	}
	/// OTG_FS device VBUS discharge time register
	enum mask_DVBUSDIS {
		VBUSDT = 0x0000FFFF,		/// Device VBUS discharge time offset:0 width:16 
	}
	/// OTG_FS device VBUS pulsing time register
	enum mask_DVBUSPULSE {
		DVBUSP = 0x00000FFF,		/// Device VBUS pulsing time offset:0 width:12 
	}
	/// OTG_FS device IN endpoint FIFO empty interrupt mask register
	enum mask_DIEPEMPMSK {
		INEPTXFEM = 0x0000FFFF,		/// IN EP Tx FIFO empty interrupt mask bits offset:0 width:16 
	}
	/// OTG_FS device control IN endpoint 0 control register (OTG_FS_DIEPCTL0)
	enum mask_FS_DIEPCTL0 {
		MPSIZ = 0x00000003,		/// Maximum packet size offset:0 width:2 
		USBAEP = 0x00008000,		/// USB active endpoint offset:15 width:1 
		NAKSTS = 0x00020000,		/// NAK status offset:17 width:1 
		EPTYP = 0x000C0000,		/// Endpoint type offset:18 width:2 
		STALL = 0x00200000,		/// STALL handshake offset:21 width:1 
		TXFNUM = 0x03C00000,		/// TxFIFO number offset:22 width:4 
		CNAK = 0x04000000,		/// Clear NAK offset:26 width:1 
		SNAK = 0x08000000,		/// Set NAK offset:27 width:1 
		EPDIS = 0x40000000,		/// Endpoint disable offset:30 width:1 
		EPENA = 0x80000000,		/// Endpoint enable offset:31 width:1 
	}
	/// OTG device endpoint-1 control register
	enum mask_DIEPCTL1 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM_SD1PID = 0x20000000,		/// SODDFRM/SD1PID offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		TXFNUM = 0x03C00000,		/// TXFNUM offset:22 width:4 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// OTG device endpoint-2 control register
	enum mask_DIEPCTL2 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM = 0x20000000,		/// SODDFRM offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		TXFNUM = 0x03C00000,		/// TXFNUM offset:22 width:4 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// OTG device endpoint-3 control register
	enum mask_DIEPCTL3 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM = 0x20000000,		/// SODDFRM offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		TXFNUM = 0x03C00000,		/// TXFNUM offset:22 width:4 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// device endpoint-0 control register
	enum mask_DOEPCTL0 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		SNPM = 0x00100000,		/// SNPM offset:20 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x00000003,		/// MPSIZ offset:0 width:2 
	}
	/// device endpoint-1 control register
	enum mask_DOEPCTL1 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM = 0x20000000,		/// SODDFRM offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		SNPM = 0x00100000,		/// SNPM offset:20 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// device endpoint-2 control register
	enum mask_DOEPCTL2 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM = 0x20000000,		/// SODDFRM offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		SNPM = 0x00100000,		/// SNPM offset:20 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// device endpoint-3 control register
	enum mask_DOEPCTL3 {
		EPENA = 0x80000000,		/// EPENA offset:31 width:1 
		EPDIS = 0x40000000,		/// EPDIS offset:30 width:1 
		SODDFRM = 0x20000000,		/// SODDFRM offset:29 width:1 
		SD0PID_SEVNFRM = 0x10000000,		/// SD0PID/SEVNFRM offset:28 width:1 
		SNAK = 0x08000000,		/// SNAK offset:27 width:1 
		CNAK = 0x04000000,		/// CNAK offset:26 width:1 
		Stall = 0x00200000,		/// Stall offset:21 width:1 
		SNPM = 0x00100000,		/// SNPM offset:20 width:1 
		EPTYP = 0x000C0000,		/// EPTYP offset:18 width:2 
		NAKSTS = 0x00020000,		/// NAKSTS offset:17 width:1 
		EONUM_DPID = 0x00010000,		/// EONUM/DPID offset:16 width:1 
		USBAEP = 0x00008000,		/// USBAEP offset:15 width:1 
		MPSIZ = 0x000007FF,		/// MPSIZ offset:0 width:11 
	}
	/// device endpoint-x interrupt register
	enum mask_DIEPINT0 {
		TXFE = 0x00000080,		/// TXFE offset:7 width:1 
		INEPNE = 0x00000040,		/// INEPNE offset:6 width:1 
		ITTXFE = 0x00000010,		/// ITTXFE offset:4 width:1 
		TOC = 0x00000008,		/// TOC offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-1 interrupt register
	enum mask_DIEPINT1 {
		TXFE = 0x00000080,		/// TXFE offset:7 width:1 
		INEPNE = 0x00000040,		/// INEPNE offset:6 width:1 
		ITTXFE = 0x00000010,		/// ITTXFE offset:4 width:1 
		TOC = 0x00000008,		/// TOC offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-2 interrupt register
	enum mask_DIEPINT2 {
		TXFE = 0x00000080,		/// TXFE offset:7 width:1 
		INEPNE = 0x00000040,		/// INEPNE offset:6 width:1 
		ITTXFE = 0x00000010,		/// ITTXFE offset:4 width:1 
		TOC = 0x00000008,		/// TOC offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-3 interrupt register
	enum mask_DIEPINT3 {
		TXFE = 0x00000080,		/// TXFE offset:7 width:1 
		INEPNE = 0x00000040,		/// INEPNE offset:6 width:1 
		ITTXFE = 0x00000010,		/// ITTXFE offset:4 width:1 
		TOC = 0x00000008,		/// TOC offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-0 interrupt register
	enum mask_DOEPINT0 {
		B2BSTUP = 0x00000040,		/// B2BSTUP offset:6 width:1 
		OTEPDIS = 0x00000010,		/// OTEPDIS offset:4 width:1 
		STUP = 0x00000008,		/// STUP offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-1 interrupt register
	enum mask_DOEPINT1 {
		B2BSTUP = 0x00000040,		/// B2BSTUP offset:6 width:1 
		OTEPDIS = 0x00000010,		/// OTEPDIS offset:4 width:1 
		STUP = 0x00000008,		/// STUP offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-2 interrupt register
	enum mask_DOEPINT2 {
		B2BSTUP = 0x00000040,		/// B2BSTUP offset:6 width:1 
		OTEPDIS = 0x00000010,		/// OTEPDIS offset:4 width:1 
		STUP = 0x00000008,		/// STUP offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-3 interrupt register
	enum mask_DOEPINT3 {
		B2BSTUP = 0x00000040,		/// B2BSTUP offset:6 width:1 
		OTEPDIS = 0x00000010,		/// OTEPDIS offset:4 width:1 
		STUP = 0x00000008,		/// STUP offset:3 width:1 
		EPDISD = 0x00000002,		/// EPDISD offset:1 width:1 
		XFRC = 0x00000001,		/// XFRC offset:0 width:1 
	}
	/// device endpoint-0 transfer size register
	enum mask_DIEPTSIZ0 {
		PKTCNT = 0x00180000,		/// Packet count offset:19 width:2 
		XFRSIZ = 0x0000007F,		/// Transfer size offset:0 width:7 
	}
	/// device OUT endpoint-0 transfer size register
	enum mask_DOEPTSIZ0 {
		STUPCNT = 0x60000000,		/// SETUP packet count offset:29 width:2 
		PKTCNT = 0x00080000,		/// Packet count offset:19 width:1 
		XFRSIZ = 0x0000007F,		/// Transfer size offset:0 width:7 
	}
	/// device endpoint-1 transfer size register
	enum mask_DIEPTSIZ1 {
		MCNT = 0x60000000,		/// Multi count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	/// device endpoint-2 transfer size register
	enum mask_DIEPTSIZ2 {
		MCNT = 0x60000000,		/// Multi count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	/// device endpoint-3 transfer size register
	enum mask_DIEPTSIZ3 {
		MCNT = 0x60000000,		/// Multi count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	/// OTG_FS device IN endpoint transmit FIFO status register
	enum mask_DTXFSTS0 {
		INEPTFSAV = 0x0000FFFF,		/// IN endpoint TxFIFO space available offset:0 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO status register
	enum mask_DTXFSTS1 {
		INEPTFSAV = 0x0000FFFF,		/// IN endpoint TxFIFO space available offset:0 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO status register
	enum mask_DTXFSTS2 {
		INEPTFSAV = 0x0000FFFF,		/// IN endpoint TxFIFO space available offset:0 width:16 
	}
	/// OTG_FS device IN endpoint transmit FIFO status register
	enum mask_DTXFSTS3 {
		INEPTFSAV = 0x0000FFFF,		/// IN endpoint TxFIFO space available offset:0 width:16 
	}
	/// device OUT endpoint-1 transfer size register
	enum mask_DOEPTSIZ1 {
		RXDPID_STUPCNT = 0x60000000,		/// Received data PID/SETUP packet count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	/// device OUT endpoint-2 transfer size register
	enum mask_DOEPTSIZ2 {
		RXDPID_STUPCNT = 0x60000000,		/// Received data PID/SETUP packet count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	/// device OUT endpoint-3 transfer size register
	enum mask_DOEPTSIZ3 {
		RXDPID_STUPCNT = 0x60000000,		/// Received data PID/SETUP packet count offset:29 width:2 
		PKTCNT = 0x1FF80000,		/// Packet count offset:19 width:10 
		XFRSIZ = 0x0007FFFF,		/// Transfer size offset:0 width:19 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_FS_DCFG,uint) FS_DCFG;		/// OTG_FS device configuration register (OTG_FS_DCFG) offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_FS_DCTL,uint) FS_DCTL;		/// OTG_FS device control register (OTG_FS_DCTL) offset:0x00000004
	@Register("read-only",0x08)	RO_Reg!(mask_FS_DSTS,uint) FS_DSTS;		/// OTG_FS device status register (OTG_FS_DSTS) offset:0x00000008
	uint[0x01]		Reserved0;
	@Register("read-write",0x10)	RW_Reg!(mask_FS_DIEPMSK,uint) FS_DIEPMSK;		/// OTG_FS device IN endpoint common interrupt mask register (OTG_FS_DIEPMSK) offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_FS_DOEPMSK,uint) FS_DOEPMSK;		/// OTG_FS device OUT endpoint common interrupt mask register (OTG_FS_DOEPMSK) offset:0x00000014
	@Register("read-only",0x18)	RO_Reg!(mask_FS_DAINT,uint) FS_DAINT;		/// OTG_FS device all endpoints interrupt register (OTG_FS_DAINT) offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_FS_DAINTMSK,uint) FS_DAINTMSK;		/// OTG_FS all endpoints interrupt mask register (OTG_FS_DAINTMSK) offset:0x0000001C
	uint[0x02]		Reserved1;
	@Register("read-write",0x28)	RW_Reg!(mask_DVBUSDIS,uint) DVBUSDIS;		/// OTG_FS device VBUS discharge time register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_DVBUSPULSE,uint) DVBUSPULSE;		/// OTG_FS device VBUS pulsing time register offset:0x0000002C
	uint[0x01]		Reserved2;
	@Register("read-write",0x34)	RW_Reg!(mask_DIEPEMPMSK,uint) DIEPEMPMSK;		/// OTG_FS device IN endpoint FIFO empty interrupt mask register offset:0x00000034
	uint[0x32]		Reserved3;
	@Register("read-write",0x100)	RW_Reg!(mask_FS_DIEPCTL0,uint) FS_DIEPCTL0;		/// OTG_FS device control IN endpoint 0 control register (OTG_FS_DIEPCTL0) offset:0x00000100
	uint[0x01]		Reserved4;
	@Register("read-write",0x108)	RW_Reg!(mask_DIEPINT0,uint) DIEPINT0;		/// device endpoint-x interrupt register offset:0x00000108
	uint[0x01]		Reserved5;
	@Register("read-write",0x110)	RW_Reg!(mask_DIEPTSIZ0,uint) DIEPTSIZ0;		/// device endpoint-0 transfer size register offset:0x00000110
	uint[0x01]		Reserved6;
	@Register("read-only",0x118)	RO_Reg!(mask_DTXFSTS0,uint) DTXFSTS0;		/// OTG_FS device IN endpoint transmit FIFO status register offset:0x00000118
	uint[0x01]		Reserved7;
	@Register("read-write",0x120)	RW_Reg!(mask_DIEPCTL1,uint) DIEPCTL1;		/// OTG device endpoint-1 control register offset:0x00000120
	uint[0x01]		Reserved8;
	@Register("read-write",0x128)	RW_Reg!(mask_DIEPINT1,uint) DIEPINT1;		/// device endpoint-1 interrupt register offset:0x00000128
	uint[0x01]		Reserved9;
	@Register("read-write",0x130)	RW_Reg!(mask_DIEPTSIZ1,uint) DIEPTSIZ1;		/// device endpoint-1 transfer size register offset:0x00000130
	uint[0x01]		Reserved10;
	@Register("read-only",0x138)	RO_Reg!(mask_DTXFSTS1,uint) DTXFSTS1;		/// OTG_FS device IN endpoint transmit FIFO status register offset:0x00000138
	uint[0x01]		Reserved11;
	@Register("read-write",0x140)	RW_Reg!(mask_DIEPCTL2,uint) DIEPCTL2;		/// OTG device endpoint-2 control register offset:0x00000140
	uint[0x01]		Reserved12;
	@Register("read-write",0x148)	RW_Reg!(mask_DIEPINT2,uint) DIEPINT2;		/// device endpoint-2 interrupt register offset:0x00000148
	uint[0x01]		Reserved13;
	@Register("read-write",0x150)	RW_Reg!(mask_DIEPTSIZ2,uint) DIEPTSIZ2;		/// device endpoint-2 transfer size register offset:0x00000150
	uint[0x01]		Reserved14;
	@Register("read-only",0x158)	RO_Reg!(mask_DTXFSTS2,uint) DTXFSTS2;		/// OTG_FS device IN endpoint transmit FIFO status register offset:0x00000158
	uint[0x01]		Reserved15;
	@Register("read-write",0x160)	RW_Reg!(mask_DIEPCTL3,uint) DIEPCTL3;		/// OTG device endpoint-3 control register offset:0x00000160
	uint[0x01]		Reserved16;
	@Register("read-write",0x168)	RW_Reg!(mask_DIEPINT3,uint) DIEPINT3;		/// device endpoint-3 interrupt register offset:0x00000168
	uint[0x01]		Reserved17;
	@Register("read-write",0x170)	RW_Reg!(mask_DIEPTSIZ3,uint) DIEPTSIZ3;		/// device endpoint-3 transfer size register offset:0x00000170
	uint[0x01]		Reserved18;
	@Register("read-only",0x178)	RO_Reg!(mask_DTXFSTS3,uint) DTXFSTS3;		/// OTG_FS device IN endpoint transmit FIFO status register offset:0x00000178
	uint[0x61]		Reserved19;
	@Register("read-write",0x300)	RW_Reg!(mask_DOEPCTL0,uint) DOEPCTL0;		/// device endpoint-0 control register offset:0x00000300
	uint[0x01]		Reserved20;
	@Register("read-write",0x308)	RW_Reg!(mask_DOEPINT0,uint) DOEPINT0;		/// device endpoint-0 interrupt register offset:0x00000308
	uint[0x01]		Reserved21;
	@Register("read-write",0x310)	RW_Reg!(mask_DOEPTSIZ0,uint) DOEPTSIZ0;		/// device OUT endpoint-0 transfer size register offset:0x00000310
	uint[0x03]		Reserved22;
	@Register("read-write",0x320)	RW_Reg!(mask_DOEPCTL1,uint) DOEPCTL1;		/// device endpoint-1 control register offset:0x00000320
	uint[0x01]		Reserved23;
	@Register("read-write",0x328)	RW_Reg!(mask_DOEPINT1,uint) DOEPINT1;		/// device endpoint-1 interrupt register offset:0x00000328
	uint[0x01]		Reserved24;
	@Register("read-write",0x330)	RW_Reg!(mask_DOEPTSIZ1,uint) DOEPTSIZ1;		/// device OUT endpoint-1 transfer size register offset:0x00000330
	uint[0x03]		Reserved25;
	@Register("read-write",0x340)	RW_Reg!(mask_DOEPCTL2,uint) DOEPCTL2;		/// device endpoint-2 control register offset:0x00000340
	uint[0x01]		Reserved26;
	@Register("read-write",0x348)	RW_Reg!(mask_DOEPINT2,uint) DOEPINT2;		/// device endpoint-2 interrupt register offset:0x00000348
	uint[0x01]		Reserved27;
	@Register("read-write",0x350)	RW_Reg!(mask_DOEPTSIZ2,uint) DOEPTSIZ2;		/// device OUT endpoint-2 transfer size register offset:0x00000350
	uint[0x03]		Reserved28;
	@Register("read-write",0x360)	RW_Reg!(mask_DOEPCTL3,uint) DOEPCTL3;		/// device endpoint-3 control register offset:0x00000360
	uint[0x01]		Reserved29;
	@Register("read-write",0x368)	RW_Reg!(mask_DOEPINT3,uint) DOEPINT3;		/// device endpoint-3 interrupt register offset:0x00000368
	uint[0x01]		Reserved30;
	@Register("read-write",0x370)	RW_Reg!(mask_DOEPTSIZ3,uint) DOEPTSIZ3;		/// device OUT endpoint-3 transfer size register offset:0x00000370
}
/// USB on the go full speed address:0x50000E00
struct OTG_FS_PWRCLK_Type{
	@disable this();
	/// OTG_FS power and clock gating control register
	enum mask_FS_PCGCCTL {
		STPPCLK = 0x00000001,		/// Stop PHY clock offset:0 width:1 
		GATEHCLK = 0x00000002,		/// Gate HCLK offset:1 width:1 
		PHYSUSP = 0x00000010,		/// PHY Suspended offset:4 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_FS_PCGCCTL,uint) FS_PCGCCTL;		/// OTG_FS power and clock gating control register offset:0x00000000
}
/// FLASH address:0x40023C00
struct FLASH_Type{
	@disable this();
	enum Interrupt {
		FLASH = 4,		/// FLASH global interrupt
	};
	/// Flash access control register
	enum mask_ACR {
		LATENCY = 0x00000007,		/// Latency offset:0 width:3 
		PRFTEN = 0x00000100,		/// Prefetch enable offset:8 width:1 
		ICEN = 0x00000200,		/// Instruction cache enable offset:9 width:1 
		DCEN = 0x00000400,		/// Data cache enable offset:10 width:1 
		ICRST = 0x00000800,		/// Instruction cache reset offset:11 width:1 
		DCRST = 0x00001000,		/// Data cache reset offset:12 width:1 
	}
	/// Flash key register
	enum mask_KEYR {
		KEY = 0xFFFFFFFF,		/// FPEC key offset:0 width:32 
	}
	/// Flash option key register
	enum mask_OPTKEYR {
		OPTKEY = 0xFFFFFFFF,		/// Option byte key offset:0 width:32 
	}
	/// Status register
	enum mask_SR {
		EOP = 0x00000001,		/// End of operation offset:0 width:1 
		OPERR = 0x00000002,		/// Operation error offset:1 width:1 
		WRPERR = 0x00000010,		/// Write protection error offset:4 width:1 
		PGAERR = 0x00000020,		/// Programming alignment error offset:5 width:1 
		PGPERR = 0x00000040,		/// Programming parallelism error offset:6 width:1 
		PGSERR = 0x00000080,		/// Programming sequence error offset:7 width:1 
		BSY = 0x00010000,		/// Busy offset:16 width:1 
	}
	/// Control register
	enum mask_CR {
		PG = 0x00000001,		/// Programming offset:0 width:1 
		SER = 0x00000002,		/// Sector Erase offset:1 width:1 
		MER = 0x00000004,		/// Mass Erase offset:2 width:1 
		SNB = 0x00000078,		/// Sector number offset:3 width:4 
		PSIZE = 0x00000300,		/// Program size offset:8 width:2 
		STRT = 0x00010000,		/// Start offset:16 width:1 
		EOPIE = 0x01000000,		/// End of operation interrupt enable offset:24 width:1 
		ERRIE = 0x02000000,		/// Error interrupt enable offset:25 width:1 
		LOCK = 0x80000000,		/// Lock offset:31 width:1 
	}
	/// Flash option control register
	enum mask_OPTCR {
		OPTLOCK = 0x00000001,		/// Option lock offset:0 width:1 
		OPTSTRT = 0x00000002,		/// Option start offset:1 width:1 
		BOR_LEV = 0x0000000C,		/// BOR reset Level offset:2 width:2 
		WDG_SW = 0x00000020,		/// WDG_SW User option bytes offset:5 width:1 
		nRST_STOP = 0x00000040,		/// nRST_STOP User option bytes offset:6 width:1 
		nRST_STDBY = 0x00000080,		/// nRST_STDBY User option bytes offset:7 width:1 
		RDP = 0x0000FF00,		/// Read protect offset:8 width:8 
		nWRP = 0x0FFF0000,		/// Not write protect offset:16 width:12 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_ACR,uint) ACR;		/// Flash access control register offset:0x00000000
	@Register("write-only",0x04)	WO_Reg!(mask_KEYR,uint) KEYR;		/// Flash key register offset:0x00000004
	@Register("write-only",0x08)	WO_Reg!(mask_OPTKEYR,uint) OPTKEYR;		/// Flash option key register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_SR,uint) SR;		/// Status register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_CR,uint) CR;		/// Control register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_OPTCR,uint) OPTCR;		/// Flash option control register offset:0x00000014
}
/// External interrupt/event controller address:0x40013C00
struct EXTI_Type{
	@disable this();
	enum Interrupt {
		EXTI0 = 6,		/// EXTI Line0 interrupt
		EXTI1 = 7,		/// EXTI Line1 interrupt
		TAMP_STAMP = 2,		/// Tamper and TimeStamp interrupts through the EXTI line
		EXTI15_10 = 40,		/// EXTI Line[15:10] interrupts
		EXTI4 = 10,		/// EXTI Line4 interrupt
		EXTI9_5 = 23,		/// EXTI Line[9:5] interrupts
		EXTI2 = 8,		/// EXTI Line2 interrupt
		EXTI3 = 9,		/// EXTI Line3 interrupt
	};
	/// Interrupt mask register (EXTI_IMR)
	enum mask_IMR {
		MR0 = 0x00000001,		/// Interrupt Mask on line 0 offset:0 width:1 
		MR1 = 0x00000002,		/// Interrupt Mask on line 1 offset:1 width:1 
		MR2 = 0x00000004,		/// Interrupt Mask on line 2 offset:2 width:1 
		MR3 = 0x00000008,		/// Interrupt Mask on line 3 offset:3 width:1 
		MR4 = 0x00000010,		/// Interrupt Mask on line 4 offset:4 width:1 
		MR5 = 0x00000020,		/// Interrupt Mask on line 5 offset:5 width:1 
		MR6 = 0x00000040,		/// Interrupt Mask on line 6 offset:6 width:1 
		MR7 = 0x00000080,		/// Interrupt Mask on line 7 offset:7 width:1 
		MR8 = 0x00000100,		/// Interrupt Mask on line 8 offset:8 width:1 
		MR9 = 0x00000200,		/// Interrupt Mask on line 9 offset:9 width:1 
		MR10 = 0x00000400,		/// Interrupt Mask on line 10 offset:10 width:1 
		MR11 = 0x00000800,		/// Interrupt Mask on line 11 offset:11 width:1 
		MR12 = 0x00001000,		/// Interrupt Mask on line 12 offset:12 width:1 
		MR13 = 0x00002000,		/// Interrupt Mask on line 13 offset:13 width:1 
		MR14 = 0x00004000,		/// Interrupt Mask on line 14 offset:14 width:1 
		MR15 = 0x00008000,		/// Interrupt Mask on line 15 offset:15 width:1 
		MR16 = 0x00010000,		/// Interrupt Mask on line 16 offset:16 width:1 
		MR17 = 0x00020000,		/// Interrupt Mask on line 17 offset:17 width:1 
		MR18 = 0x00040000,		/// Interrupt Mask on line 18 offset:18 width:1 
		MR19 = 0x00080000,		/// Interrupt Mask on line 19 offset:19 width:1 
		MR20 = 0x00100000,		/// Interrupt Mask on line 20 offset:20 width:1 
		MR21 = 0x00200000,		/// Interrupt Mask on line 21 offset:21 width:1 
		MR22 = 0x00400000,		/// Interrupt Mask on line 22 offset:22 width:1 
	}
	/// Event mask register (EXTI_EMR)
	enum mask_EMR {
		MR0 = 0x00000001,		/// Event Mask on line 0 offset:0 width:1 
		MR1 = 0x00000002,		/// Event Mask on line 1 offset:1 width:1 
		MR2 = 0x00000004,		/// Event Mask on line 2 offset:2 width:1 
		MR3 = 0x00000008,		/// Event Mask on line 3 offset:3 width:1 
		MR4 = 0x00000010,		/// Event Mask on line 4 offset:4 width:1 
		MR5 = 0x00000020,		/// Event Mask on line 5 offset:5 width:1 
		MR6 = 0x00000040,		/// Event Mask on line 6 offset:6 width:1 
		MR7 = 0x00000080,		/// Event Mask on line 7 offset:7 width:1 
		MR8 = 0x00000100,		/// Event Mask on line 8 offset:8 width:1 
		MR9 = 0x00000200,		/// Event Mask on line 9 offset:9 width:1 
		MR10 = 0x00000400,		/// Event Mask on line 10 offset:10 width:1 
		MR11 = 0x00000800,		/// Event Mask on line 11 offset:11 width:1 
		MR12 = 0x00001000,		/// Event Mask on line 12 offset:12 width:1 
		MR13 = 0x00002000,		/// Event Mask on line 13 offset:13 width:1 
		MR14 = 0x00004000,		/// Event Mask on line 14 offset:14 width:1 
		MR15 = 0x00008000,		/// Event Mask on line 15 offset:15 width:1 
		MR16 = 0x00010000,		/// Event Mask on line 16 offset:16 width:1 
		MR17 = 0x00020000,		/// Event Mask on line 17 offset:17 width:1 
		MR18 = 0x00040000,		/// Event Mask on line 18 offset:18 width:1 
		MR19 = 0x00080000,		/// Event Mask on line 19 offset:19 width:1 
		MR20 = 0x00100000,		/// Event Mask on line 20 offset:20 width:1 
		MR21 = 0x00200000,		/// Event Mask on line 21 offset:21 width:1 
		MR22 = 0x00400000,		/// Event Mask on line 22 offset:22 width:1 
	}
	/// Rising Trigger selection register (EXTI_RTSR)
	enum mask_RTSR {
		TR0 = 0x00000001,		/// Rising trigger event configuration of line 0 offset:0 width:1 
		TR1 = 0x00000002,		/// Rising trigger event configuration of line 1 offset:1 width:1 
		TR2 = 0x00000004,		/// Rising trigger event configuration of line 2 offset:2 width:1 
		TR3 = 0x00000008,		/// Rising trigger event configuration of line 3 offset:3 width:1 
		TR4 = 0x00000010,		/// Rising trigger event configuration of line 4 offset:4 width:1 
		TR5 = 0x00000020,		/// Rising trigger event configuration of line 5 offset:5 width:1 
		TR6 = 0x00000040,		/// Rising trigger event configuration of line 6 offset:6 width:1 
		TR7 = 0x00000080,		/// Rising trigger event configuration of line 7 offset:7 width:1 
		TR8 = 0x00000100,		/// Rising trigger event configuration of line 8 offset:8 width:1 
		TR9 = 0x00000200,		/// Rising trigger event configuration of line 9 offset:9 width:1 
		TR10 = 0x00000400,		/// Rising trigger event configuration of line 10 offset:10 width:1 
		TR11 = 0x00000800,		/// Rising trigger event configuration of line 11 offset:11 width:1 
		TR12 = 0x00001000,		/// Rising trigger event configuration of line 12 offset:12 width:1 
		TR13 = 0x00002000,		/// Rising trigger event configuration of line 13 offset:13 width:1 
		TR14 = 0x00004000,		/// Rising trigger event configuration of line 14 offset:14 width:1 
		TR15 = 0x00008000,		/// Rising trigger event configuration of line 15 offset:15 width:1 
		TR16 = 0x00010000,		/// Rising trigger event configuration of line 16 offset:16 width:1 
		TR17 = 0x00020000,		/// Rising trigger event configuration of line 17 offset:17 width:1 
		TR18 = 0x00040000,		/// Rising trigger event configuration of line 18 offset:18 width:1 
		TR19 = 0x00080000,		/// Rising trigger event configuration of line 19 offset:19 width:1 
		TR20 = 0x00100000,		/// Rising trigger event configuration of line 20 offset:20 width:1 
		TR21 = 0x00200000,		/// Rising trigger event configuration of line 21 offset:21 width:1 
		TR22 = 0x00400000,		/// Rising trigger event configuration of line 22 offset:22 width:1 
	}
	/// Falling Trigger selection register (EXTI_FTSR)
	enum mask_FTSR {
		TR0 = 0x00000001,		/// Falling trigger event configuration of line 0 offset:0 width:1 
		TR1 = 0x00000002,		/// Falling trigger event configuration of line 1 offset:1 width:1 
		TR2 = 0x00000004,		/// Falling trigger event configuration of line 2 offset:2 width:1 
		TR3 = 0x00000008,		/// Falling trigger event configuration of line 3 offset:3 width:1 
		TR4 = 0x00000010,		/// Falling trigger event configuration of line 4 offset:4 width:1 
		TR5 = 0x00000020,		/// Falling trigger event configuration of line 5 offset:5 width:1 
		TR6 = 0x00000040,		/// Falling trigger event configuration of line 6 offset:6 width:1 
		TR7 = 0x00000080,		/// Falling trigger event configuration of line 7 offset:7 width:1 
		TR8 = 0x00000100,		/// Falling trigger event configuration of line 8 offset:8 width:1 
		TR9 = 0x00000200,		/// Falling trigger event configuration of line 9 offset:9 width:1 
		TR10 = 0x00000400,		/// Falling trigger event configuration of line 10 offset:10 width:1 
		TR11 = 0x00000800,		/// Falling trigger event configuration of line 11 offset:11 width:1 
		TR12 = 0x00001000,		/// Falling trigger event configuration of line 12 offset:12 width:1 
		TR13 = 0x00002000,		/// Falling trigger event configuration of line 13 offset:13 width:1 
		TR14 = 0x00004000,		/// Falling trigger event configuration of line 14 offset:14 width:1 
		TR15 = 0x00008000,		/// Falling trigger event configuration of line 15 offset:15 width:1 
		TR16 = 0x00010000,		/// Falling trigger event configuration of line 16 offset:16 width:1 
		TR17 = 0x00020000,		/// Falling trigger event configuration of line 17 offset:17 width:1 
		TR18 = 0x00040000,		/// Falling trigger event configuration of line 18 offset:18 width:1 
		TR19 = 0x00080000,		/// Falling trigger event configuration of line 19 offset:19 width:1 
		TR20 = 0x00100000,		/// Falling trigger event configuration of line 20 offset:20 width:1 
		TR21 = 0x00200000,		/// Falling trigger event configuration of line 21 offset:21 width:1 
		TR22 = 0x00400000,		/// Falling trigger event configuration of line 22 offset:22 width:1 
	}
	/// Software interrupt event register (EXTI_SWIER)
	enum mask_SWIER {
		SWIER0 = 0x00000001,		/// Software Interrupt on line 0 offset:0 width:1 
		SWIER1 = 0x00000002,		/// Software Interrupt on line 1 offset:1 width:1 
		SWIER2 = 0x00000004,		/// Software Interrupt on line 2 offset:2 width:1 
		SWIER3 = 0x00000008,		/// Software Interrupt on line 3 offset:3 width:1 
		SWIER4 = 0x00000010,		/// Software Interrupt on line 4 offset:4 width:1 
		SWIER5 = 0x00000020,		/// Software Interrupt on line 5 offset:5 width:1 
		SWIER6 = 0x00000040,		/// Software Interrupt on line 6 offset:6 width:1 
		SWIER7 = 0x00000080,		/// Software Interrupt on line 7 offset:7 width:1 
		SWIER8 = 0x00000100,		/// Software Interrupt on line 8 offset:8 width:1 
		SWIER9 = 0x00000200,		/// Software Interrupt on line 9 offset:9 width:1 
		SWIER10 = 0x00000400,		/// Software Interrupt on line 10 offset:10 width:1 
		SWIER11 = 0x00000800,		/// Software Interrupt on line 11 offset:11 width:1 
		SWIER12 = 0x00001000,		/// Software Interrupt on line 12 offset:12 width:1 
		SWIER13 = 0x00002000,		/// Software Interrupt on line 13 offset:13 width:1 
		SWIER14 = 0x00004000,		/// Software Interrupt on line 14 offset:14 width:1 
		SWIER15 = 0x00008000,		/// Software Interrupt on line 15 offset:15 width:1 
		SWIER16 = 0x00010000,		/// Software Interrupt on line 16 offset:16 width:1 
		SWIER17 = 0x00020000,		/// Software Interrupt on line 17 offset:17 width:1 
		SWIER18 = 0x00040000,		/// Software Interrupt on line 18 offset:18 width:1 
		SWIER19 = 0x00080000,		/// Software Interrupt on line 19 offset:19 width:1 
		SWIER20 = 0x00100000,		/// Software Interrupt on line 20 offset:20 width:1 
		SWIER21 = 0x00200000,		/// Software Interrupt on line 21 offset:21 width:1 
		SWIER22 = 0x00400000,		/// Software Interrupt on line 22 offset:22 width:1 
	}
	/// Pending register (EXTI_PR)
	enum mask_PR {
		PR0 = 0x00000001,		/// Pending bit 0 offset:0 width:1 
		PR1 = 0x00000002,		/// Pending bit 1 offset:1 width:1 
		PR2 = 0x00000004,		/// Pending bit 2 offset:2 width:1 
		PR3 = 0x00000008,		/// Pending bit 3 offset:3 width:1 
		PR4 = 0x00000010,		/// Pending bit 4 offset:4 width:1 
		PR5 = 0x00000020,		/// Pending bit 5 offset:5 width:1 
		PR6 = 0x00000040,		/// Pending bit 6 offset:6 width:1 
		PR7 = 0x00000080,		/// Pending bit 7 offset:7 width:1 
		PR8 = 0x00000100,		/// Pending bit 8 offset:8 width:1 
		PR9 = 0x00000200,		/// Pending bit 9 offset:9 width:1 
		PR10 = 0x00000400,		/// Pending bit 10 offset:10 width:1 
		PR11 = 0x00000800,		/// Pending bit 11 offset:11 width:1 
		PR12 = 0x00001000,		/// Pending bit 12 offset:12 width:1 
		PR13 = 0x00002000,		/// Pending bit 13 offset:13 width:1 
		PR14 = 0x00004000,		/// Pending bit 14 offset:14 width:1 
		PR15 = 0x00008000,		/// Pending bit 15 offset:15 width:1 
		PR16 = 0x00010000,		/// Pending bit 16 offset:16 width:1 
		PR17 = 0x00020000,		/// Pending bit 17 offset:17 width:1 
		PR18 = 0x00040000,		/// Pending bit 18 offset:18 width:1 
		PR19 = 0x00080000,		/// Pending bit 19 offset:19 width:1 
		PR20 = 0x00100000,		/// Pending bit 20 offset:20 width:1 
		PR21 = 0x00200000,		/// Pending bit 21 offset:21 width:1 
		PR22 = 0x00400000,		/// Pending bit 22 offset:22 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_IMR,uint) IMR;		/// Interrupt mask register (EXTI_IMR) offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_EMR,uint) EMR;		/// Event mask register (EXTI_EMR) offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_RTSR,uint) RTSR;		/// Rising Trigger selection register (EXTI_RTSR) offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_FTSR,uint) FTSR;		/// Falling Trigger selection register (EXTI_FTSR) offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SWIER,uint) SWIER;		/// Software interrupt event register (EXTI_SWIER) offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_PR,uint) PR;		/// Pending register (EXTI_PR) offset:0x00000014
}
/// Nested Vectored Interrupt Controller address:0xE000E000
struct NVIC_Type{
	@disable this();
	/// Interrupt Controller Type Register
	enum mask_ICTR {
		INTLINESNUM = 0x0000000F,		/// Total number of interrupt lines in groups offset:0 width:4 
	}
	/// Software Triggered Interrupt Register
	enum mask_STIR {
		INTID = 0x000001FF,		/// interrupt to be triggered offset:0 width:9 
	}
	/// Interrupt Set-Enable Register
	enum mask_ISER0 {
		SETENA = 0xFFFFFFFF,		/// SETENA offset:0 width:32 
	}
	/// Interrupt Set-Enable Register
	enum mask_ISER1 {
		SETENA = 0xFFFFFFFF,		/// SETENA offset:0 width:32 
	}
	/// Interrupt Set-Enable Register
	enum mask_ISER2 {
		SETENA = 0xFFFFFFFF,		/// SETENA offset:0 width:32 
	}
	/// Interrupt Clear-Enable Register
	enum mask_ICER0 {
		CLRENA = 0xFFFFFFFF,		/// CLRENA offset:0 width:32 
	}
	/// Interrupt Clear-Enable Register
	enum mask_ICER1 {
		CLRENA = 0xFFFFFFFF,		/// CLRENA offset:0 width:32 
	}
	/// Interrupt Clear-Enable Register
	enum mask_ICER2 {
		CLRENA = 0xFFFFFFFF,		/// CLRENA offset:0 width:32 
	}
	/// Interrupt Set-Pending Register
	enum mask_ISPR0 {
		SETPEND = 0xFFFFFFFF,		/// SETPEND offset:0 width:32 
	}
	/// Interrupt Set-Pending Register
	enum mask_ISPR1 {
		SETPEND = 0xFFFFFFFF,		/// SETPEND offset:0 width:32 
	}
	/// Interrupt Set-Pending Register
	enum mask_ISPR2 {
		SETPEND = 0xFFFFFFFF,		/// SETPEND offset:0 width:32 
	}
	/// Interrupt Clear-Pending Register
	enum mask_ICPR0 {
		CLRPEND = 0xFFFFFFFF,		/// CLRPEND offset:0 width:32 
	}
	/// Interrupt Clear-Pending Register
	enum mask_ICPR1 {
		CLRPEND = 0xFFFFFFFF,		/// CLRPEND offset:0 width:32 
	}
	/// Interrupt Clear-Pending Register
	enum mask_ICPR2 {
		CLRPEND = 0xFFFFFFFF,		/// CLRPEND offset:0 width:32 
	}
	/// Interrupt Active Bit Register
	enum mask_IABR0 {
		ACTIVE = 0xFFFFFFFF,		/// ACTIVE offset:0 width:32 
	}
	/// Interrupt Active Bit Register
	enum mask_IABR1 {
		ACTIVE = 0xFFFFFFFF,		/// ACTIVE offset:0 width:32 
	}
	/// Interrupt Active Bit Register
	enum mask_IABR2 {
		ACTIVE = 0xFFFFFFFF,		/// ACTIVE offset:0 width:32 
	}
	/// Interrupt Priority Register
	enum mask_IPR0 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR1 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR2 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR3 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR4 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR5 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR6 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR7 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR8 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR9 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR10 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR11 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR12 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR13 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR14 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR15 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR16 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR17 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR18 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	/// Interrupt Priority Register
	enum mask_IPR19 {
		IPR_N0 = 0x000000FF,		/// IPR_N0 offset:0 width:8 
		IPR_N1 = 0x0000FF00,		/// IPR_N1 offset:8 width:8 
		IPR_N2 = 0x00FF0000,		/// IPR_N2 offset:16 width:8 
		IPR_N3 = 0xFF000000,		/// IPR_N3 offset:24 width:8 
	}
	@Register("read-only",0x04)	RO_Reg!(mask_ICTR,uint) ICTR;		/// Interrupt Controller Type Register offset:0x00000004
	uint[0x3E]		Reserved0;
	@Register("read-write",0x100)	RW_Reg!(mask_ISER0,uint) ISER0;		/// Interrupt Set-Enable Register offset:0x00000100
	@Register("read-write",0x104)	RW_Reg!(mask_ISER1,uint) ISER1;		/// Interrupt Set-Enable Register offset:0x00000104
	@Register("read-write",0x108)	RW_Reg!(mask_ISER2,uint) ISER2;		/// Interrupt Set-Enable Register offset:0x00000108
	uint[0x1D]		Reserved1;
	@Register("read-write",0x180)	RW_Reg!(mask_ICER0,uint) ICER0;		/// Interrupt Clear-Enable Register offset:0x00000180
	@Register("read-write",0x184)	RW_Reg!(mask_ICER1,uint) ICER1;		/// Interrupt Clear-Enable Register offset:0x00000184
	@Register("read-write",0x188)	RW_Reg!(mask_ICER2,uint) ICER2;		/// Interrupt Clear-Enable Register offset:0x00000188
	uint[0x1D]		Reserved2;
	@Register("read-write",0x200)	RW_Reg!(mask_ISPR0,uint) ISPR0;		/// Interrupt Set-Pending Register offset:0x00000200
	@Register("read-write",0x204)	RW_Reg!(mask_ISPR1,uint) ISPR1;		/// Interrupt Set-Pending Register offset:0x00000204
	@Register("read-write",0x208)	RW_Reg!(mask_ISPR2,uint) ISPR2;		/// Interrupt Set-Pending Register offset:0x00000208
	uint[0x1D]		Reserved3;
	@Register("read-write",0x280)	RW_Reg!(mask_ICPR0,uint) ICPR0;		/// Interrupt Clear-Pending Register offset:0x00000280
	@Register("read-write",0x284)	RW_Reg!(mask_ICPR1,uint) ICPR1;		/// Interrupt Clear-Pending Register offset:0x00000284
	@Register("read-write",0x288)	RW_Reg!(mask_ICPR2,uint) ICPR2;		/// Interrupt Clear-Pending Register offset:0x00000288
	uint[0x1D]		Reserved4;
	@Register("read-only",0x300)	RO_Reg!(mask_IABR0,uint) IABR0;		/// Interrupt Active Bit Register offset:0x00000300
	@Register("read-only",0x304)	RO_Reg!(mask_IABR1,uint) IABR1;		/// Interrupt Active Bit Register offset:0x00000304
	@Register("read-only",0x308)	RO_Reg!(mask_IABR2,uint) IABR2;		/// Interrupt Active Bit Register offset:0x00000308
	uint[0x3D]		Reserved5;
	@Register("read-write",0x400)	RW_Reg!(mask_IPR0,uint) IPR0;		/// Interrupt Priority Register offset:0x00000400
	@Register("read-write",0x404)	RW_Reg!(mask_IPR1,uint) IPR1;		/// Interrupt Priority Register offset:0x00000404
	@Register("read-write",0x408)	RW_Reg!(mask_IPR2,uint) IPR2;		/// Interrupt Priority Register offset:0x00000408
	@Register("read-write",0x40C)	RW_Reg!(mask_IPR3,uint) IPR3;		/// Interrupt Priority Register offset:0x0000040C
	@Register("read-write",0x410)	RW_Reg!(mask_IPR4,uint) IPR4;		/// Interrupt Priority Register offset:0x00000410
	@Register("read-write",0x414)	RW_Reg!(mask_IPR5,uint) IPR5;		/// Interrupt Priority Register offset:0x00000414
	@Register("read-write",0x418)	RW_Reg!(mask_IPR6,uint) IPR6;		/// Interrupt Priority Register offset:0x00000418
	@Register("read-write",0x41C)	RW_Reg!(mask_IPR7,uint) IPR7;		/// Interrupt Priority Register offset:0x0000041C
	@Register("read-write",0x420)	RW_Reg!(mask_IPR8,uint) IPR8;		/// Interrupt Priority Register offset:0x00000420
	@Register("read-write",0x424)	RW_Reg!(mask_IPR9,uint) IPR9;		/// Interrupt Priority Register offset:0x00000424
	@Register("read-write",0x428)	RW_Reg!(mask_IPR10,uint) IPR10;		/// Interrupt Priority Register offset:0x00000428
	@Register("read-write",0x42C)	RW_Reg!(mask_IPR11,uint) IPR11;		/// Interrupt Priority Register offset:0x0000042C
	@Register("read-write",0x430)	RW_Reg!(mask_IPR12,uint) IPR12;		/// Interrupt Priority Register offset:0x00000430
	@Register("read-write",0x434)	RW_Reg!(mask_IPR13,uint) IPR13;		/// Interrupt Priority Register offset:0x00000434
	@Register("read-write",0x438)	RW_Reg!(mask_IPR14,uint) IPR14;		/// Interrupt Priority Register offset:0x00000438
	@Register("read-write",0x43C)	RW_Reg!(mask_IPR15,uint) IPR15;		/// Interrupt Priority Register offset:0x0000043C
	@Register("read-write",0x440)	RW_Reg!(mask_IPR16,uint) IPR16;		/// Interrupt Priority Register offset:0x00000440
	@Register("read-write",0x444)	RW_Reg!(mask_IPR17,uint) IPR17;		/// Interrupt Priority Register offset:0x00000444
	@Register("read-write",0x448)	RW_Reg!(mask_IPR18,uint) IPR18;		/// Interrupt Priority Register offset:0x00000448
	@Register("read-write",0x44C)	RW_Reg!(mask_IPR19,uint) IPR19;		/// Interrupt Priority Register offset:0x0000044C
	uint[0x2AC]		Reserved6;
	@Register("write-only",0xF00)	WO_Reg!(mask_STIR,uint) STIR;		/// Software Triggered Interrupt Register offset:0x00000F00
}
/// ADC common registers address:0x40012300
struct ADC_Common_Type{
	@disable this();
	/// ADC Common status register
	enum mask_CSR {
		OVR3 = 0x00200000,		/// Overrun flag of ADC3 offset:21 width:1 
		STRT3 = 0x00100000,		/// Regular channel Start flag of ADC 3 offset:20 width:1 
		JSTRT3 = 0x00080000,		/// Injected channel Start flag of ADC 3 offset:19 width:1 
		JEOC3 = 0x00040000,		/// Injected channel end of conversion of ADC 3 offset:18 width:1 
		EOC3 = 0x00020000,		/// End of conversion of ADC 3 offset:17 width:1 
		AWD3 = 0x00010000,		/// Analog watchdog flag of ADC 3 offset:16 width:1 
		OVR2 = 0x00002000,		/// Overrun flag of ADC 2 offset:13 width:1 
		STRT2 = 0x00001000,		/// Regular channel Start flag of ADC 2 offset:12 width:1 
		JSTRT2 = 0x00000800,		/// Injected channel Start flag of ADC 2 offset:11 width:1 
		JEOC2 = 0x00000400,		/// Injected channel end of conversion of ADC 2 offset:10 width:1 
		EOC2 = 0x00000200,		/// End of conversion of ADC 2 offset:9 width:1 
		AWD2 = 0x00000100,		/// Analog watchdog flag of ADC 2 offset:8 width:1 
		OVR1 = 0x00000020,		/// Overrun flag of ADC 1 offset:5 width:1 
		STRT1 = 0x00000010,		/// Regular channel Start flag of ADC 1 offset:4 width:1 
		JSTRT1 = 0x00000008,		/// Injected channel Start flag of ADC 1 offset:3 width:1 
		JEOC1 = 0x00000004,		/// Injected channel end of conversion of ADC 1 offset:2 width:1 
		EOC1 = 0x00000002,		/// End of conversion of ADC 1 offset:1 width:1 
		AWD1 = 0x00000001,		/// Analog watchdog flag of ADC 1 offset:0 width:1 
	}
	/// ADC common control register
	enum mask_CCR {
		TSVREFE = 0x00800000,		/// Temperature sensor and VREFINT enable offset:23 width:1 
		VBATE = 0x00400000,		/// VBAT enable offset:22 width:1 
		ADCPRE = 0x00030000,		/// ADC prescaler offset:16 width:2 
		DMA = 0x0000C000,		/// Direct memory access mode for multi ADC mode offset:14 width:2 
		DDS = 0x00002000,		/// DMA disable selection for multi-ADC mode offset:13 width:1 
		DELAY = 0x00000F00,		/// Delay between 2 sampling phases offset:8 width:4 
	}
	/// ADC common regular data register for dual
	enum mask_CDR {
		DATA2 = 0xFFFF0000,		/// 2nd data of a pair of regular conversions offset:16 width:16 
		DATA1 = 0x0000FFFF,		/// 1st data of a pair of regular conversions offset:0 width:16 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_CSR,uint) CSR;		/// ADC Common status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_CCR,uint) CCR;		/// ADC common control register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_CDR,uint) CDR;		/// ADC common regular data register for dual offset:0x00000008
}
/// Floting point unit address:0xE000EF34
struct FPU_Type{
	@disable this();
	enum Interrupt {
		FPU = 81,		/// Floating point unit interrupt
	};
	/// Floating-point context control register
	enum mask_FPCCR {
		LSPACT = 0x00000001,		/// LSPACT offset:0 width:1 
		USER = 0x00000002,		/// USER offset:1 width:1 
		THREAD = 0x00000008,		/// THREAD offset:3 width:1 
		HFRDY = 0x00000010,		/// HFRDY offset:4 width:1 
		MMRDY = 0x00000020,		/// MMRDY offset:5 width:1 
		BFRDY = 0x00000040,		/// BFRDY offset:6 width:1 
		MONRDY = 0x00000100,		/// MONRDY offset:8 width:1 
		LSPEN = 0x40000000,		/// LSPEN offset:30 width:1 
		ASPEN = 0x80000000,		/// ASPEN offset:31 width:1 
	}
	/// Floating-point context address register
	enum mask_FPCAR {
		ADDRESS = 0xFFFFFFF8,		/// Location of unpopulated floating-point offset:3 width:29 
	}
	/// Floating-point status control register
	enum mask_FPSCR {
		IOC = 0x00000001,		/// Invalid operation cumulative exception bit offset:0 width:1 
		DZC = 0x00000002,		/// Division by zero cumulative exception bit. offset:1 width:1 
		OFC = 0x00000004,		/// Overflow cumulative exception bit offset:2 width:1 
		UFC = 0x00000008,		/// Underflow cumulative exception bit offset:3 width:1 
		IXC = 0x00000010,		/// Inexact cumulative exception bit offset:4 width:1 
		IDC = 0x00000080,		/// Input denormal cumulative exception bit. offset:7 width:1 
		RMode = 0x00C00000,		/// Rounding Mode control field offset:22 width:2 
		FZ = 0x01000000,		/// Flush-to-zero mode control bit: offset:24 width:1 
		DN = 0x02000000,		/// Default NaN mode control bit offset:25 width:1 
		AHP = 0x04000000,		/// Alternative half-precision control bit offset:26 width:1 
		V = 0x10000000,		/// Overflow condition code flag offset:28 width:1 
		C = 0x20000000,		/// Carry condition code flag offset:29 width:1 
		Z = 0x40000000,		/// Zero condition code flag offset:30 width:1 
		N = 0x80000000,		/// Negative condition code flag offset:31 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_FPCCR,uint) FPCCR;		/// Floating-point context control register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_FPCAR,uint) FPCAR;		/// Floating-point context address register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_FPSCR,uint) FPSCR;		/// Floating-point status control register offset:0x00000008
}
/// Memory protection unit address:0xE000ED90
struct MPU_Type{
	@disable this();
	/// MPU type register
	enum mask_MPU_TYPER {
		SEPARATE = 0x00000001,		/// Separate flag offset:0 width:1 
		DREGION = 0x0000FF00,		/// Number of MPU data regions offset:8 width:8 
		IREGION = 0x00FF0000,		/// Number of MPU instruction regions offset:16 width:8 
	}
	/// MPU control register
	enum mask_MPU_CTRL {
		ENABLE = 0x00000001,		/// Enables the MPU offset:0 width:1 
		HFNMIENA = 0x00000002,		/// Enables the operation of MPU during hard fault offset:1 width:1 
		PRIVDEFENA = 0x00000004,		/// Enable priviliged software access to default memory map offset:2 width:1 
	}
	/// MPU region number register
	enum mask_MPU_RNR {
		REGION = 0x000000FF,		/// MPU region offset:0 width:8 
	}
	/// MPU region base address register
	enum mask_MPU_RBAR {
		REGION = 0x0000000F,		/// MPU region field offset:0 width:4 
		VALID = 0x00000010,		/// MPU region number valid offset:4 width:1 
		ADDR = 0xFFFFFFE0,		/// Region base address field offset:5 width:27 
	}
	/// MPU region attribute and size register
	enum mask_MPU_RASR {
		ENABLE = 0x00000001,		/// Region enable bit. offset:0 width:1 
		SIZE = 0x0000003E,		/// Size of the MPU protection region offset:1 width:5 
		SRD = 0x0000FF00,		/// Subregion disable bits offset:8 width:8 
		B = 0x00010000,		/// memory attribute offset:16 width:1 
		C = 0x00020000,		/// memory attribute offset:17 width:1 
		S = 0x00040000,		/// Shareable memory attribute offset:18 width:1 
		TEX = 0x00380000,		/// memory attribute offset:19 width:3 
		AP = 0x07000000,		/// Access permission offset:24 width:3 
		XN = 0x10000000,		/// Instruction access disable bit offset:28 width:1 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_MPU_TYPER,uint) MPU_TYPER;		/// MPU type register offset:0x00000000
	@Register("read-only",0x04)	RO_Reg!(mask_MPU_CTRL,uint) MPU_CTRL;		/// MPU control register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_MPU_RNR,uint) MPU_RNR;		/// MPU region number register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_MPU_RBAR,uint) MPU_RBAR;		/// MPU region base address register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_MPU_RASR,uint) MPU_RASR;		/// MPU region attribute and size register offset:0x00000010
}
/// SysTick timer address:0xE000E010
struct STK_Type{
	@disable this();
	/// SysTick control and status register
	enum mask_CTRL {
		ENABLE = 0x00000001,		/// Counter enable offset:0 width:1 
		TICKINT = 0x00000002,		/// SysTick exception request enable offset:1 width:1 
		CLKSOURCE = 0x00000004,		/// Clock source selection offset:2 width:1 
		COUNTFLAG = 0x00010000,		/// COUNTFLAG offset:16 width:1 
	}
	/// SysTick reload value register
	enum mask_LOAD {
		RELOAD = 0x00FFFFFF,		/// RELOAD value offset:0 width:24 
	}
	/// SysTick current value register
	enum mask_VAL {
		CURRENT = 0x00FFFFFF,		/// Current counter value offset:0 width:24 
	}
	/// SysTick calibration value register
	enum mask_CALIB {
		TENMS = 0x00FFFFFF,		/// Calibration value offset:0 width:24 
		SKEW = 0x40000000,		/// SKEW flag: Indicates whether the TENMS value is exact offset:30 width:1 
		NOREF = 0x80000000,		/// NOREF flag. Reads as zero offset:31 width:1 
	}
	@Register("read-write",0x00)	RW_Reg!(mask_CTRL,uint) CTRL;		/// SysTick control and status register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_LOAD,uint) LOAD;		/// SysTick reload value register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_VAL,uint) VAL;		/// SysTick current value register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_CALIB,uint) CALIB;		/// SysTick calibration value register offset:0x0000000C
}
/// System control block address:0xE000ED00
struct SCB_Type{
	@disable this();
	/// CPUID base register
	enum mask_CPUID {
		Revision = 0x0000000F,		/// Revision number offset:0 width:4 
		PartNo = 0x0000FFF0,		/// Part number of the processor offset:4 width:12 
		Constant = 0x000F0000,		/// Reads as 0xF offset:16 width:4 
		Variant = 0x00F00000,		/// Variant number offset:20 width:4 
		Implementer = 0xFF000000,		/// Implementer code offset:24 width:8 
	}
	/// Interrupt control and state register
	enum mask_ICSR {
		VECTACTIVE = 0x000001FF,		/// Active vector offset:0 width:9 
		RETTOBASE = 0x00000800,		/// Return to base level offset:11 width:1 
		VECTPENDING = 0x0007F000,		/// Pending vector offset:12 width:7 
		ISRPENDING = 0x00400000,		/// Interrupt pending flag offset:22 width:1 
		PENDSTCLR = 0x02000000,		/// SysTick exception clear-pending bit offset:25 width:1 
		PENDSTSET = 0x04000000,		/// SysTick exception set-pending bit offset:26 width:1 
		PENDSVCLR = 0x08000000,		/// PendSV clear-pending bit offset:27 width:1 
		PENDSVSET = 0x10000000,		/// PendSV set-pending bit offset:28 width:1 
		NMIPENDSET = 0x80000000,		/// NMI set-pending bit. offset:31 width:1 
	}
	/// Vector table offset register
	enum mask_VTOR {
		TBLOFF = 0x3FFFFE00,		/// Vector table base offset field offset:9 width:21 
	}
	/// Application interrupt and reset control register
	enum mask_AIRCR {
		VECTRESET = 0x00000001,		/// VECTRESET offset:0 width:1 
		VECTCLRACTIVE = 0x00000002,		/// VECTCLRACTIVE offset:1 width:1 
		SYSRESETREQ = 0x00000004,		/// SYSRESETREQ offset:2 width:1 
		PRIGROUP = 0x00000700,		/// PRIGROUP offset:8 width:3 
		ENDIANESS = 0x00008000,		/// ENDIANESS offset:15 width:1 
		VECTKEY = 0xFFFF0000,		/// Register key offset:16 width:16 
	}
	/// System control register
	enum mask_SCR {
		SLEEPONEXIT = 0x00000002,		/// SLEEPONEXIT offset:1 width:1 
		SLEEPDEEP = 0x00000004,		/// SLEEPDEEP offset:2 width:1 
		SEVEONPEND = 0x00000010,		/// Send Event on Pending bit offset:4 width:1 
	}
	/// Configuration and control register
	enum mask_CCR {
		NONBASETHRDENA = 0x00000001,		/// Configures how the processor enters Thread mode offset:0 width:1 
		USERSETMPEND = 0x00000002,		/// USERSETMPEND offset:1 width:1 
		UNALIGN__TRP = 0x00000008,		/// UNALIGN_ TRP offset:3 width:1 
		DIV_0_TRP = 0x00000010,		/// DIV_0_TRP offset:4 width:1 
		BFHFNMIGN = 0x00000100,		/// BFHFNMIGN offset:8 width:1 
		STKALIGN = 0x00000200,		/// STKALIGN offset:9 width:1 
	}
	/// System handler priority registers
	enum mask_SHPR1 {
		PRI_4 = 0x000000FF,		/// Priority of system handler 4 offset:0 width:8 
		PRI_5 = 0x0000FF00,		/// Priority of system handler 5 offset:8 width:8 
		PRI_6 = 0x00FF0000,		/// Priority of system handler 6 offset:16 width:8 
	}
	/// System handler priority registers
	enum mask_SHPR2 {
		PRI_11 = 0xFF000000,		/// Priority of system handler 11 offset:24 width:8 
	}
	/// System handler priority registers
	enum mask_SHPR3 {
		PRI_14 = 0x00FF0000,		/// Priority of system handler 14 offset:16 width:8 
		PRI_15 = 0xFF000000,		/// Priority of system handler 15 offset:24 width:8 
	}
	/// System handler control and state register
	enum mask_SHCRS {
		MEMFAULTACT = 0x00000001,		/// Memory management fault exception active bit offset:0 width:1 
		BUSFAULTACT = 0x00000002,		/// Bus fault exception active bit offset:1 width:1 
		USGFAULTACT = 0x00000008,		/// Usage fault exception active bit offset:3 width:1 
		SVCALLACT = 0x00000080,		/// SVC call active bit offset:7 width:1 
		MONITORACT = 0x00000100,		/// Debug monitor active bit offset:8 width:1 
		PENDSVACT = 0x00000400,		/// PendSV exception active bit offset:10 width:1 
		SYSTICKACT = 0x00000800,		/// SysTick exception active bit offset:11 width:1 
		USGFAULTPENDED = 0x00001000,		/// Usage fault exception pending bit offset:12 width:1 
		MEMFAULTPENDED = 0x00002000,		/// Memory management fault exception pending bit offset:13 width:1 
		BUSFAULTPENDED = 0x00004000,		/// Bus fault exception pending bit offset:14 width:1 
		SVCALLPENDED = 0x00008000,		/// SVC call pending bit offset:15 width:1 
		MEMFAULTENA = 0x00010000,		/// Memory management fault enable bit offset:16 width:1 
		BUSFAULTENA = 0x00020000,		/// Bus fault enable bit offset:17 width:1 
		USGFAULTENA = 0x00040000,		/// Usage fault enable bit offset:18 width:1 
	}
	/// Configurable fault status register
	enum mask_CFSR_UFSR_BFSR_MMFSR {
		IACCVIOL = 0x00000002,		/// Instruction access violation flag offset:1 width:1 
		MUNSTKERR = 0x00000008,		/// Memory manager fault on unstacking for a return from exception offset:3 width:1 
		MSTKERR = 0x00000010,		/// Memory manager fault on stacking for exception entry. offset:4 width:1 
		MLSPERR = 0x00000020,		/// MLSPERR offset:5 width:1 
		MMARVALID = 0x00000080,		/// Memory Management Fault Address Register (MMAR) valid flag offset:7 width:1 
		IBUSERR = 0x00000100,		/// Instruction bus error offset:8 width:1 
		PRECISERR = 0x00000200,		/// Precise data bus error offset:9 width:1 
		IMPRECISERR = 0x00000400,		/// Imprecise data bus error offset:10 width:1 
		UNSTKERR = 0x00000800,		/// Bus fault on unstacking for a return from exception offset:11 width:1 
		STKERR = 0x00001000,		/// Bus fault on stacking for exception entry offset:12 width:1 
		LSPERR = 0x00002000,		/// Bus fault on floating-point lazy state preservation offset:13 width:1 
		BFARVALID = 0x00008000,		/// Bus Fault Address Register (BFAR) valid flag offset:15 width:1 
		UNDEFINSTR = 0x00010000,		/// Undefined instruction usage fault offset:16 width:1 
		INVSTATE = 0x00020000,		/// Invalid state usage fault offset:17 width:1 
		INVPC = 0x00040000,		/// Invalid PC load usage fault offset:18 width:1 
		NOCP = 0x00080000,		/// No coprocessor usage fault. offset:19 width:1 
		UNALIGNED = 0x01000000,		/// Unaligned access usage fault offset:24 width:1 
		DIVBYZERO = 0x02000000,		/// Divide by zero usage fault offset:25 width:1 
	}
	/// Hard fault status register
	enum mask_HFSR {
		VECTTBL = 0x00000002,		/// Vector table hard fault offset:1 width:1 
		FORCED = 0x40000000,		/// Forced hard fault offset:30 width:1 
		DEBUG_VT = 0x80000000,		/// Reserved for Debug use offset:31 width:1 
	}
	/// Memory management fault address register
	enum mask_MMFAR {
		MMFAR = 0xFFFFFFFF,		/// Memory management fault address offset:0 width:32 
	}
	/// Bus fault address register
	enum mask_BFAR {
		BFAR = 0xFFFFFFFF,		/// Bus fault address offset:0 width:32 
	}
	/// Auxiliary fault status register
	enum mask_AFSR {
		IMPDEF = 0xFFFFFFFF,		/// Implementation defined offset:0 width:32 
	}
	@Register("read-only",0x00)	RO_Reg!(mask_CPUID,uint) CPUID;		/// CPUID base register offset:0x00000000
	@Register("read-write",0x04)	RW_Reg!(mask_ICSR,uint) ICSR;		/// Interrupt control and state register offset:0x00000004
	@Register("read-write",0x08)	RW_Reg!(mask_VTOR,uint) VTOR;		/// Vector table offset register offset:0x00000008
	@Register("read-write",0x0C)	RW_Reg!(mask_AIRCR,uint) AIRCR;		/// Application interrupt and reset control register offset:0x0000000C
	@Register("read-write",0x10)	RW_Reg!(mask_SCR,uint) SCR;		/// System control register offset:0x00000010
	@Register("read-write",0x14)	RW_Reg!(mask_CCR,uint) CCR;		/// Configuration and control register offset:0x00000014
	@Register("read-write",0x18)	RW_Reg!(mask_SHPR1,uint) SHPR1;		/// System handler priority registers offset:0x00000018
	@Register("read-write",0x1C)	RW_Reg!(mask_SHPR2,uint) SHPR2;		/// System handler priority registers offset:0x0000001C
	@Register("read-write",0x20)	RW_Reg!(mask_SHPR3,uint) SHPR3;		/// System handler priority registers offset:0x00000020
	@Register("read-write",0x24)	RW_Reg!(mask_SHCRS,uint) SHCRS;		/// System handler control and state register offset:0x00000024
	@Register("read-write",0x28)	RW_Reg!(mask_CFSR_UFSR_BFSR_MMFSR,uint) CFSR_UFSR_BFSR_MMFSR;		/// Configurable fault status register offset:0x00000028
	@Register("read-write",0x2C)	RW_Reg!(mask_HFSR,uint) HFSR;		/// Hard fault status register offset:0x0000002C
	uint[0x01]		Reserved0;
	@Register("read-write",0x34)	RW_Reg!(mask_MMFAR,uint) MMFAR;		/// Memory management fault address register offset:0x00000034
	@Register("read-write",0x38)	RW_Reg!(mask_BFAR,uint) BFAR;		/// Bus fault address register offset:0x00000038
	@Register("read-write",0x3C)	RW_Reg!(mask_AFSR,uint) AFSR;		/// Auxiliary fault status register offset:0x0000003C
}

