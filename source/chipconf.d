


module chipconf;


 
import arm.stk:STK_Conf,STK_TickFreq;
/// 主配置
struct ChipConfig{
    /// 建立默认 中断函数,可通过指定 mangle 的方式重定位 默认:true
    enum Maker_IRQHandler = true;
    
    enum Enable_CRC         = true;

    /// 芯片名称
    enum TargetChips        = "stm32f401cc";
    // 芯片家族
    
	enum string SVDName                  = `STM32F401`;
	enum string SVDVersion               = `1.1`;
	enum size_t SVDUnitBits              = 8;
	enum size_t SVDWidth                 = 32;
	enum size_t SVDSize                  = 0x20;
	enum size_t SVDresetValue            = 0x0;
	enum size_t SVDresetMask             = 0xFFFFFFFF;
	enum string SVDCpuName               = `CM4`;
	enum string SVDCpuRevision           = `r1p0`;
	enum string SVDCpuEndian             = `little`;
	enum bool SVDCpuMpuPresent           = true;
	enum bool SVDCpuFpuPresent           = true;
	enum size_t SVDCpuNvicPrioBits       = 4;
	enum bool SVDCpuVendorSystickConfig  = false;


	enum bool CRCEnable                  = true;
	/// Instruction cache
	enum bool ICacheEnable               = true;
	/// Data cache
	enum bool DCacheEnable               = true;
	/// Flash prefetch
	enum bool PrefetchEnable             = true;
	/// 
	enum uint Flash_LATENCY			  = 2;

	/// 系统目标频率
	enum HCLK_Frequency = 84_000_000;
	/// HES时钟源频率
	enum HSE_Frequency = 0;
	/// LSE时钟源频率
	enum LSE_Frequency = 0;
	/// AHB Prescaler
	enum AHB_Prescaler = 	0b0000;
	/// APB1 Prescaler
	enum APB1_Prescaler = 	0b0100;
	/// APB2 Prescaler
	enum APB2_Prescaler = 	0b0000;

	/// 是否使用usb
	enum bool UseUSB = true;
	
	enum Config_STK 			 = STK_Conf(true,STK_TickFreq.Freq_1KHz,true);
    
}
