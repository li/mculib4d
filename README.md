# mculib4d

## 更新记录
20230318 之前一直没有再继续更新是因为觉得繁琐,和我想要的一个编译器就行,不要那些混乱的库距离太大就暂停了一下,最近看dlang的日志看到可以支持位操作了,虽然还有很多bug.但它能用了
我这就提交上来一版可以编译通过可以烧录到stm32f401CC,正常执行的版本.我认为可以到试用阶段了.
原有体系被作废了,这次提交上来的也不确定后面会不会改,不过修改的目标是弄出一个开发用的库,完全不去交叉C的基础库,定位是源码库,
具体编译器参数查阅 dub.json ldc2.conf配置文件

**编译说明**
dub build 直接构建就好了
build --compiler=ldc2 --build=debug
build --compiler=ldc2 --build=release


## 介绍
D语言原生的arm的hal操作库,直接使用[LDC](https://github.com/ldc-developers/ldc)进行编译,无需交叉环境这种混乱东西.
通过**SVD**直接生成mc的底层操作,大部分功能基于D的模板进行实现,尽量实现D语言原生的实现.

本人搞单片机有点年头了,一直都是干应用的.常用开发软件用Keil,iar,一类的盗版软件,在经历2020年的时候突然发现不能在这样下去了,
各种技术上的**卡脖子**事情让我觉得应该 "**搞点事情**",
国产的MCU很多,真的不少,可为何开发软件及开发库就那么几种盗版的?
离开这些软件我们还能做自己的事情么?
所以我定下目标是搞一个D语言下的单片机开发(嵌入式的直接用就好了;~))

我认为D语言做开发有一些先天上的设计优势,比如说非常强力的[编译前解释器](https://blog.csdn.net/sdv/article/details/113239316)(感觉类似很强的宏脚本解释器),把所有没必要的运算压在编译前.
资源文件无需转换直接嵌入 **`import(string file)`** ,
强大的 LDC 可以支持多种MCU及结构 (aarch64,arm,avr,mips,thumb,esp32.....),很适合**搞事情**,其内置的LDD还可以支持直接的bin输出(当前版本 LLVM 11还是存在TLS的BUG)

欢迎**搞事情的人**一起出手.

**追加说明**
用D搞这个有上面提到的原因,再就是现在的LL库和hal库都是人家ST的,国内芯片企业的库还都是纯寄存器控制,缺少 面向对象.
虽然D的betterC模式下不支持完整的面向对象操作,但它能用,可以给出基础的调用关系,更好的是 VScode上的D插件可以正确识别代码提示.

## 构架原则
1, 能编译前做的,全部弄到编译前
2, 库中尽量避免嵌入汇编,避免特定汇编指令,能交给编译器的全部交给编译器
3, 类似bitband这种应用交给程序员去处理,库中只保留通用部分
4, 非必要不添加二进制文件到编译队列中 **当前有一个 object.o是必要的,内容是一个空白的object.d的二进制 , 应对缺少druntime问题**
5, 添加扩展功能实现放在 ext部分,使用D语言编写


## 软件架构
前端选择D语言,编译器使用LDC.

D语言中声明 与存储位置相关
```c
uint t1=0x11111111;             // this goes to tdata   TLS,有初始化
int t2;                         // this goes to tbss    TLS
__gshared int t3=0x33333333;    // this goes to data    有初始化
__gshared int t4;               // this goes to bss     无初始化


shared int t5 = 0x55555555;     // this goes to data    有初始化
static int t6;                  // this goes to bss     无初始化
shared int tt6;                 // this goes to bss     无初始化
```

**注意** 当前库中并**未实现** TLS的功能.当前解决方式是指向bss的存储位置
**注意** 当前实现了malloc和free,具体实现可以查阅代码
**注意** 当前实现了class的构建 通过 函数模板完成的
```d
C* c = CreateObject!(C)();
```

## 目录结构
mculib
arch|Arch 目录,当前目录下包括一些Arch支持的杂项
arch.***|Arch 支持
chips|mcu 芯片型号对应
stdlib|本库自身支持
与phobos内子目录同名|调用库简单修正,不建议从D的基础库中调用.

直接调用寄存器库

```c
import chips;
extern(C) void mloop() 
{
    RCC.CR.PLLI2SON = 0x00 ;  // 这里可以直接使用寄存器 所有寄存器由 **Register_(T)**结构 实现
    while(1){}
}

```
## 现有完成
svd工具已经重写,代码未整理,未提交上来

### 自定义中断函数方式

```c

```


## 安装教程

1.  **不需要了** 编译器需 LDC,开发用版本为 1.24/DMD v2.094.1,因 LDD的BUG不能由ldc直接输出bin文件,需要使用 arm-none-eabi 的LD 和 objcopy.LDC2可以输出不使用abi LTS的方案.
1.1 直接安装LDC - the LLVM D compiler (1.31.0)就行
2.  IDE可使用VScode,更多信息可参考[我的文章](https://blog.csdn.net/sdv/article/details/112453911).
3.  **不需要了** windows的host平台编译实现代码在 bu2.cmd中,linux的编译脚本我未实现
3.1 直接使用dub构建就行了
4. 烧录脚本我没有实现,可自由选择烧录程序 **我当前是用jlink**
5.  **不需要了**通过**tools**目录下的**svd**工具可以转换现有svd数据.
5.1 新版本还没由整理提交

## 说明

1.  代码可自由使用,商业发布请注明来源
2.  二次发布请告知我一下,一同开心
3.  xxxx其他的想到再写

## 参与贡献

1.  开发参考的硬件为 STM32F401CC,和芯片相关的手册
2.  代码参考 [wiki](https://wiki.dlang.org/Minimal_semihosted_ARM_Cortex-M_%22Hello_World%22) , 
4.  有讨论的发issues
5.  开发中使用的一些不常用技巧汇总在[我的博客](https://blog.csdn.net/sdv) .
6.  target 设定 https://clang.llvm.org/docs/CrossCompilation.html
7.  开发库生成 https://github.com/ARM-software/LLVM-embedded-toolchain-for-Arm
8.  


## 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

# 更新记录
- 2023年3月18日:提交了新的构架版本,放弃原有的结构.TLS直接调用BSS的对象,也就是没有TLS管理.替换原来的寄存器管理派生,采用static interface 及其模板化方式实例化 外设寄存器操作.支持class的操作(当前无应用,后面要做硬件功能寄存器的分时复用).当前问题是生产的代码量较大,不适合需要尺寸优化场合..(这种场合还多么...去用CC吧,就差那几分钱啊).
- 2021年4月9日: 开了个公司,主营是原型机开发(机械),更新这边的时间不会太多.
- 2021年2月11日:更新 `Peripheral` 内模板 ,详情查看 **寄存器读写模板**
- 2021年2月6日:更新目录树结构
- 2021年2月5日:增加自定义中断函数的方式
- 2021年2月3日:增加了一个svd转换用代码,详见tools目录下的svd.d .
- 2021年2月2日:精简了一下生成方案,控制一下单文件的总行数,不然vscode有些不正常.
- 2021年1月19日: 基本完成 peripheral 模板,通过简单测试 可正常应用.